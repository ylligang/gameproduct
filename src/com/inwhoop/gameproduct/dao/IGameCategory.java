package com.inwhoop.gameproduct.dao;

import java.util.List;

import com.inwhoop.gameproduct.entity.GameCategory;
/**
 * 游戏类别接口
 * @author lg
 *
 */
public interface IGameCategory {
	/**
	 * 获取所有的游戏类别
	 * @return
	 */
   List<GameCategory> getAllGameCategory();
   /**
    * 根据游戏类别id获取类别实体
    * @param gategoryId
    * @return
    */
   GameCategory findGameGategoryById(int gategoryId); 
   
}
