package com.inwhoop.gameproduct.dao;
/**
 * dialog接口
 * @Project: Game
 * @Title: DialogLinsener.java
 * @Package com.inwhoop.gameproduct.dao
 * @Description: TODO
 *
 * @author ligang@inwhoop.com
 * @date 2014-7-1 下午8:19:15
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public interface DialogLinsener {
    void onSure();
    void onCancle();
}
