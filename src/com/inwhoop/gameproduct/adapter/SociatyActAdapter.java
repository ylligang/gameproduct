package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.SociatyActBean;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * @Describe: TODO    公会活动列表适配器
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/11 10:12
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SociatyActAdapter extends BaseAdapter {
    private  Context context;
    private ArrayList<SociatyActBean> lists = new ArrayList<SociatyActBean>();

    public SociatyActAdapter(Context mContext) {
        this.context=mContext;
    }

    public ArrayList<SociatyActBean> getLists() {
        return lists;
    }

    public void addList(List<SociatyActBean> list) {
        for (SociatyActBean bean : list)
            lists.add(bean);
        notifyDataSetChanged();
    }
    
    public void addInfo(SociatyActBean bean){
    	lists.add(0, bean);
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (null == convertView) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_sociaty_act,
                    null);
            holder.headImg = (ImageView) convertView.findViewById(R.id.act_img);
            holder.actTitle = (TextView) convertView.findViewById(R.id.act_title);
            holder.actContent = (TextView) convertView.findViewById(R.id.act_content);
            holder.startTiem = (TextView) convertView.findViewById(R.id.start_time);
            holder.endTime = (TextView) convertView.findViewById(R.id.end_time);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        SociatyActBean bean = lists.get(position);
        BitmapManager.INSTANCE.loadBitmap(bean.actimage, holder.headImg, R.drawable.loading, true);
        holder.actTitle.setText(bean.actname);
        holder.actContent.setText(bean.actcontent);
        holder.startTiem.setText(bean.begintime);
        holder.endTime.setText(bean.endtime);


        return convertView;
    }

    class Holder {
        public ImageView headImg;
        public TextView actTitle;
        public TextView actContent;
        public TextView startTiem;
        public TextView endTime;
    }
}
