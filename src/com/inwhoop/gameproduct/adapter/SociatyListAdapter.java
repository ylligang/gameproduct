package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.SearchLoopActivity;
import com.inwhoop.gameproduct.acitivity.SearchSociatyActivity;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.BitmapManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zk
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO
 * @date 2014年6月10日 17:20:15
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class SociatyListAdapter extends BaseAdapter {

    public List<SociatyBean> list = null;

    public LayoutInflater inflater = null;

    public Context context;

    public SociatyListAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<SociatyBean>();
        inflater = LayoutInflater.from(context);
    }

    public void add(List<SociatyBean> addlist) {
        for (int i = 0; i < addlist.size(); i++) {
            list.add(addlist.get(i));
        }
        this.notifyDataSetChanged();
    }

    public List<SociatyBean> getList() {
        return list;
    }

    public SociatyBean getItemBean(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        Holder holder = null;
        if (null == convertView) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.loop_fragment_list_item,
                    null);
            holder.headImg = (ImageView) convertView.findViewById(R.id.loop_fragment_list_item_head_img);
            holder.loopTitle = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_title);
            holder.loopDec = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_dec);
            holder.loopId = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_id_num);
            holder.personNum = (TextView) convertView.findViewById(R.id.loop_fragment_list_item_people_num);

            holder.loopId.setVisibility(View.VISIBLE);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        SociatyBean bean = list.get(position);
        holder.loopId.setText("" + bean.id);
        holder.loopTitle.setText(bean.guildname);
        holder.loopDec.setText(bean.guildremark);
        holder.personNum.setText("" + bean.personNum);
        BitmapManager.INSTANCE.loadBitmap(bean.guildheadphoto, holder.headImg, R.drawable.loading, true);
        return convertView;
    }

    class Holder {
        public ImageView headImg;
        public TextView loopTitle;
        public TextView loopDec;
        public TextView loopId;
        public TextView personNum;
    }

}