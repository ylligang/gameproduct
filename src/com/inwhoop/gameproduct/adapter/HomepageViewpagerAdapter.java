package com.inwhoop.gameproduct.adapter;

import java.util.List;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

/**
 * @Project: MainActivity
 * @Title: HomepageViewpagerAdapter.java
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO 首页banner的adapter
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-3 下午1:12:10
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HomepageViewpagerAdapter extends PagerAdapter {

	private List<View> list = null;

	public HomepageViewpagerAdapter(List<View> list) {
		this.list = list;
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		// ((ViewPager) arg0).removeView(list.get(arg1%list.size()));
	}

	@Override
	public int getCount() {
		if(list.size() == 1){
			return list.size();
		}
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return (arg0 == arg1);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		if(list.size()==0){
			return null;
		}
		if(list.size() == 1){
			((ViewPager) container).addView(list.get(position), 0);
			return list.get(position);
		}
		if (list.get(position%list.size()).getParent() == null) {
			((ViewPager) container).addView(list.get(position% list.size()), 0);
		} else {
			((ViewGroup) list.get(position% list.size()).getParent()).removeView(list
					.get(position% list.size()));
			((ViewPager) container).addView(list.get(position% list.size()), 0);
		}
		return list.get(position % list.size());
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

}
