package com.inwhoop.gameproduct.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.Gift;

import java.util.List;

/**
 * @Describe: TODO  已获取礼包 消息界面。适配器
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/04 18:12
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class GetGiftBagOkAdapter extends BaseAdapter {


    private final List<Gift> giftLists;
    private LayoutInflater inflater;


    public GetGiftBagOkAdapter(Context context, List<Gift> giftLists) {
        this.inflater = LayoutInflater.from(context);
        this.giftLists = giftLists;
    }

    @Override
    public int getCount() {
        return giftLists.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (null == convertView) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_get_gift_ok, null);
            holder.title = (TextView) convertView.findViewById(R.id.textView_title);
            holder.info = (TextView) convertView.findViewById(R.id.textView_info);
            holder.delete = (Button) convertView.findViewById(R.id.item_right);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Gift bean = giftLists.get(position);
        holder.title.setText(bean.name);
        holder.info.setText(bean.cd_key);

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                if (holder.delete.getVisibility() == View.GONE)
////                    holder.delete.setVisibility(View.VISIBLE);
////                else
////                    holder.delete.setVisibility(View.GONE);
//            }
//        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                giftLists.remove(position);
//                notifyDataSetChanged();
//                holder.delete.setVisibility(View.GONE);

                if (mListener != null) {
                    mListener.onRightItemClick(v, position);
                    holder.delete.setVisibility(View.GONE);
                }
            }
        });

        return convertView;
    }


    class ViewHolder {
        TextView title;
        TextView info;
        Button delete;
    }

    /**
     * 单击事件监听器
     */
    private onRightItemClickListener mListener = null;

    public void setOnRightItemClickListener(onRightItemClickListener listener) {
        mListener = listener;
    }

    public interface onRightItemClickListener {
        void onRightItemClick(View v, int position);
    }
}