package com.inwhoop.gameproduct.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import com.inwhoop.gameproduct.fragment.GiftItemFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO
 * @date 2014/4/16   20:12
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GiftViewPagerAdapters extends FragmentPagerAdapter {

    private List<Fragment> fragmentsList;

    public GiftViewPagerAdapters(FragmentManager fm) {
        super(fm);
    }

    public GiftViewPagerAdapters(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragmentsList = fragments;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public Fragment getItem(int arg0) {
        return fragmentsList.get(arg0);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}
