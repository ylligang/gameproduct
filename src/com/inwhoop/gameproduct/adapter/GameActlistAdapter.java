package com.inwhoop.gameproduct.adapter;

import java.util.List;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.Actvitieslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.utils.BitmapManager;

/**
 * @Project: MainActivity
 * @Title: SubscribeLeftlistAdapter.java
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO 全部游戏的listview的适配器
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-4 下午3:28:11
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GameActlistAdapter extends BaseAdapter {

	public List<Actvitieslist> list = null;

	public LayoutInflater inflater = null;

	public Context context;

	public GameActlistAdapter(Context context, List<Actvitieslist> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
	}
	
	public void add(List<Actvitieslist> addlist){
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}
	
	public List<Actvitieslist> getAll(){
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == convertView) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.gameact_list_item,
					null);
			holder.img = (ImageView) convertView.findViewById(R.id.img);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.introduce = (TextView) convertView.findViewById(R.id.type);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					MyApplication.widthPixels / 8*3,
					MyApplication.widthPixels / 32*9);
			holder.imgrela = (RelativeLayout) convertView
					.findViewById(R.id.imgrela);
			holder.imgrela.setLayoutParams(parm);
			convertView.setTag(holder);
		}else{
			holder = (Holder) convertView.getTag();
		}
		Actvitieslist bean = list.get(position);

		holder.title.setText(bean.actName);
		holder.introduce.setText(bean.actInfo);

        BitmapManager.INSTANCE.loadBitmap(bean.imgUrl,holder.img,R.drawable.loading,true);

		return convertView;
	}

	class Holder {
		ImageView img;
		TextView title,introduce;
		RelativeLayout imgrela;
	}

}
