package com.inwhoop.gameproduct.xmpp;

import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smackx.muc.MultiUserChat;

import com.inwhoop.gameproduct.adapter.GroupChatlistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.db.HandleFriendchatlistRecordDB;
import com.inwhoop.gameproduct.db.HandleGroupchatNoReadRecordDB;
import com.inwhoop.gameproduct.db.HandleGroupchatRecordDB;
import com.inwhoop.gameproduct.entity.GroupChatinfo;
import com.inwhoop.gameproduct.entity.Groups;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.utils.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;

/**
 * @Project: WZSchool
 * @Title: ClientGroupServer.java
 * @Package com.wz.util
 * @Description: TODO 群聊监听
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-3 下午1:42:27
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ClientGroupServer {

	public static ClientGroupServer client = null;

	public static Context contxet = null;

	private HandleGroupchatRecordDB db = null;
	
	private HandleGroupchatNoReadRecordDB nodb = null;
	
	private HandleFriendchatlistRecordDB fdb = null;

	private GroupChatlistAdapter adapter = null;

	public boolean isChat = false;

	private String groupid;

	private ListView listView;
	
	private List<MultiUserChat> mulist = new ArrayList<MultiUserChat>();
	
	private List<Groups> list = new ArrayList<Groups>();

	public static ClientGroupServer getInstance(Context mContext) {
		contxet = mContext;
		if (null == client) {
			client = new ClientGroupServer();
		}
		return client;
	}

	private ClientGroupServer() {
		db = new HandleGroupchatRecordDB(contxet);
		fdb = new HandleFriendchatlistRecordDB(contxet);
		nodb = new HandleGroupchatNoReadRecordDB(contxet);
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getGroups(UserInfoUtil.getUserInfo(contxet).id);
					initMultiUserChat();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				initHandler.sendMessage(msg);
			}
		}).start();
	}
	
	private Handler initHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MyApplication.READ_SUCCESS:
//				initMultiUserChat();
				break;

			case MyApplication.READ_FAIL:
				client = null;
				break;

			default:
				break;
			}
		};
	};
	
	private void initMultiUserChat() {
		MypacketListener.getInstance().setgHandler(handler);
		for (int i = 0; i < list.size(); i++) {
			try{
				MultiUserChat mu = XmppConnectionUtil.getInstance()
						.joinMultiUserChat(UserInfoUtil.getUserInfo(contxet).chatId,
								list.get(i).groupid, "");
				mulist.add(mu);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public void addMultiuserchat(String groupid,String groupname,MultiUserChat mu){
		Groups groups = new Groups();
		groups.groupid = groupid;
		groups.groupname = groupname;
		list.add(groups);
		mulist.add(mu);
	}
	
	public void addMultiuserchat(String groupid,String groupname){
		Groups groups = new Groups();
		groups.groupid = groupid;
		groups.groupname = groupname;
		list.add(groups);
		MultiUserChat mu = XmppConnectionUtil.getInstance()
				.joinMultiUserChat(UserInfoUtil.getUserInfo(contxet).chatId,
						groupid, "");
		mulist.add(mu);
	}
	
	public void noExistToJoin(String groupid,String groupname){
		boolean flag = true;
		for (int i = 0; i < list.size(); i++) {
			if(groupid.equals(list.get(i).groupid)){
				flag = false;
				break;
			}
		}
		if(flag){
			Groups groups = new Groups();
			groups.groupid = groupid;
			groups.groupname = groupname;
			list.add(groups);
			MultiUserChat mu = XmppConnectionUtil.getInstance()
					.joinMultiUserChat(UserInfoUtil.getUserInfo(contxet).chatId,
							groupid, "");
			mulist.add(mu);
		}
	}

	public void set(String groupid, GroupChatlistAdapter adapter, ListView listView) {
		this.groupid = groupid;
		this.adapter = adapter;
		this.listView = listView;
	}
	
	private Handler handler = new Handler() {
		@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyApplication.MSG_BODY:
				String from = msg.getData().getString("from");
//				if (!from.toLowerCase().equals(UserInfoUtil.getUserInfo(contxet).chatId.toLowerCase())) {
					GroupChatinfo info;
					try {
						info = GroupChatPacketExtension.getBosy(msg.getData()
								.getString("body"));
						if(!info.chatid.toLowerCase().equals(UserInfoUtil.getUserInfo(contxet).chatId.toLowerCase())){
							info.isMymsg = false;
							info.userid = UserInfoUtil.getUserInfo(contxet).id;
							if(info.msgtype == 3){
								Utils.decoderBase64File(info.content,  Environment.getExternalStorageDirectory()
									+info.audiopath);
							}
							if(!fdb.isExist(info.groupid,2,info.time, "" + UserInfoUtil.getUserInfo(contxet).id)){
								fdb.saveGmember(info.groupid,2,info.time, "" + UserInfoUtil.getUserInfo(contxet).id,info.userheadpath,info.groupname);
							}else{
								fdb.UpdateChatagetTime(info.groupid,2, "" + UserInfoUtil.getUserInfo(contxet).id,info.time,info.userheadpath,info.groupname);
							}
							if (isChat && info.groupid.equals(groupid)) {
								adapter.addChatinfo(info);
								adapter.notifyDataSetChanged();
								listView.setSelection(listView.getCount() - 1);
								saveChatinfo(info);
							}else{
								nodb.saveGmember(info);
								if(MyApplication.messagelistishiden){
									Intent intent = new Intent();
									intent.setAction(MyApplication.ACTION_MESSAGE);
									contxet.sendBroadcast(intent);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
//				}
				break;
			}

		}
	};

	private void saveChatinfo(GroupChatinfo info) {
		db.saveGmember(info);
	}
	
	public MultiUserChat getChatMu() {
		if (null != list && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if (groupid.equals(list.get(i).groupid)) {
					if(i<mulist.size()){
						return mulist.get(i);
					}
					return null;
				}
			}
		}
		return null;
	}

}
