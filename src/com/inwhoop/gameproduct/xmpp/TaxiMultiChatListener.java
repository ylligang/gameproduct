package com.inwhoop.gameproduct.xmpp;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;

import com.inwhoop.gameproduct.application.MyApplication;

import android.os.Bundle;
import android.os.Handler;

public class TaxiMultiChatListener implements PacketListener {
	private Handler handler;

	public TaxiMultiChatListener(Handler handler) {
		this.handler = handler;
	}

	@Override
	public void processPacket(Packet packet) {
		Message message = (Message) packet;
		if (!message
				.getFrom()
				.toString()
				.substring(message.getFrom().indexOf("@")+1,
						message.getFrom().lastIndexOf("/"))
				.equals(XmppConnectionUtil.getInstance().getConnection()
						.getServiceName())) {
			System.out.println("=========getInstance==========");
			System.out.println("========message.getBody()======="+message.getBody());
			String body = message.getBody();
			String from = message.getFrom();
			int index = from.lastIndexOf("/");
			from = from.substring(index + 1, from.length());
			android.os.Message msg = handler.obtainMessage();
			Bundle bundle = new Bundle();
			bundle.putString("body", body);
			bundle.putString("from", from);
			msg.setData(bundle);
			msg.what = MyApplication.MSG_BODY;
			handler.sendMessage(msg);
		}
	}

}
