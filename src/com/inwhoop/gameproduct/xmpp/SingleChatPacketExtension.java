
package com.inwhoop.gameproduct.xmpp;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.jivesoftware.smack.packet.PacketExtension;
import org.xmlpull.v1.XmlPullParser;

import com.inwhoop.gameproduct.entity.FriendChatinfo;
import com.inwhoop.gameproduct.utils.TimeRender;

import android.util.Xml;


/**
 * 单聊
 * @Project: MainActivity
 * @Title: MyPacketExtension.java
 * @Package com.inwhoop.gameproduct.xmpp
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-16 下午3:18:42
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SingleChatPacketExtension implements PacketExtension {
	public static final String ELEMENT = "body";
	public static final String NAMESPACE = "HaHaMyPackExtension.com";
	
	public String usernick;

	private String content;

	private String sendtime;
	
	private int msgtype;
	
	private String audiopath;
	
	private String headpath;
	
	private int audiolength;
	
	public void setAudiolength(int audiolength) {
		this.audiolength = audiolength;
	}

	public void setUsernick(String usernick) {
		this.usernick = usernick;
	}

	public void setHeadpath(String headpath) {
		this.headpath = headpath;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public void setAudiopath(String audiopath){
		this.audiopath = audiopath;
	}

	@Override
	public String getElementName() {
		return ELEMENT;
	}

	public String getSendtime() {
		return sendtime;
	}

	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}

	public int getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(int msgtype) {
		this.msgtype = msgtype;
	}

	@Override
	public String getNamespace() {
		return NAMESPACE;
	}
	
	@Override
	public String toXML() {
		String sb = createContinueString(
				"<",
				ELEMENT,
				" usernick=\"",""+usernick,"\"",
				" msgtype=\"",""+msgtype,"\"",
				" audiopath=\"",""+audiopath,"\"",
				" sendtime=\"",getSendtime(),"\"",
				" headpath=\"",""+headpath,"\"",
				" audiolength=\"",""+audiolength,"\"",
				">", getContent(), "</", ELEMENT, ">");
		return sb;
	}

	// 连续字符串
	public static String createContinueString(String... strings) {
		StringBuffer sb = new StringBuffer();
		for (String string : strings) {
			sb.append(string);
		}
		return sb.toString();
	}
	
	public static FriendChatinfo getBosy(String result) throws Exception{
		FriendChatinfo info = new FriendChatinfo();
		InputStream is = null;
		is = new ByteArrayInputStream(result.getBytes("UTF-8"));
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(is, "UTF-8");
		int event = parser.getEventType();
		while (event != XmlPullParser.END_DOCUMENT) {
			switch (event) {
			case XmlPullParser.START_DOCUMENT:
				break;
			case XmlPullParser.START_TAG:
				if ("body".equals(parser.getName())) {
					info.usernick = parser.getAttributeValue(0);
					info.msgtype = Integer.parseInt(parser.getAttributeValue(1));
					info.audiopath = parser.getAttributeValue(2);
				    info.time = parser.getAttributeValue(3);
					info.userheadpath = parser.getAttributeValue(4);
					info.audiolength = Integer.parseInt(parser.getAttributeValue(5));
					info.content = parser.nextText();
				}
				break;
			case XmlPullParser.END_TAG:
				break;
			}
			event = parser.next();
		}
		return info;
	}
	
//	public static FriendChatinfo getgrBosy(String result) throws Exception{
//	
//		FriendChatinfo info = new FriendChatinfo();
//		InputStream is = null;
//		is = new ByteArrayInputStream(result.getBytes("UTF-8"));
//		XmlPullParser parser = Xml.newPullParser();
//		parser.setInput(is, "UTF-8");
//		int event = parser.getEventType();
//		while (event != XmlPullParser.END_DOCUMENT) {
//			switch (event) {
//			case XmlPullParser.START_DOCUMENT:
//				break;
//			case XmlPullParser.START_TAG:
//				if ("body".equals(parser.getName())) {
//					info.username = parser.getAttributeValue(0);
//					info.msgtype = Integer.parseInt(parser.getAttributeValue(1));
//				    info.time = TimeRender.getStandardDate();
//					info.userheadpath = parser.getAttributeValue(3);
//					info.content = parser.nextText();
//				}
//				break;
//			case XmlPullParser.END_TAG:
//				break;
//			}
//			event = parser.next();
//		}
//		return info;
//	}

}
