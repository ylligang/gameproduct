package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.utils.Utils;
import com.inwhoop.gameproduct.view.CustomProgressDialog;
import com.inwhoop.gameproduct.view.ScoreDialog;

/**
 * Activity基类
 * 
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: BaseActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * @date 2014-4-3 下午2:24:47
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class BaseActivity extends ActivityGroup {
	/**
	 * 标题栏第一个按钮，相对靠左一点的按钮 标题栏左侧按钮或者文字
	 */
	public RelativeLayout head_left = null;
	private TextView head_left_bt = null;
	/**
	 * 标题栏第一个按钮，相对靠左一点的按钮
	 */
	public RelativeLayout head_right_first = null;
	public TextView head_right_bt_1 = null;
	/**
	 * 标题栏最右边的按钮
	 */
	public RelativeLayout head_right_second = null;
	public TextView head_right_bt_2 = null;
	/**
	 * 标题
	 */
	private TextView titleView = null;

	protected Context mContext = null;

	// public ProgressDialog progressDialog;// 加载进度框

	public CustomProgressDialog progressDialog = null;

	/**
	 * 控件初始化
	 * 
	 * @Title: init
	 * @Description: TODO
	 */
	public void init() {
		head_left = (RelativeLayout) findViewById(R.id.head_left);
		head_left.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		head_left_bt = (TextView) findViewById(R.id.head_left_button);
		head_right_first = (RelativeLayout) findViewById(R.id.head_right_first);
		head_right_second = (RelativeLayout) findViewById(R.id.head_right_second);
		head_right_bt_2 = (TextView) findViewById(R.id.head_right_bt_2);
		head_right_bt_1 = (TextView) findViewById(R.id.head_right_bt_1);
		titleView = (TextView) findViewById(R.id.head_title);
	}

	/**
	 * 设置左边按钮显示图片
	 * 
	 * @param imgId
	 *            void
	 * 
	 * @Title: setHeadLeftButton
	 * @Description: TODO
	 */
	public void setHeadLeftButton(int imgId) {
		head_left.setVisibility(View.VISIBLE);
		head_left_bt.setBackgroundResource(imgId);
	}

	/**
	 * 设置左边显示文字
	 * 
	 * @param textId
	 *            void
	 * 
	 * @Title: setHeadLeftText
	 * @Description: TODO
	 */
	public void setHeadLeftText(int textId) {
		head_left.setVisibility(View.VISIBLE);
		head_left_bt.setText(textId);
	}

	/**
	 * 设置标题
	 * 
	 * @param textId
	 *            void
	 * 
	 * @Title: setTitle
	 * @Description: TODO
	 */
	public void setTitle(int textId) {
		titleView.setText(textId);
	}

	/**
	 * 设置标题
	 * 
	 * @param text
	 *            void
	 * 
	 * @Title: setTitle
	 * @Description: TODO
	 */
	public void setStringTitle(String text) {
		titleView.setText(text);
	}

	/**
	 * 设置右边第一个按钮
	 * 
	 * @param imgId
	 *            void
	 * 
	 * @Title: setRightFirstBt
	 * @Description: TODO, 注意必须同时显示右边第二个按钮，不然无法显示
	 */
	public void setRightFirstBt(int imgId) {
		head_right_first.setVisibility(View.VISIBLE);
		head_right_bt_1.setBackgroundResource(imgId);
	}

	/**
	 * 设置右边第二个按钮
	 * 
	 * @param imgId
	 *            void
	 * 
	 * @Title: setRightSecondBt
	 * @Description: TODO
	 */
	public void setRightSecondBt(int imgId) {
		head_right_second.setVisibility(View.VISIBLE);
		head_right_bt_2.setBackgroundResource(imgId);
	}

	/*
	 * 设置右边第二个按钮的文字
	 */
	public void setRightSecondBtText(String str) {
		head_right_second.setVisibility(View.VISIBLE);
		head_right_bt_2.setText(str);
	}

	/**
	 * 获取左边按钮
	 * 
	 * @return RelativeLayout
	 * 
	 * @Title: getLeftBt
	 * @Description: TODO
	 */
	public RelativeLayout getLeftBt() {
		return head_left;
	}

	/**
	 * 获取右边第一个按钮
	 * 
	 * @return RelativeLayout
	 * 
	 * @Title: getRightFirstBt
	 * @Description: TODO
	 */
	public RelativeLayout getRightFirstBt() {
		return head_right_first;
	}

	/**
	 * 获取右边第二个按钮
	 * 
	 * @return RelativeLayout
	 * 
	 * @Title: getRightSecondBt
	 * @Description: TODO
	 */
	public RelativeLayout getRightSecondBt() {
		return head_right_second;
	}

	/**
	 * 显示进度框
	 * 
	 * @param @param msg
	 * 
	 * @return void
	 * 
	 * @Title: showProgressDialog
	 * @Description: TODO
	 */
	protected void showProgressDialog(String msg) {
		if (progressDialog == null) {
			progressDialog = CustomProgressDialog.createDialog(this);
		}
		progressDialog.setMessage(msg);
		progressDialog.setCancelable(true);

		progressDialog.show();
	}

	/**
	 * 关闭进度框
	 * 
	 * @param
	 * 
	 * @return void
	 * 
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 显示toast
	 * 
	 * @param @param msg
	 * 
	 * @return void
	 * 
	 * @Title: showToast
	 * @Description: TODO
	 */
	protected void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

	public static final int CAMERA = 1102;// 手机拍摄
	public static final int GALLEY = 1101;// 手机相册
	public static final int PHOTO_REQUEST_CUT = 1104;// 裁剪之后

	protected static final int RESULT_IMG = 1108;// 返回不裁剪
	protected static final int RESULT_CUT_IMG = 1109;// 返回裁剪

	private Handler handler;
	private boolean isCut = false;
	private int cutW = 200, cutH = 200;
	private Uri cameraUri;
	private boolean isScale = true;

	/**
	 * TODO 以下为调用系统相机代码，目前bug：传true切图，照相的话问题。 TODO
	 * 返回值为图片路径，ImageView.setImageBitmap(BitmapFactory.decodeFile(""+msg.obj));
	 * TODO 如果需要压缩： Utils.compressImage(""+msg.obj);方法即可
	 * 
	 * @param handler
	 *            需要hanlder接收数据:RESULT_IMG/RESULT_CUT_IMG，
	 * @param isCut
	 *            是否裁剪
	 * @param cutW
	 *            isCut为真才有效，裁剪宽度
	 * @param cutH
	 *            裁剪高度
	 * @param isScale
	 *            是否固定裁剪的比例,true表固定比例，传参cutW和cutH才有效
	 */
	protected void showCameraDialog(Handler handler, final boolean isCut,
			int cutW, int cutH, boolean isScale) {
		this.handler = handler;
		this.isCut = isCut;
		this.cutW = cutW;
		this.cutH = cutH;
		this.isScale = isScale;
		String sdState = Environment.getExternalStorageState();
		if (!sdState.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
			showToast("sd卡不存在");
			return;
		}
		final ScoreDialog dialog = new ScoreDialog(this,
				R.layout.select_camera_dialog, R.style.dialog_more_style);
		dialog.setParamsBotton();
		dialog.show();
		dialog.findViewById(R.id.select_camera_dialog_camera)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						dialog.dismiss();
						Intent camera = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						cameraUri = Utils.createImagePathUri(mContext);
						camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
						startActivityForResult(camera, CAMERA);
					}
				});
		dialog.findViewById(R.id.select_camera_dialog_album)
				.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						dialog.dismiss();
						Intent picture = new Intent(
								Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
						startActivityForResult(picture, GALLEY);
					}
				});
		dialog.findViewById(R.id.select_camera_dialog_back).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						dialog.dismiss();
					}
				});

		Window dialogWindow = dialog.getWindow();
		dialogWindow.setWindowAnimations(R.style.dialog_more_style);
	}

	@SuppressWarnings("static-access")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		String picpath = "";
		Message msg = new Message();

		if (requestCode == CAMERA && resultCode == Activity.RESULT_OK
				&& null == data) {
			if (isCut) {
				startPhotoZoom(cameraUri);
			} else {
				msg.what = RESULT_IMG;
				msg.obj = Utils.uriToPath(mContext, cameraUri);
				handler.sendMessage(msg);
			}
		} else if (requestCode == GALLEY && resultCode == Activity.RESULT_OK
				&& null != data) {
			Uri selectedImage = data.getData();
			String[] filePathColumns = { MediaStore.Images.Media.DATA };
			Cursor c = this.getContentResolver().query(selectedImage,
					filePathColumns, null, null, null);
			c.moveToFirst();
			int columnIndex = c.getColumnIndex(filePathColumns[0]);
			String picturePath = c.getString(columnIndex);
			picpath = picturePath;
			c.close();

			if (isCut) {
				// startPhotoZoom(Utils.pathToUri(mContext,picpath));
				startPhotoZoom(selectedImage);
			} else {
				msg.what = RESULT_IMG;
				msg.obj = picpath;
				handler.sendMessage(msg);
			}
		} else if (requestCode == PHOTO_REQUEST_CUT
				&& resultCode == Activity.RESULT_OK && null != data) {
			Bitmap bmp = data.getExtras().getParcelable("data");
			msg.what = RESULT_CUT_IMG;
			msg.obj = bmp;
			handler.sendMessage(msg);
		}
	}

	private void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// crop为true是设置在开启的intent中设置显示的view可以剪裁
		intent.putExtra("crop", "true");
		if (isScale) { // 目前bug，拍照不能自由比例
			// aspectX aspectY 是宽高的比例
			intent.putExtra("aspectX", cutW);
			intent.putExtra("aspectY", cutH);
		}
		// outputX,outputY 是剪裁图片的宽高,不能设置过大
		intent.putExtra("outputX", cutW);
		intent.putExtra("outputY", cutH);

		intent.putExtra("return-data", true);
		startActivityForResult(intent, PHOTO_REQUEST_CUT);
	}

	/**
	 * 自定义Dialog
	 * 
	 * @Title: buildDialog
	 * @Description: TODO
	 * @param @param dialogLinsener 自定义接口
	 * @param @param title 标题
	 * @param @param messgae 信息
	 * @param @return
	 * @return Dialog
	 */
	public void buildDialog(final DialogLinsener dialogLinsener, int title,
			int messgae) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.custom_dialog, null);
		Button ok = (Button) view.findViewById(R.id.dialog_button_ok);
		Button cancle = (Button) view.findViewById(R.id.dialog_button_cancle);
		TextView titleView = (TextView) view.findViewById(R.id.dialog_title);
		TextView msgView = (TextView) view.findViewById(R.id.dialog_message);
		titleView.setText(title);
		msgView.setText(messgae);
		final Dialog dialog = new Dialog(mContext, R.style.dialog_more_style);
		dialog.setContentView(view);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				dialogLinsener.onSure();
			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				dialogLinsener.onCancle();
			}
		});

	}
}
