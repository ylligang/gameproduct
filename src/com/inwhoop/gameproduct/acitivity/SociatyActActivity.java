package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.SociatyActAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.SociatyActBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.XListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO   公会活动
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/11 9:56
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SociatyActActivity extends BaseActivity implements XListView.IXListViewListener {
    private XListView xListview;
    private SociatyActAdapter adapter;
    private SociatyBean sociatyActBean = new SociatyBean();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sociaty_act);
        mContext = SociatyActActivity.this;
        SociatyBean bean = (SociatyBean) getIntent().getSerializableExtra("bean");
        if (null != bean) sociatyActBean = bean;

        initData();

        initSociatyAct("-1", sociatyActBean.id, 0);
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.sociaty_activity);
        if (UserInfoUtil.getUserInfo(mContext).id.equals(""+sociatyActBean.userid)) {
        	setRightSecondBt(R.drawable.btn_add);
            getRightSecondBt().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                	Intent intent = new Intent(mContext,CreateActActivity.class);
                    Bundle b=new Bundle();
                    b.putSerializable("bean", sociatyActBean);
                    intent.putExtras(b);
                    startActivityForResult(intent, 100);
                }
            });
        }
        xListview = (XListView) findViewById(R.id.listView);
        xListview.setPullLoadEnable(true);
        xListview.setPullRefreshEnable(true);
        adapter = new SociatyActAdapter(mContext);
        xListview.setAdapter(adapter);
        xListview.setXListViewListener(this);
        xListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0)return;
                Bundle b=new Bundle();
                b.putSerializable("bean",adapter.getLists().get(position-1)); //注意减1
                MyToast.showShort(mContext,"活动"+(position-1));
            }
        });
    }

    @Override
    public void onRefresh() {
        initSociatyAct("-1", sociatyActBean.id, 0);
    }

    @Override
    public void onLoadMore() {
        ArrayList<SociatyActBean> xxlist = adapter.getLists();
        if (xxlist.size() > 0)
            initSociatyAct("" + xxlist.get(xxlist.size() - 1).id, sociatyActBean.id, 1);
    }

    //TODO  拉取公会活动
    private void initSociatyAct(final String minId, final int sociatyId, final int tag) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getSociatyActList(sociatyId, minId);
                    if (tag == 0)
                        msg.what = MyApplication.REFRESH_SUCCESS;
                    else if (tag == 1)
                        msg.what = MyApplication.LOAD_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(resultCode == 200){
    		SociatyActBean bean = (SociatyActBean) data.getSerializableExtra("bean");
    		adapter.addInfo(bean);
    		adapter.notifyDataSetChanged();
    	}
    }

    @SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            List<SociatyActBean> newList = (List<SociatyActBean>) msg.obj;
            xListview.stopLoadMore();
            xListview.stopRefresh();
            switch (msg.what) {
                case MyApplication.FAIL:
                    MyToast.showShort(mContext, "获取数据失败");
                    xListview.setPullLoadEnable(false);
                    break;
                case MyApplication.REFRESH_SUCCESS:
                    if (newList.size() == 0)
                        MyToast.showShort(mContext, "暂无活动哦~");
                    else {
                        adapter.getLists().clear();
                        adapter.addList(newList);
                    }

                    if (newList.size() < 10)
                        xListview.setPullLoadEnable(false);
                    break;
                case MyApplication.LOAD_SUCCESS:
                    if (newList.size() < 10)
                        xListview.setPullLoadEnable(false);

                    adapter.addList(newList);
                    break;
            }
        }
    };
}
