package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameNews;
import com.inwhoop.gameproduct.onekeyshare.Share;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.SyncHttp;

import java.io.UnsupportedEncodingException;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO   游戏资讯页面
 * @date 2014/4/19   11:54
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GameNewsActivity extends BaseActivity {
    private RelativeLayout back;
    private TextView back_text;
    private TextView title;
    private RelativeLayout share;
    private TextView share_text;
    private String newsId = "";
    private GameNews gameNews = null;
    private WebView webView;
    private String newsName = "";
    private TextView name;
    private TextView time;
//    private ProgressBar progressBar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_news_activity);
        ShareSDK.initSDK(this);
        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            newsId = bundle.getString("newsid");
            newsName = bundle.getString("newsName");
        }
        init();
        webView = (WebView) findViewById(R.id.game_news_activity_web);
        name = (TextView) findViewById(R.id.game_news_activity_name);
        time = (TextView) findViewById(R.id.game_news_activity_time);
        name.setText(newsName);
//        progressBar = (ProgressBar) findViewById(R.id.game_news_activity_bar);
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.game_news);
        setRightSecondBt(R.drawable.share);
        showProgressDialog("正在加载...");
        setClick();
        getGameNews();

    }

    private void setClick() {
        getRightSecondBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Share.showShare(GameNewsActivity.this, getResources().getString(R.string.app_name), "");
            }
        });
    }

    private void getGameNews() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/gamenewsservice_getGamenewsByNewsid.action?newsId=" + newsId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    System.out.println("json==获取到的服务器的数据为" + jsonData);
//                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
//                    Log.v("json==获取到的服务器的数据为", jsonData);
                    gameNews = JsonUtils.getGameNews(jsonData);
                    if (!gameNews.equals("")) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
//            progressBar.setVisibility(View.GONE);
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    try {
                        time.setText(gameNews.createtime);
                        webView.loadDataWithBaseURL(
                                "fake://not/needed",
                                "<html><head><meta http-equiv='content-type' content='text/html;charset=utf-8'><style type=\"text/css\">img{ width:95%}</style><STYLE TYPE=\"text/css\"> BODY {margin: 3px 0px 0px 0px} </STYLE><BODY TOPMARGIN=5 LEFTMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0></head><body bgcolor=\"#ffffff\" line-height:150%>"
                                        + new String(gameNews.newsremark.getBytes("utf-8"))
                                        + "</body></html>", "text/html", "utf-8", ""
                        );
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(GameNewsActivity.this, getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(GameNewsActivity.this, getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
            dismissProgressDialog();
        }
    };
}