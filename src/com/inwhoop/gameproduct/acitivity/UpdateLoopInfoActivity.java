package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;

/**
 * @Describe: TODO    更新圈子信息，目前有名称
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/26 14:58
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class UpdateLoopInfoActivity extends BaseActivity implements View.OnClickListener {
    private String title = "";

    private String content = "";

    private int id = 0;

    private EditText contentEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upadate_sociaty_info_layout);
        mContext = UpdateLoopInfoActivity.this;
        title = getIntent().getStringExtra("title");
        content = getIntent().getStringExtra("content");
        id = getIntent().getIntExtra("id", 0);

        init();

        KeyBoard.showKeyBoard(contentEditText);
    }

    @Override
    public void init() {
        super.init();
        setStringTitle("" + title);
        setHeadLeftButton(R.drawable.back);
        setRightSecondBtText("完成");
        head_right_second.setOnClickListener(this);
        contentEditText = (EditText) findViewById(R.id.contentedit);
        contentEditText.setText("" + content);
        contentEditText.setSelection(content.length());
        contentEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_NULL:
                        break;
                    case EditorInfo.IME_ACTION_SEND:
                        break;
                    case EditorInfo.IME_ACTION_DONE:
                        Update();
                        break;
                    case EditorInfo.IME_ACTION_GO:
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_right_second:
                Update();
                break;
            default:
                break;
        }
    }

    private void Update() {
        content = contentEditText.getText().toString().trim();
        if ("".equals(content)) {
            showToast("请填写修改内容");
            return;
        }
        showProgressDialog("修改中...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (isName()) { //圈子名称
                        Name name = new Name();
                        name.circleid = id;
                        name.circlename = content;
                        msg.obj = JsonUtils.updateLoop(name);
                    } else if (isRemark()) { //圈子说明
                        Remark remark = new Remark();
                        remark.circleid = id;
                        remark.circleremark = content;
                        msg.obj = JsonUtils.updateLoop(remark);
                    }
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private boolean isRemark() {
        return title.equals(getResources().getString(R.string.loop_instructions));
    }

    private boolean isName() {
        return title.equals(getResources().getString(R.string.loop_name));
    }

    class Name {
        public int circleid = 0;
        public String circlename = "";
    }

    class Remark {
        public int circleid = 0;
        public String circleremark = "";
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    showToast("失败了%>_<%，稍后再试吧");
                    break;

                case MyApplication.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[1]) {
                        showToast("修改成功O(∩_∩)O~~");
                        Intent intent = new Intent();
                        intent.putExtra("content", content);

                        if (isName()) { //圈子名称
                            setResult(100, intent);
                        } else if (isRemark()) { //圈子说明
                            setResult(101, intent);
                        }

                        finish();
                    } else {
                        showToast("修改失败%>_<%~~");
                    }
                    break;

                default:
                    break;
            }
        }

        ;
    };

    @SuppressWarnings("deprecation")
    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

}
