package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GamelistAdapter;
import com.inwhoop.gameproduct.adapter.MyGridViewAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.db.HandleGameinfoDB;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.view.HeightFitGridView;
import com.inwhoop.gameproduct.view.MyListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO  游戏搜索界面 . ;【H】方法db.getInterestingGameinfo传参需修改成4?反正是推荐放这里
 * * * *    TODO 开始不弹软键盘，edittext父布局：android:focusable="true"  android:focusableInTouchMode="true"
 * TODO 还是要做刷新
 * ****** Created by ZK ********
 * @Date: 2014/04/16 19:24
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SearchGameActivity extends BaseActivity implements View.OnClickListener, MyListView.OnRefreshListener, TextView.OnEditorActionListener {

    private static final int INTERSTING_OK = 132;
    private Context context = SearchGameActivity.this;

    private MyGridViewAdapter gridAdapter;

    private View isVisibilityLayout;//当搜索游戏后就隐藏 包含gridview 的layout

    private List<GameInfo> interest_lists = new ArrayList<GameInfo>();  //感兴趣游戏，数据库

    private EditText importET;

    private List<GameInfo> searched_one_page_lists = new ArrayList<GameInfo>();

    private GamelistAdapter listAdapter;

    private MyListView myListView;

    private HandleGameinfoDB db = new HandleGameinfoDB(context);//数据库
    private String searchStr = "";//搜索的字符串。点击搜藏按钮后设置
    private int tag;//标识从哪个界面跳转而来

    @Override
    protected void onStop() {
        super.onStop();
        KeyBoard.HiddenInputPanel(mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_game);
        tag = getIntent().getIntExtra("tag", 0);

        initData();

//        setGridViewDBData(); //不要了，读取网络的

        setGridViewNetData();
    }

    /**
     * TODO 初始化数据
     */
    private void initData() {
        mContext = context;
        View backBtn = findViewById(R.id.head_left);
        backBtn.setOnClickListener(this);

        View searchBtn = findViewById(R.id.head_right_second);
        searchBtn.setOnClickListener(this);

        HeightFitGridView myGridView = (HeightFitGridView) findViewById(R.id.search_my_gridView);
        gridAdapter = new MyGridViewAdapter(context, interest_lists);
        myGridView.setAdapter(gridAdapter);
        myGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle b = new Bundle();
                b.putSerializable("bean", interest_lists.get(position));
                Act.toAct(context, GameInfosActivity.class, b);
            }
        });

        importET = (EditText) findViewById(R.id.search_et);
        importET.setOnEditorActionListener(this);

        isVisibilityLayout = findViewById(R.id.isVisibility_layout);
        if (tag == CreateSociatyActivity.SEARCH_GAME_RESULT){
            isVisibilityLayout.setVisibility(View.GONE);
            findViewById(R.id.main_layout).setFocusable(false);
            KeyBoard.showKeyBoard(importET);
        }

        myListView = (MyListView) findViewById(R.id.my_listview);
        listAdapter = new GamelistAdapter(context, searched_one_page_lists);
        myListView.setAdapter(listAdapter);
        myListView.setRefreshListener(this);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                Bundle b = new Bundle();
                b.putSerializable("bean", listAdapter.list.get(position));
                if (tag != CreateSociatyActivity.SEARCH_GAME_RESULT)
                    Act.toAct(context, GameInfosActivity.class, b);
                else {
                    intent.putExtras(b);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    /**
     * TODO 设置gridview数据。从数据库查出
     */
    private void setGridViewDBData() {
        List<GameInfo> newList = db.getInterestingGameinfo(MyApplication.TYPE_INTEREST);

        for (int i = 0; i < newList.size() && i < 8; i++) {
            interest_lists.add(newList.get(i));
        }
        gridAdapter.notifyDataSetChanged();
    }

    /**
     * TODO 点击事件
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_left:
                finish();
                KeyBoard.HiddenInputPanel(mContext);
                break;
            case R.id.head_right_second://搜索按钮
                search();
                break;
        }
    }

    //TODO  编译框事件，如果软键盘是【前往】，写的跳转搜索
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        switch (actionId) {
            case EditorInfo.IME_NULL:
                System.out.println("null for default_content: " + v.getText());
                break;
            case EditorInfo.IME_ACTION_SEND:
                System.out.println("action send for email_content: " + v.getText());
                break;
            case EditorInfo.IME_ACTION_DONE:
                System.out.println("action done for number_content: " + v.getText());
                break;
            case EditorInfo.IME_ACTION_GO:
                search();
                break;
            case EditorInfo.IME_ACTION_SEARCH:
                search();
                break;
        }

        return true;
    }

    // TODO 搜索
    private void search() {
        searchStr = importET.getText().toString();
        if ("".equals(searchStr)) {
            MyToast.showShort(context, R.string.please_import);
            return;
        }

        listAdapter.list.clear();//清空前面搜索的list数据
        initSearchData(searchStr);
    }

    /**
     * TODO 点击搜索,有返回搜索的列表，完成后，感兴趣的列表保存到数据库
     *
     * @param str
     */
    private void initSearchData(final String str) {
        showProgressDialog("正在努力加载...");
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
//                    searched_one_page_lists = getSubcrilist(0);
                    searched_one_page_lists = JsonUtils.getGameBySearch(str, "-1");

                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    /**
     * TODO 保存服务器返回的兴趣列表到数据库
     *
     * @param gameInfos
     */
    private void saveInterestingDB(List<GameInfo> gameInfos) {
        for (int i = 0; i < gameInfos.size(); i++) {
            GameInfo bean = gameInfos.get(i);
            bean.gametype = MyApplication.TYPE_INTEREST;
            db.saveGameinfo(bean, MyApplication.TYPE_INTEREST);
        }
    }

    // TODO 上拉加载 .MyListView只有这个刷新
    @Override
    public void onRefresh() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (listAdapter.list.size() == 0) return;
                    searched_one_page_lists = JsonUtils.getGameBySearch(searchStr,
                            listAdapter.list.get(listAdapter.list.size() - 1).gameId + "");
                    msg.what = MyApplication.REFRESH_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    //TODO 服务器感兴趣游戏
    private void setGridViewNetData() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getGameIntersting();
                    msg.what = INTERSTING_OK;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    /**
     * TODO 搜索后listview的处理
     */
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            myListView.onRefreshComplete();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    MyToast.showShort(context, R.string.search_excoption);
                    break;

                case MyApplication.READ_SUCCESS:
                    saveInterestingDB(searched_one_page_lists);
                case MyApplication.REFRESH_SUCCESS:
                    if (searched_one_page_lists.size() < 10) {
                        myListView.isRefreshable = false;
                    }

                    //如果有条数，隐藏推荐
                    if (searched_one_page_lists.size() > 0) {
                        isVisibilityLayout.setVisibility(View.GONE);
                        myListView.setVisibility(View.VISIBLE);
                        KeyBoard.HiddenInputPanel(context);
                    } else MyToast.showShort(context, R.string.search_not_have_one);

                    listAdapter.add(searched_one_page_lists);
                    listAdapter.notifyDataSetChanged();

                    break;
                case INTERSTING_OK:
                    List<GameInfo> newList = (List<GameInfo>) msg.obj;

                    for (int i = 0; i < newList.size() && i < 8; i++) {
                        interest_lists.add(newList.get(i));
                    }
                    gridAdapter.notifyDataSetChanged();
                    break;

                default:
                    break;
            }
        }
    };


//    /**
//     * @param @param  id
//     * @param @return
//     *
//     * @return List<SubscibeInfo>
//     *
//     * @Title: getSubcrilist
//     * @Description: TODO 测试数据
//     */
//    private List<GameInfo> getSubcrilist(int type) {
//        List<GameInfo> list = new ArrayList<GameInfo>();
//        for (int i = 0; i < 10; i++) {
//            GameInfo info = new GameInfo();
//            info.gameId = i + 1;
//            info.collectCount = 15782;
//            info.gameName = "世界" + i;
//            info.gameMode = "角色扮演/武侠/3D";
//            info.gameState = "正在运营";
//            if (i % 2 == 0) {
//                info.imgUrl = "http://ww4.sinaimg.cn/mw600/aa2d33dajw1duio5yf2hzj.jpg";
//                info.isActive = true;
//            }
//            if (i % 3 == 0) {
//                info.imgUrl = "http://www.it.com.cn/audio/mp4/img/2011/04/25/09/110425_cowon_c2_1.jpg";
//                info.isGift = true;
//            }
//            if (i % 4 == 0) {
//                info.imgUrl = "http://www.dabaoku.net/bizhi/youxi/youximm/059cg.jpg";
//                info.isActcode = true;
//            }
//            list.add(info);
//        }
//        return list;
//    }


}
