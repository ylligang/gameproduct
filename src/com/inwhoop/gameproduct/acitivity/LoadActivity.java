package com.inwhoop.gameproduct.acitivity;

import android.content.Context;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.db.GameinfoDatabaseDBhelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.inwhoop.gameproduct.utils.*;

/**
 * TODO 开始等待界面
 */
public class LoadActivity extends Activity {
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.load);
        mContext=LoadActivity.this;
        LoginOpenfireUtil.loginOpenfire(mContext);
        new Thread(new Runnable() {

            @Override
            public void run() {
                GameinfoDatabaseDBhelper d = GameinfoDatabaseDBhelper
                        .getInstance(mContext);
                FaceConversionUtil.getInstace().getFileText(getApplication());
                handler.sendEmptyMessageDelayed(0, 2000);

            }
        }).start();

    }

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            Act.toActClearTop(mContext, MainActivity.class);
            finish();
        }
    };

    @Override
    protected void onRestart() {
        super.onRestart();
        LogUtil.i("LoadActivity,onRestart");
//        Utils.againLogin(mContext);
    }
}
