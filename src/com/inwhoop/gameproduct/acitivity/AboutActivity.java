package com.inwhoop.gameproduct.acitivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.utils.MyToast;

/**
 * @Describe: TODO  关于
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/13 20:49
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class AboutActivity extends BaseActivity implements View.OnClickListener {
    private String telphone;

    @SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        mContext=AboutActivity.this;

        initData();
    }

    private void initData() {
        init();
        setTitle(R.string.about);
        setHeadLeftButton(R.drawable.back);

        TextView appName= (TextView) findViewById(R.id.app_name_and_versionName);
        appName.setText(getResources().getString(R.string.app_name)
                +"v"+getVersionName(mContext));

        telphone=getResources().getString(R.string.about_telephone);
        TextView phone= (TextView) findViewById(R.id.kefurexian_TV);
        phone.setText(telphone);
        findViewById(R.id.rexian_layout).setOnClickListener(this);

        findViewById(R.id.jianchagegnxin_layout).setOnClickListener(this);
        findViewById(R.id.yonghuxieyi_layout).setOnClickListener(this);

    }

    //TODO  得到版本号
    public static String getVersionName(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
        } catch (Exception e) {

        }
        return versionName;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rexian_layout:
                //拨号
                Uri uri = Uri.parse("tel:" + telphone);       //tel:xxxxxx
                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                mContext.startActivity(intent);
                break;
            case R.id.jianchagegnxin_layout:
                MyToast.showShort(mContext,"检查更新暂未开发");
                break;
            case R.id.yonghuxieyi_layout:
                MyToast.showShort(mContext,"用户协议稍后展述");
                break;
        }
    }
}
