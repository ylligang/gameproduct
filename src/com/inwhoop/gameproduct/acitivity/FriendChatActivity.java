package com.inwhoop.gameproduct.acitivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.ChatlistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.db.HandleFriendchatNoReadRecordDB;
import com.inwhoop.gameproduct.db.HandleFriendchatRecordDB;
import com.inwhoop.gameproduct.db.HandleFriendchatlistRecordDB;
import com.inwhoop.gameproduct.entity.FriendChatinfo;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.KeyBoard;
import com.inwhoop.gameproduct.utils.TimeRender;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.utils.Utils;
import com.inwhoop.gameproduct.view.FaceRelativeLayout;
import com.inwhoop.gameproduct.view.TouchSayButton;
import com.inwhoop.gameproduct.view.TouchSayButton.OnRecordListener;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.SingleChatPacketExtension;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

/**
 * @Project: MainActivity
 * @Title: FriendChatActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 单聊
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-6-14 上午10:47:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class FriendChatActivity extends BaseActivity implements
		IXListViewListener, OnClickListener, OnRecordListener {

	private String usernick; // 聊天对象用户昵称

	private String username; // 聊天对象openfire的账号

	private String userphoto;// 聊天对象的头像

	private XListView listView = null;

	private Button sendButton = null; // 发送按钮

	private TouchSayButton audioButton = null; // 按住说话按钮

	private ImageView recordImageView = null; // 录音按钮

	private EditText msgEditText = null; // 输入框

	private boolean isRecording = false;

	private ImageView carmerImageView = null; // 调用照相机

	private List<FriendChatinfo> chatList = new ArrayList<FriendChatinfo>();

	private String msgcontent = ""; // 消息内容

	// private Chat newchat = null; // chat聊天对象

	private ChatlistAdapter adapter = null; // 适配器

	private FriendChatinfo chatinfo = null;

	private HandleFriendchatRecordDB db = null;

	private HandleFriendchatNoReadRecordDB ndb = null;

	private HandleFriendchatlistRecordDB fdb = null;

	private int page = 1; // 用于聊天记录分页

	private FaceRelativeLayout faceRelativeLayout = null;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
						| WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		setContentView(R.layout.friend_chat_layout);
		mContext = FriendChatActivity.this;
		db = new HandleFriendchatRecordDB(mContext);
		ndb = new HandleFriendchatNoReadRecordDB(mContext);
		fdb = new HandleFriendchatlistRecordDB(mContext);
		username = getIntent().getStringExtra("username");
		usernick = getIntent().getStringExtra("usernick");
		userphoto = getIntent().getStringExtra("userphoto");
		init();
	}

	@Override
	public void init() {
		super.init();
		setStringTitle("" + usernick);
		setHeadLeftButton(R.drawable.back);
		listView = (XListView) findViewById(R.id.chatlist);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(false);
		sendButton = (Button) findViewById(R.id.sendbtn);
		sendButton.setOnClickListener(this);
		recordImageView = (ImageView) findViewById(R.id.record_img);
		recordImageView.setOnClickListener(this);
		recordImageView.setVisibility(View.VISIBLE);
		audioButton = (TouchSayButton) findViewById(R.id.audiobtn);
		audioButton.setOnRecordListener(this);
		carmerImageView = (ImageView) findViewById(R.id.carmer_bg);
		carmerImageView.setOnClickListener(this);
		faceRelativeLayout = (FaceRelativeLayout) findViewById(R.id.faceRelativeLayout);
		listView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				faceRelativeLayout.hide();
				return false;
			}
		});
		IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
		filter.addAction(MyApplication.CHAT_AGAIN_INIT);
		registerReceiver(mRecever, filter);
		msgEditText = (EditText) findViewById(R.id.sendmsg);
		// 监听输入框的内容
		msgEditText.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
				if (s.length() > 0) {
					sendButton.setVisibility(View.VISIBLE);
					recordImageView.setVisibility(View.GONE);
					audioButton.setVisibility(View.GONE);
					msgEditText.setVisibility(View.VISIBLE);
				} else {
					sendButton.setVisibility(View.GONE);
					recordImageView.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int arg1, int arg2,
					int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});
		initListviewData();
	}

	/**
	 * 初始化listview
	 * 
	 * @Title: initListview
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void initListviewData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				chatList = db.getUserBygroupid(username,
						UserInfoUtil.getUserInfo(mContext).id, page);
				List<FriendChatinfo> nlist = ndb.getUserBygroupid(username,
						mContext);
				for (int i = 0; i < nlist.size(); i++) {
					chatList.add(nlist.get(i));
					db.saveGmember(nlist.get(i));
				}
				if (null != nlist && nlist.size() > 0) {
					ndb.deleteGmember(UserInfoUtil.getUserInfo(mContext).id,
							username);
				}
				handler.sendEmptyMessage(0);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			page++;
			adapter = new ChatlistAdapter(mContext, chatList, listView);
			listView.setAdapter(adapter);
			listView.setSelection(listView.getCount() - 1);
			ClienConServer.getInstance(mContext).isChat = true;
			ClienConServer.getInstance(mContext).set(adapter.getChatlist(),
					adapter, listView, username);
		};
	};

	@Override
	public void onRefresh() {
		try {
			chatList = db.getUserBygroupid(username,
					UserInfoUtil.getUserInfo(mContext).id, page);
			page++;
			adapter.addChatinfo(chatList);
			adapter.notifyDataSetChanged();
		} catch (Exception e) {
		}
		listView.stopRefresh();
	}

	@Override
	public void onLoadMore() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sendbtn:
			sendMsgcontext();
			break;

		case R.id.record_img:
			if (isRecording) {
				isRecording = false;
				msgEditText.setVisibility(View.VISIBLE);
				audioButton.setVisibility(View.GONE);
				recordImageView.setBackgroundResource(R.drawable.btn_vol);
			} else {
				isRecording = true;
				msgEditText.setVisibility(View.GONE);
				audioButton.setVisibility(View.VISIBLE);
				recordImageView.setBackgroundResource(R.drawable.btn_keybord);
			}
			faceRelativeLayout.hide();
			break;

		case R.id.carmer_bg:
			showCameraDialog(imgpathHandler, true, 200, 200, true);
			break;

		default:
			break;
		}
	}

	/** 调用系统照相机 */
	@SuppressLint("HandlerLeak")
	private Handler imgpathHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (RESULT_CUT_IMG == msg.what) {
				Bitmap bt = (Bitmap) msg.obj;
				@SuppressWarnings("static-access")
				String name = new DateFormat().format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA)) + ".jpg";
				FileOutputStream b = null;
				File file = new File(Environment.getExternalStorageDirectory()
						+ "/gameproduct/camera");
				file.mkdirs();// 创建文件夹
				String fileName = Environment.getExternalStorageDirectory()
						+ "/gameproduct/camera/" + name;
				try {
					b = new FileOutputStream(fileName);
					bt.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				sendImg(fileName);
			}
		};
	};

	private void sendImg(final String imgpath) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (imgpath.length() > 0) {
					msgcontent = Utils.imgToBase64(Utils.compressImage(imgpath));
					Message msg = new Message();
					try {
						UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
						sendMsg("", userinfo.userNick, 2, msgcontent, 0,
								userinfo.userphoto);
						msg.what = MyApplication.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = MyApplication.READ_FAIL;
					}
					imgHandler.sendMessage(msg);
				}
			}
		}).start();
	}

	private Handler imgHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MyApplication.READ_SUCCESS:
				UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
				chatinfo = new FriendChatinfo();
				chatinfo.content = msgcontent;
				chatinfo.userid = userinfo.id;
				chatinfo.usernick = userinfo.userNick;
				chatinfo.username = username;
				chatinfo.userheadpath = userinfo.userphoto;
				chatinfo.time = TimeRender.getStandardDate();
				chatinfo.isMymsg = true;
				chatinfo.msgtype = 2;
				chatinfo.audiopath = "";
				chatinfo.audiolength = 0;
				db.saveGmember(chatinfo);
				if (!fdb.isExist(username, 1, TimeRender.getStandardDate(),
						userinfo.id)) {
					fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
							userinfo.id, userphoto, usernick);
				} else {
					fdb.UpdateChatagetTime(username, 1, userinfo.id,
							TimeRender.getStandardDate(), userphoto, usernick);
				}
				adapter.addChatinfo(chatinfo);
				adapter.notifyDataSetChanged();
				listView.setSelection(listView.getCount() - 1);
				break;

			case MyApplication.READ_FAIL:
				showToast("断线了%>_<%~~");
                Utils.againLogin(mContext);
                break;

			default:
				break;
			}
		};
	};

	/**
	 * 录音之后进行的处理
	 */
	@Override
	public void onRecord(String filepath, long time) {
		if (time < 1) {
			showToast("亲，您录音时间太短了~~");
			return;
		}
		UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
		try {
			sendMsg(filepath,
					userinfo.userNick,
					3,
					Utils.encodeBase64File(Environment
							.getExternalStorageDirectory() + filepath),
					(int) time, userinfo.userphoto);
		} catch (Exception e) {
			showToast("断线了%>_<%~~");
            Utils.againLogin(mContext);
            return;
		}
		chatinfo = new FriendChatinfo();
		chatinfo.content = msgcontent;
		chatinfo.userid = userinfo.id;
		chatinfo.usernick = userinfo.userNick;
		chatinfo.username = username;
		chatinfo.userheadpath = userinfo.userphoto;
		chatinfo.time = TimeRender.getStandardDate();
		chatinfo.isMymsg = true;
		chatinfo.msgtype = 3;
		chatinfo.audiopath = filepath;
		chatinfo.audiolength = (int) time;
		db.saveGmember(chatinfo);
		if (!fdb.isExist(username, 1, TimeRender.getStandardDate(), userinfo.id)) {
			fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
					userinfo.id, userphoto, usernick);
		} else {
			fdb.UpdateChatagetTime(username, 1, userinfo.id,
					TimeRender.getStandardDate(), userphoto, usernick);
		}
		adapter.addChatinfo(chatinfo);
		adapter.notifyDataSetChanged();
		listView.setSelection(listView.getCount() - 1);
	}

	/**
	 * 发送文字信息
	 * 
	 * @Title: sendMsgcontext
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void sendMsgcontext() {
		msgcontent = msgEditText.getText().toString().trim();
		if ("".equals(msgcontent)) {
			showToast("消息内容不能为空");
		} else {
			try {
				UserInfo userinfo = UserInfoUtil.getUserInfo(mContext);
				sendMsg("", userinfo.userNick, 1, msgcontent, 0,
						userinfo.userphoto);
				chatinfo = new FriendChatinfo();
				chatinfo.content = msgcontent;
				chatinfo.userid = userinfo.id;
				chatinfo.usernick = userinfo.userNick;
				chatinfo.username = username;
				chatinfo.userheadpath = userinfo.userphoto;
				chatinfo.time = TimeRender.getStandardDate();
				chatinfo.isMymsg = true;
				chatinfo.msgtype = 1;
				chatinfo.audiopath = "";
				chatinfo.audiolength = 0;
				db.saveGmember(chatinfo);
				if (!fdb.isExist(username, 1, TimeRender.getStandardDate(),
						userinfo.id)) {
					fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
							userinfo.id, userphoto, usernick);
				} else {
					fdb.UpdateChatagetTime(username, 1, userinfo.id,
							TimeRender.getStandardDate(), userphoto, usernick);
				}
				adapter.addChatinfo(chatinfo);
				adapter.notifyDataSetChanged();
				listView.setSelection(listView.getCount() - 1);
				msgcontent = "";
				msgEditText.setText("");
			} catch (Exception e) {
				showToast("断线了%>_<%~~");
                Utils.againLogin(mContext);
                return;
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onDestroy() {
		super.onDestroy();
		ClienConServer.getInstance(mContext).isChat = false;
	}

	private void sendMsg(String audiopath, String usernick, int msgtype,
			String msgcontent, int audiolength, String headpath)
			throws Exception {
		SingleChatPacketExtension pack = new SingleChatPacketExtension();
		pack.setAudiopath(audiopath);
		pack.setUsernick(usernick);
		pack.setMsgtype(msgtype);
		pack.setContent(msgcontent);
		pack.setAudiolength(audiolength);
		pack.setSendtime(TimeRender.getStandardDate());
		pack.setHeadpath(headpath);
		ClienConServer.getInstance(mContext).getChatManager(username)
				.sendMessage(pack.toXML());
	}

	public BroadcastReceiver mRecever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(MyApplication.CHAT_AGAIN_INIT)) {
				ClienConServer.getInstance(mContext).isChat = true;
				ClienConServer.getInstance(mContext).set(adapter.getChatlist(),
						adapter, listView, username);
			}
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}
}
