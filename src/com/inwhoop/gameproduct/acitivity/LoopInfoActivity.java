package com.inwhoop.gameproduct.acitivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.DialogPreference;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.LoopInfoBean;
import com.inwhoop.gameproduct.onekeyshare.Share;
import com.inwhoop.gameproduct.utils.*;

import java.io.Serializable;

/**
 * @Describe: TODO 圈子资料界面。在圈子聊天的右上角跳转而来.1期，取消移交功能
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/26 14:14
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class LoopInfoActivity extends BaseActivity implements View.OnClickListener {
    private static final char EXIT_OK = 0x01;      //退出成功
    private static final char EXIT_WRONG = 0x02;  //退出失败
    private static final char GET_CIRCLE_FORM_NET_OK = 0x03; //网络读到圈子ok
    private static final char GET_CIRCLE_FORM_NET_WRONG = 0x04;

    private ImageView userheadimg;
    private LoopInfoBean loopInfoBean;
    private boolean isLoopBoss;//是否是圈子创建者
    private Bitmap headBmp;
    private TextView nameTV;
    private TextView introduceTV;//说明

    @Override
    protected void onResume() {
        super.onResume();  //此接口返回数据是大写字母，会读取失败
        if (loopInfoBean.id != 0) getCircleFormNet("" + loopInfoBean.id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loop_info);
        ShareSDK.initSDK(this);
        mContext = LoopInfoActivity.this;

        LoopInfoBean lBean = (LoopInfoBean) getIntent().getSerializableExtra("bean");
        loopInfoBean = lBean != null ? lBean : new LoopInfoBean();
        if (loopInfoBean.id==0){//如果没传有值对象，获取这个id
            loopInfoBean.id= Integer.parseInt(getIntent().getStringExtra("groupid"));
        }

        initData();

        initLoopData();
    }

    private void initData() {
        init();
        setTitle(R.string.loop_info);
        setHeadLeftButton(R.drawable.back);
        setRightSecondBt(R.drawable.share);
        head_left.setOnClickListener(this);
        getRightSecondBt().setOnClickListener(this);

        View yaoqing = findViewById(R.id.invisit_number_layout);
        yaoqing.setOnClickListener(this);
        View daoru = findViewById(R.id.daoru_number_layout);
        daoru.setOnClickListener(this);
        Button get_out_btn = (Button) findViewById(R.id.get_out_btn);
        get_out_btn.setOnClickListener(this);
        findViewById(R.id.loop_head_layout).setOnClickListener(this);
        findViewById(R.id.loop_number_layout).setOnClickListener(this);
        userheadimg = (ImageView) findViewById(R.id.head_img);
        View yijiaoBtn = findViewById(R.id.yijiao_btn);
        yijiaoBtn.setOnClickListener(this);
        introduceTV = (TextView) findViewById(R.id.loop_instructions);
        introduceTV.setOnClickListener(this);

        String uid = UserInfoUtil.getUserInfo(mContext).id;
        isLoopBoss = (""+loopInfoBean.circlemanage).equals(uid) ? true : false;

//        isLoopBoss = true;//测试后需要改回
        if (!isLoopBoss) {
            findViewById(R.id.headupdate).setVisibility(View.GONE);
            findViewById(R.id.nameimg).setVisibility(View.GONE);
            findViewById(R.id.introimg).setVisibility(View.GONE);

            yijiaoBtn.setVisibility(View.GONE);
        } else {
            findViewById(R.id.headupdate).setVisibility(View.VISIBLE);
            findViewById(R.id.nameimg).setVisibility(View.VISIBLE);
            findViewById(R.id.introimg).setVisibility(View.VISIBLE);

            yijiaoBtn.setVisibility(View.GONE); //1期取消移交功能

            findViewById(R.id.loop_name_layout).setOnClickListener(this);
        }

        findViewById(R.id.btn_add_or_getout).setVisibility(View.GONE);
        get_out_btn.setVisibility(View.VISIBLE);
        yaoqing.setVisibility(View.VISIBLE);
        daoru.setVisibility(View.VISIBLE);
    }

    private void initLoopData() {
        BitmapManager.INSTANCE.loadBitmap(loopInfoBean.circleheadphoto,
                userheadimg, R.drawable.loading,
                true);
        nameTV = ((TextView) findViewById(R.id.loop_name));
        nameTV.setText(loopInfoBean.circlename);
        ((TextView) findViewById(R.id.loop_num))
                .setText(loopInfoBean.personNum + "人");
        introduceTV.setText(loopInfoBean.circleremark);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_left:
                Intent intent1 = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean", loopInfoBean);
                bundle.putString("groupname",loopInfoBean.circlename);
                bundle.putInt("groupid",loopInfoBean.id);
                bundle.putString("groupphoto",loopInfoBean.circleheadphoto);
                intent1.putExtras(bundle);
                setResult(300, intent1);
//                 Act.toActClearTop(mContext,LoopGroupChatActivity.class,bundle);
                finish();
                break;
            case R.id.head_right_second:
                Share.showShare(mContext, "分享此圈子", "null");
                break;
            case R.id.invisit_number_layout: //邀请
                Bundle bundle1=new Bundle();
                bundle1.putString("Tag","Loop");
                bundle1.putString("Id",loopInfoBean.id+"");
                Intent inte2 = new Intent(mContext, InviteMembersActivity.class);
                inte2.putExtras(bundle1);
                //inte2.putExtra("circleid", loopInfoBean.id);
                startActivity(inte2);                break;
            case R.id.daoru_number_layout: //导入
                Intent in3 = new Intent(mContext, MailListActivity.class);
                in3.putExtra("teltype", 1);
                in3.putExtra("id", loopInfoBean.id);
                startActivity(in3);
                break;
            case R.id.get_out_btn: //退出圈子
                showIsAgreeToast(0);
                break;
            case R.id.yijiao_btn://移交
                showIsAgreeToast(1);
                break;
            case R.id.loop_head_layout: //头像
                if (isLoopBoss)
                    showCameraDialog(headHandler, true, 200, 200, true);
                break;
            case R.id.loop_name_layout://圈子名称
                if (isLoopBoss) {
                    Intent intent = new Intent(mContext,
                            UpdateLoopInfoActivity.class);
                    intent.putExtra("title", getResources().getString(R.string.loop_name));
                    intent.putExtra("id", loopInfoBean.id);
                    intent.putExtra("content", loopInfoBean.circlename);
                    startActivityForResult(intent, 100);
                }
                break;
            case R.id.loop_number_layout:  //圈子成员(ーー゛)
                //MyToast.showShort(mContext,"圈子成员待实现");
               /* Bundle bundle2=new Bundle();
                bundle2.putString("MemberTag","Loop");
                Intent intent2=new Intent(LoopInfoActivity.this,TradeMembersActivity.class);
                intent2.putExtras(bundle2);
                startActivity(intent2);*/
                Bundle bundle2=new Bundle();
                bundle2.putSerializable("loopBean", loopInfoBean);
                bundle2.putString("MemberTag","Loop");
                Act.toAct(mContext, TradeMembersActivity.class, bundle2);
                break;

            case R.id.loop_instructions: //圈子说明
                if (isLoopBoss) {
                    Intent intent = new Intent(mContext,
                            UpdateLoopInfoActivity.class);
                    intent.putExtra("title", getResources().getString(R.string.loop_instructions));
                    intent.putExtra("id", loopInfoBean.id);
                    intent.putExtra("content", loopInfoBean.circleremark);
                    startActivityForResult(intent, 101);
                }
                break;

        }
    }


    private void showIsAgreeToast(int tag) {
        String str = "";
        if (tag == 0) { //退出
//            if (isLoopBoss) {
//                str = "您是圈子管理者，请先移交管理权";
//            } else {
                str = "确认退出该圈子？";
//            }
        } else if (tag == 1) {//移交
            str = "移交管理权待实现";
        }
        new AlertDialog.Builder(mContext)
                .setTitle(str)
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        exitForLoop(loopInfoBean.id,
                                Integer.parseInt(UserInfoUtil.getUserInfo(mContext).id));
                    }
                }).setNegativeButton("取消",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    private void exitForLoop(final int loopid, final int userid) {
        showProgressDialog("");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.exitLoop(loopid, userid);
                    msg.what = EXIT_OK;
                } catch (Exception e) {
                    msg.what = EXIT_WRONG;
                    LogUtil.e("exception,exitLoop:" + e.toString());
                }
                headHandler.sendMessage(msg);
            }
        }).start();
    }

    //通过网络查询圈子信息
    private void getCircleFormNet(final String loopid) {
        showProgressDialog("");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getCircle(loopid);
                    msg.what = GET_CIRCLE_FORM_NET_OK;
                } catch (Exception e) {
                    msg.what = GET_CIRCLE_FORM_NET_WRONG;
                    LogUtil.e("exception,netLoop:" + e.toString());
                }
                headHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler headHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case RESULT_CUT_IMG: // 裁剪
                    headBmp = (Bitmap) msg.obj;
                    new Thread(new Runnable() {

                        @Override
                        public void run() {
                            Message msg = new Message();
                            try {
                                LoopHeadphotoBean photo = new LoopHeadphotoBean();
                                photo.circleid = loopInfoBean.id;
                                photo.circleheadphoto = ""
                                        + Utils.imgToBase64(Utils
                                        .compressImage(headBmp));
                                msg.obj = JsonUtils.updateLoop(photo);
                                msg.what = MyApplication.READ_SUCCESS;
                            } catch (Exception e) {
                                msg.what = MyApplication.READ_FAIL;
                            }
                            headHandler.sendMessage(msg);
                        }
                    }).start();
                    break;

                case MyApplication.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[1]) {
                        showToast("修改成功O(∩_∩)O~~");
                        userheadimg.setImageBitmap(headBmp);
//                        loopInfoBean.circleheadphoto = ((LoopInfoBean) obj[2]).circleheadphoto; //没有返回对象，重新读取圈子信息

                        getCircleFormNet("" + loopInfoBean.id);
                    } else {
                        showToast("修改失败%>_<%~~");
                    }
                    break;
                case GET_CIRCLE_FORM_NET_OK:
                    loopInfoBean = (LoopInfoBean) msg.obj;
                    initData();
                    initLoopData();
                    break;
                case GET_CIRCLE_FORM_NET_WRONG:
                    MyToast.showShort(mContext, "数据异常");
                    break;

                case MyApplication.READ_FAIL:
                    showToast("失败了%>_<%，稍后再试吧");
                    break;

                case EXIT_OK:
                    Object[] obj2 = (Object[]) msg.obj;
                    MyToast.showShort(mContext, "" + obj2[0]);
                    Act.toActClearTop(mContext, MainActivity.class);
                    break;
                case EXIT_WRONG:
                    showToast("失败了%>_<%，稍后再试吧");
                    break;
                default:
                    break;
            }
        }
    };

    class LoopHeadphotoBean implements Serializable {
        public int circleid;
        public String circleheadphoto = "";
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            if (null != data) {
                String content = data.getStringExtra("content");
                nameTV.setText(content);
                loopInfoBean.circlename = content;

//                getCircleFormNet("" + loopInfoBean.id);
            }
        } else if (resultCode == 101 && null != data) {
            String content = data.getStringExtra("content");
            introduceTV.setText(content);
            loopInfoBean.circleremark = content;
//            getCircleFormNet("" + loopInfoBean.id);
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {  //不写返回码，重新查询单个圈子信息，我的圈子列表界面的话，重新刷新
//            Intent intent1 = new Intent();
//            Bundle bundle = new Bundle();
//            bundle.putSerializable("bean", loopInfoBean);
//            intent1.putExtras(bundle);
//            setResult(300, intent1);
//            finish();
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }
}
