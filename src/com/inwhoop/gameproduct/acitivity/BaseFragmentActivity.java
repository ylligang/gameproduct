package com.inwhoop.gameproduct.acitivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import com.inwhoop.gameproduct.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.CustomProgressDialog;

/**
 * FragmentActivity基类
 * @Project: GameProduct
 * @Title: BaseFragmentActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 *
 * @author ylligang118@126.com
 * @date 2014-4-2 下午4:30:14
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class BaseFragmentActivity extends FragmentActivity {
	public Context context;

	/** 标题栏左侧按钮或者文字 */
	private RelativeLayout head_left = null;
	private TextView head_left_bt = null;
	/** 标题栏第一个按钮，相对靠左一点的按钮 */
	private RelativeLayout head_right_first = null;
	private TextView head_right_bt_1 = null;
	/** 标题栏最右边的按钮 */
	private RelativeLayout head_right_second = null;
	private TextView head_right_bt_2 = null;
	/** 标题 */
	private TextView titleView = null;

	protected Context mContext = null;

//	public ProgressDialog progressDialog;// 加载进度框
	
	public CustomProgressDialog progressDialog = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
        mContext=this;
	}

	/**
	 * 控件初始化
	 *
	 * @Title: init
	 * @Description: TODO

	 */
	public void init() {
	    head_left = (RelativeLayout) findViewById(R.id.head_left);
        head_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        head_left_bt = (TextView) findViewById(R.id.head_left_button);
        head_right_first = (RelativeLayout) findViewById(R.id.head_right_first);
        head_right_second = (RelativeLayout) findViewById(R.id.head_right_second);
        head_right_bt_2 = (TextView) findViewById(R.id.head_right_bt_2);
        head_right_bt_1 = (TextView) findViewById(R.id.head_right_bt_1);
        titleView = (TextView) findViewById(R.id.head_title);
	}

	/**
	 * 设置左边按钮显示图片
	 *
	 * @Title: setHeadLeftButton
	 * @Description: TODO
	 * @param imgId
	 *            void
	 */
	public void setHeadLeftButton(int imgId) {
		head_left.setVisibility(View.VISIBLE);
		head_left_bt.setBackgroundResource(imgId);
	}

	/**
	 * 设置左边显示文字
	 *
	 * @Title: setHeadLeftText
	 * @Description: TODO
	 * @param textId
	 *            void
	 */
	public void setHeadLeftText(int textId) {
		head_left.setVisibility(View.VISIBLE);
		head_left_bt.setText(textId);
	}

	/**
	 * 设置标题
	 *
	 * @Title: setTitle
	 * @Description: TODO
	 * @param textId
	 *            void
	 */
	public void setTitle(int textId) {
		titleView.setText(textId);
	}

    /**
     * 设置标题，字符串
     *
     * @Title: setTitle
     * @Description: TODO
     * @param titleStr
     */
    public void setTitleStr(String titleStr) {
        titleView.setText(titleStr);
    }

    /**
	 * 设置右边第一个按钮
	 *
	 * @Title: setRightFirstBt
	 * @Description: TODO
	 * @param imgId
	 *            void
	 */
	public void setRightFirstBt(int imgId) {
		head_right_first.setVisibility(View.VISIBLE);
		head_right_bt_1.setBackgroundResource(imgId);
	}

	/**
	 * 设置右边第二个按钮
	 *
	 * @Title: setRightSecondBt
	 * @Description: TODO
	 * @param imgId
	 *            void
	 */
	public void setRightSecondBt(int imgId) {
		head_right_second.setVisibility(View.VISIBLE);
		head_right_bt_2.setBackgroundResource(imgId);
	}

	/**
	 * 获取左边按钮
	 *
	 * @Title: getLeftBt
	 * @Description: TODO
	 * @return RelativeLayout
	 */
	public RelativeLayout getLeftBt() {
		return head_left;
	}

	/**
	 * 获取右边第一个按钮
	 *
	 * @Title: getRightFirstBt
	 * @Description: TODO
	 * @return RelativeLayout
	 */
	public RelativeLayout getRightFirstBt() {
		return head_right_first;
	}

	/**
	 * 获取右边第二个按钮
	 *
	 * @Title: getRightSecondBt
	 * @Description: TODO
	 * @return RelativeLayout
	 */
	public RelativeLayout getRightSecondBt() {
		return head_right_second;
	}

	/**
	 * 显示进度框
	 *
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialog(String msg) {
		if (progressDialog == null){
			progressDialog = CustomProgressDialog.createDialog(this);
		}
	    	progressDialog.setMessage(msg);
	    	progressDialog.setCancelable(true);
		
		
    	progressDialog.show();
	}

	/**
	 * 关闭进度框
	 *
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 显示toast
	 *
	 * @Title: showToast
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}



	/**
	 * 自定义Dialog
	 * 
	 * @Title: buildDialog
	 * @Description: TODO
	 * @param @param dialogLinsener 自定义接口
	 * @param @param title 标题
	 * @param @param messgae 信息
	 * @param @return
	 * @return Dialog
	 */
	public void buildDialog(final DialogLinsener dialogLinsener,
			int title, int messgae) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.custom_dialog, null);
		Button ok = (Button) view.findViewById(R.id.dialog_button_ok);
		Button cancle = (Button) view.findViewById(R.id.dialog_button_cancle);
		TextView titleView=(TextView) view.findViewById(R.id.dialog_title);
		TextView msgView=(TextView) view.findViewById(R.id.dialog_message);
		titleView.setText(title);
		msgView.setText(messgae);
		final Dialog dialog = new Dialog(mContext, R.style.dialog_more_style);
		dialog.setContentView(view);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				dialogLinsener.onSure();
			}
		});
		cancle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				dialogLinsener.onCancle();
			}
		});
		
	}
}
