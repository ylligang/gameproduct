package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.entity.CreateLoopInfo;
import com.inwhoop.gameproduct.onekeyshare.Share;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.view.RoundAngleImageView;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: CreateLoopAfterActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * @date 2014-5-30 下午1:08:37
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class CreateLoopAfterActivity extends BaseActivity implements OnClickListener {

    private LinearLayout mailLayout, inivateLayout, tellLayout;

    private RoundAngleImageView headAngleImageView = null; // 头像

    private TextView nameTextView, introduceTextView;

    private CreateLoopInfo cinfo = null;

    private Button finishButton = null;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.after_create_loop_layout);
        ShareSDK.initSDK(this);
        mContext = CreateLoopAfterActivity.this;
        cinfo = (CreateLoopInfo) getIntent().getSerializableExtra("info");
        init();
//		creatRoom();
    }


    @Override
    public void init() {
        super.init();
        setTitle(R.string.create_success);
        setHeadLeftButton(R.drawable.back);
        mailLayout = (LinearLayout) findViewById(R.id.mailayout);
        mailLayout.setOnClickListener(this);
        inivateLayout = (LinearLayout) findViewById(R.id.inivatelayout);
        inivateLayout.setOnClickListener(this);
        tellLayout = (LinearLayout) findViewById(R.id.tellfriend);
        tellLayout.setOnClickListener(this);
        nameTextView = (TextView) findViewById(R.id.loopname);
        nameTextView.setText(cinfo.circlename);
        introduceTextView = (TextView) findViewById(R.id.loop_instro);
        introduceTextView.setText(cinfo.circleremark);
        headAngleImageView = (RoundAngleImageView) findViewById(R.id.loop_head_img);
        finishButton = (Button) findViewById(R.id.finishbtn);
        finishButton.setOnClickListener(this);
        BitmapManager.INSTANCE.loadBitmap(cinfo.circleheadphoto, headAngleImageView, R.drawable.loop_default_bg, true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mailayout:
                Intent intent = new Intent(mContext, MailListActivity.class);
                intent.putExtra("teltype", 1);
                intent.putExtra("id", cinfo.id);
                startActivity(intent);
                break;

            case R.id.inivatelayout: //邀请成员
                Bundle b=new Bundle();
                b.putString("Id", ""+ cinfo.id);
                b.putString("Tag", "Loop");
                Act.toAct(mContext, InviteMembersActivity.class,b);
                break;
            case R.id.tellfriend:
                Share.showShare(mContext, "邀请你加入【" + cinfo.circlename + "】圈子", cinfo.circleheadphoto);
                break;

            case R.id.finishbtn:
                Bundle b2 = new Bundle();
                b2.putString("groupid", cinfo.id + "");
                b2.putString("groupname", cinfo.circlename);
                b2.putString("groupphoto", cinfo.circleheadphoto);
                b2.putInt("isCreate", 11);  //如果是这个值，在另外个界面点返回就跳到圈子列表
                Act.toAct(mContext, GroupChatActivity.class, b2);
                finish();
                break;

            default:
                break;
        }
    }


}
