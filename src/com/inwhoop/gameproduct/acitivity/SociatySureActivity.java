package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.widget.*;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.WordwrapLayout;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

/**
 * @Project: MainActivity
 * @Title: SociatySureActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 公会信息/申请公会 界面
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-6-10 下午4:09:26
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SociatySureActivity extends BaseActivity implements
		View.OnClickListener {

	public static final int ACT_TAG_ADD_SOCIATY = 101; // 表示从申请页面跳转而来

	private int act_tag;// 标识从哪个页面跳转而来

	private WordwrapLayout wrapLayout = null;

	private LayoutInflater inflater = null;

	private LinearLayout noticeLayout, handleLayout;

	private Button applyButton = null;

	private TextView nameTextView, prinameTextView, countTextView,
			createtimeTextView, noticecontentTextView, introduceTextView;
	private SociatyBean sociatyBean;
	private PopupWindow popupWindow;

	private ImageView introImageView, noticeImageView;
    private boolean isCreateOk=false;//传参是否从创建公会完成跳转而来

    @Override
    protected void onResume() {
        super.onResume();
        if (null!=sociatyBean) {
//            searchThisGuild(""+sociatyBean.id);
        }
    }

    @SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sociaty_info_layout);
		mContext = SociatySureActivity.this;
		inflater = LayoutInflater.from(mContext);
		act_tag = getIntent().getIntExtra("act_tag", 0);
		sociatyBean = (SociatyBean) getIntent().getSerializableExtra("bean");
        isCreateOk=getIntent().getBooleanExtra("isCreateOk",false);

		init();
		initPopupwindow();

		initSociatyData();
	}

	private void initSociatyData() {
		setStringTitle(""+sociatyBean.guildname);
		nameTextView.setText(sociatyBean.guildname);
		prinameTextView.setText("会长:"+sociatyBean.username);
		BitmapManager.INSTANCE.loadBitmap(sociatyBean.guildheadphoto,
				(ImageView) findViewById(R.id.sociaheadimg),
				R.drawable.loading, true);
		countTextView.setText("成员数 :" + sociatyBean.personNum);
		createtimeTextView.setText("创建时间:" + sociatyBean.createtime);
		introduceTextView.setText(sociatyBean.guildremark);
		if ("".equals(sociatyBean.guildment)) {
			noticecontentTextView.setText("暂无");
		} else {
			noticecontentTextView.setText("" + sociatyBean.guildment);
		}

        initGameitem();

        if (!UserInfoUtil.getUserInfo(mContext).id.equals(""+sociatyBean.userid)) {
            introImageView.setVisibility(View.GONE);
            noticeImageView.setVisibility(View.GONE);
        }
    }

    private void initGameitem() {
        wrapLayout.removeAllViews();
        for (int i = 0; i < sociatyBean.gameinfos.size(); i++) {
            View item = inflater.inflate(R.layout.join_game_list_item, null);
            ImageView img = (ImageView) item
                    .findViewById(R.id.gameimg);
            BitmapManager.INSTANCE.loadBitmap(sociatyBean.gameinfos.get(i).gameIcon,
                    img,R.drawable.loading,true);
            wrapLayout.addView(item);
        }
    }

	@Override
	public void init() {
		super.init();
		setHeadLeftButton(R.drawable.back);
		wrapLayout = (WordwrapLayout) findViewById(R.id.gamelistlayout);
		noticeLayout = (LinearLayout) findViewById(R.id.noticelayout);
		handleLayout = (LinearLayout) findViewById(R.id.handlelayout);
		applyButton = (Button) findViewById(R.id.aplybutton);
		applyButton.setOnClickListener(this);
		nameTextView = (TextView) findViewById(R.id.name);
		prinameTextView = (TextView) findViewById(R.id.prisdentname);
		countTextView = (TextView) findViewById(R.id.number);
		createtimeTextView = (TextView) findViewById(R.id.creatime);
		noticecontentTextView = (TextView) findViewById(R.id.noticecontent);
		noticecontentTextView.setOnClickListener(this);
		introduceTextView = (TextView) findViewById(R.id.introducecontent);
		introImageView = (ImageView) findViewById(R.id.introimg);
		noticeImageView = (ImageView) findViewById(R.id.noticeimg);

		findViewById(R.id.socia_info_layout).setOnClickListener(this);
		findViewById(R.id.actimg).setOnClickListener(this);
		findViewById(R.id.memberimg).setOnClickListener(this);
		findViewById(R.id.chatimg).setOnClickListener(this);
        findViewById(R.id.head_left).setOnClickListener(this);

		if (act_tag == ACT_TAG_ADD_SOCIATY) {
            introImageView.setVisibility(View.GONE);
            noticeImageView.setVisibility(View.GONE);

            applyButton.setVisibility(View.VISIBLE);
			noticeLayout.setVisibility(View.GONE);
			handleLayout.setVisibility(View.GONE);
            findViewById(R.id.headimg_right_sanjiao).setVisibility(View.GONE);
		}else {
		    introduceTextView.setOnClickListener(this);
        }

	}

	@Override
	public void onClick(View v) {
		Bundle b = new Bundle();

		switch (v.getId()) {
		case R.id.socia_info_layout: // 公会信息详情
			if (act_tag == ACT_TAG_ADD_SOCIATY)
				return; // 如果是申请界面，无需跳转
			Intent intento = new Intent(mContext, SociatyInfoActivity.class);
			b.putInt("act_tag", act_tag);
			b.putSerializable("bean", sociatyBean);
			intento.putExtras(b);
			startActivityForResult(intento, 300);
			break;
		case R.id.actimg: // 公会活动
			b.putSerializable("bean", sociatyBean);
			Act.toAct(mContext, SociatyActActivity.class, b);
			break;
		case R.id.memberimg: // 公会成员
			b.putSerializable("bean", sociatyBean);
            b.putString("MemberTag","Trade");
			Act.toAct(mContext, TradeMembersActivity.class, b);
			break;
		case R.id.chatimg: // 聊天大厅
//			Act.toAct(mContext, GroupChatActivity.class, b);
			Intent intent2 = new Intent(mContext, GroupChatActivity.class);
			intent2.putExtra("groupid", ""+sociatyBean.id);
			intent2.putExtra("groupname", sociatyBean.guildname);
			intent2.putExtra("groupphoto", sociatyBean.guildheadphoto);
			startActivity(intent2);
			break;
		case R.id.aplybutton: // 申请加入公会
			showPopupwindow();
			break;
		case R.id.introducecontent:
			if (UserInfoUtil.getUserInfo(mContext).id.equals(""+sociatyBean.userid)) {
				Intent intent = new Intent(mContext,
						UpdateSociatyInfoActivity.class);
				intent.putExtra("title", "公会简介");
				intent.putExtra("id", sociatyBean.id);
				intent.putExtra("content", sociatyBean.guildremark);
				intent.putExtra("msgtype", 2);
				startActivityForResult(intent, 100);
			}
			break;
		case R.id.noticecontent:
			if (UserInfoUtil.getUserInfo(mContext).id.equals(""+sociatyBean.userid)) {
				Intent intent1 = new Intent(mContext,
						UpdateSociatyInfoActivity.class);
				intent1.putExtra("id", sociatyBean.id);
				intent1.putExtra("title", "公会公告");
				intent1.putExtra("content", sociatyBean.guildment);
				intent1.putExtra("msgtype", 3);
				startActivityForResult(intent1, 200);
			}
			break;
        case R.id.head_left:  //因为有从创建公会完成后跳转而来，所以返回重写跳到我的公会界面
            if (isCreateOk)Act.toActClearTop(mContext,MainActivity.class);
            else finish();
                break;
		}
	}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK && isCreateOk){
            Act.toActClearTop(mContext,MainActivity.class);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		String content = "";
		if (resultCode == 100) {
			if (null != data) {
				content = data.getStringExtra("content");
				introduceTextView.setText("" + content);
				sociatyBean.guildremark = content;
			}
		} else if (requestCode == 200) {
			if (null != data) {
				content = data.getStringExtra("content");
				noticecontentTextView.setText("" + content);
				sociatyBean.guildment = content;
			}
		} else if(requestCode == 300 && null != data){
			SociatyBean sb = (SociatyBean) data.getSerializableExtra("bean");
//			sociatyBean.guildname = sb.guildname;
//			sociatyBean.guildheadphoto = sb.guildheadphoto;
            sociatyBean=sb;
			initSociatyData();
		}
	}

	// TODO 调接口申请加入公会
	private void getinSociaty(final String userId, final int sociatyid,
			final String askremark) {
		showProgressDialog("");
		progressDialog.setCancelable(false);
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.askForGuild(userId, sociatyid,
							askremark);
					msg.what = MyApplication.SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

    // TODO 调接口查询公会详情。为了方便数据更新和权限管理
    private void searchThisGuild( final String sociatyid) {
        showProgressDialog("");
        progressDialog.setCancelable(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.searchThisGuild(sociatyid);
                    msg.what = 22;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }


	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.FAIL:
				MyToast.showShort(mContext, "数据异常");
				break;

			case MyApplication.SUCCESS:
				if(msg.obj.toString().contains("成功")){
					ClientGroupServer.getInstance(mContext).addMultiuserchat(""+sociatyBean.id, sociatyBean.guildname);
				}
				MyToast.showShort(mContext, "" + msg.obj);
				break;
            case 22:
                sociatyBean= (SociatyBean) msg.obj;
                initSociatyData();
                break;
			}
		}
	};

	@SuppressWarnings("deprecation")
	private void initPopupwindow() {
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.check_information_popupwindow, null);
		popupWindow = new PopupWindow(view, getWindowManager()
				.getDefaultDisplay().getWidth() / 4 * 3,
				LinearLayout.LayoutParams.WRAP_CONTENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
//		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		final EditText checkEditText = (EditText) view
				.findViewById(R.id.checkinfo);
		Button cancel = (Button) view.findViewById(R.id.cancle);
		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow) {
					popupWindow.dismiss();
                    checkEditText.setText("");
                    KeyBoard.HiddenInputPanel(mContext);
                }
			}
		});
		Button sendButton = (Button) view.findViewById(R.id.send);
		sendButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow) {
					popupWindow.dismiss();
                    checkEditText.setText("");
                    KeyBoard.HiddenInputPanel(mContext);
                }
				getinSociaty(UserInfoUtil.getUserInfo(mContext).id,
						sociatyBean.id, checkEditText.getText().toString());
			}
		});
	}

	private void showPopupwindow() {
		popupWindow.showAtLocation(
				LayoutInflater.from(mContext).inflate(
						R.layout.activity_add_friend_sure, null),
				Gravity.CENTER, 0, 0);
	}

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }
}
