package com.inwhoop.gameproduct.acitivity;

import java.util.List;

import android.view.View;
import android.widget.AdapterView;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.NearPeoplelistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 *
 * 附近的人
 *
 * @Project: MainActivity
 * @Title: NearPeopleActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-6-7 上午10:14:12
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class NearPeopleActivity extends BaseActivity implements
		IXListViewListener {

	private XListView peoListView = null;

	private List<FriendData> list = null;

	private NearPeoplelistAdapter adapter = null;

	private LocationClient locationClient = null;

	private double xlat, xlong;

	private boolean flag = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.near_people_list_layout);
		mContext = NearPeopleActivity.this;
		init();
	}

	@Override
	public void init() {
		super.init();
		setHeadLeftButton(R.drawable.back);
		setTitle(R.string.man_near);
		peoListView = (XListView) findViewById(R.id.nearpeolist);
		peoListView.setXListViewListener(this);
		peoListView.setPullLoadEnable(false);
        peoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (null!=list&&list.size()>0){
                    Bundle b = new Bundle();
                    b.putSerializable("bean", list.get(position - 1 ));
                    Act.toAct(mContext, AddFriendSureActivity.class, b);
                }
            }
        });

		showProgressDialog("耐心等待哦，正在获取数据...");
		location();
	}

	private void location() {
		locationClient = new LocationClient(this); // 设置定位条件
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setAddrType("detail");
		option.setCoorType("gcj02");
		option.setScanSpan(5000);
		option.disableCache(true);// 禁止启用缓存定位
		option.setPoiNumber(5); // 最多返回POI个数
		option.setPoiDistance(1000); // poi查询距离
		option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
		locationClient.setLocOption(option);
		locationClient.registerLocationListener(new MyLocationListenner());
		locationClient.start();
		locationClient.requestLocation();
	}

	@Override
	public void onRefresh() {
		flag = true;
		location();
	}

	@Override
	public void onLoadMore() {

	}

	private class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null) {
				dismissProgressDialog();
				peoListView.stopRefresh();
				showToast("获取数据失败~~");
				return;
			}
			if(flag){
				xlat = location.getLatitude();
				xlong = location.getLongitude();
                LogUtil.i("location,x,y,MyLocationListenner" + xlat + "," + xlong);

                read(xlat, xlong);
			}
		}

		@Override
		public void onReceivePoi(BDLocation poilocation) {

		}
	}

	private void read(final double xlat, final double xlong) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getNearPeopleList(
							UserInfoUtil.getUserInfo(mContext).id, xlat,
							xlong);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			peoListView.stopRefresh();
			flag = false;
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("获取数据失败~~");
				break;

			case MyApplication.READ_SUCCESS:
				if(list.size()==0){
					showToast("悲剧啊，没有找到有缘人~~");
				}
				adapter = new NearPeoplelistAdapter(mContext, list);
				peoListView.setAdapter(adapter);
				break;

			default:
				break;
			}
		};
	};

}
