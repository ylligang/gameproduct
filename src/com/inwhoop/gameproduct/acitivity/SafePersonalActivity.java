package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.SlidButton;

/**
 * @Describe: TODO      安全与隐私
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/13 20:28
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SafePersonalActivity extends BaseActivity implements View.OnClickListener {

    private boolean flag = true;

    private int islook; //1是开启

    @SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_personal);
        mContext = SafePersonalActivity.this;
        islook = Utils.getIntpreferenceData(mContext, "islook", 1);
        initData();
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.anquanyuyinsi);

        View xiugaiPwd = findViewById(R.id.xiugai);
        xiugaiPwd.setOnClickListener(this);
        View qingchu = findViewById(R.id.qingchu);
        qingchu.setOnClickListener(this);

        SlidButton duifujin_btn = (SlidButton) findViewById(R.id.duifujin_btn);
        duifujin_btn.setState(islook);
        duifujin_btn.setOnChangedListener(new SlidButton.OnChangedListener() {
            @Override
            public void OnChanged(boolean checkState) {
                if (checkState) {
                    islook = 1;
                    setOnNet();
                } else {
                    islook = 2;
                    setOnNet();
                }
            }
        });
    }

    private void read(final int islook) {
        showProgressDialog("");
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                	if(islook == 1){
                		flag = JsonUtils.updateLocationState(UserInfoUtil.getUserInfo(mContext).id, islook);
                	}else{
                		flag = JsonUtils.updateLocationState(UserInfoUtil.getUserInfo(mContext).id, 0);
                	}
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private void setOnNet() {
        Utils.saveIntPreferenceData(mContext, "islook", islook);
        read(islook);
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_FAIL:
                    break;

                case MyApplication.READ_SUCCESS:
                    if (flag) {
//					showToast("设置成功");
                    }
                    break;

                default:
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.xiugai:
                Act.toAct(mContext, ChangePwdActivity.class);
                break;
            case R.id.qingchu:
                MyToast.showShort(mContext, "清除缓存功能稍后实现");
                break;
        }
    }
}
