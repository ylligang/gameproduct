package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.ScoreDialog;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 个人资料页面
 * @date TODO 2014年6月27日 09:43:05  ZK修改
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class PersonInfoActivity extends BaseActivity implements View.OnClickListener {
    private final int NICK_CODE = 101;  //昵称返回码
    private final int ADDRESS_CODE = 102;  //地区返回码
    private final int REMARK_CODE = 103;  //个人说明返回码

    private ImageView headImg;
    private TextView nick;
    private TextView account;
    private TextView gender;  //性别
    private TextView birthday;
    private TextView location;
    private TextView personState;
    private int mYear = 1970;
    private int mMonth = 01;
    private int mDay = 01;
    private String data_01 = "";

    private UserInfo userInfo;
    private Bitmap headBmp;//头像bitmap


    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = PersonInfoActivity.this;
        userInfo = UserInfoUtil.getUserInfo(mContext);
        setContentView(R.layout.person_info_activity);

        initView();

        setTimeDialog();

        setUserInfo();
    }

    private void initView() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.my_info);
        getLeftBt().setOnClickListener(this);

        findViewById(R.id.person_info_activity_head_img_rela).setOnClickListener(this);
        headImg = (ImageView) findViewById(R.id.person_info_activity_head_img);
        findViewById(R.id.person_info_activity_nick_rela).setOnClickListener(this);
        account = (TextView) findViewById(R.id.person_info_activity_account);
        nick = (TextView) findViewById(R.id.person_info_activity_nick);
        findViewById(R.id.person_info_activity_gender_rela).setOnClickListener(this);
        gender = (TextView) findViewById(R.id.person_info_activity_gender);
        findViewById(R.id.person_info_activity_birthday_rela).setOnClickListener(this);
        birthday = (TextView) findViewById(R.id.person_info_activity_birthday);
        findViewById(R.id.person_info_activity_location_rela).setOnClickListener(this);
        location = (TextView) findViewById(R.id.person_info_activity_location);
        findViewById(R.id.person_info_activity_person_state_rela).setOnClickListener(this);
        personState = (TextView) findViewById(R.id.person_info_activity_person_state);

    }

    private void setTimeDialog() {
        String data = userInfo.birthday;
//        LogUtil.i("data,:" + data);
        if ("".equals(data)) {
            mYear = 1970;
            mMonth = 01;
            mDay = 01;
            userInfo.birthday = "1970-01-01";
        } else if (data.length() > 8) {
            mYear = Integer.parseInt(data.substring(0, 4));
            mMonth = Integer.parseInt(data.substring(5, 7)) - 1;  //这个好像要设置减1
            mDay = Integer.parseInt(data.substring(8, 10));

//            LogUtil.i("data,y,m,d:" + mYear + "," + mMonth + "," + mDay);
        }
    }

    //设置用户信息
    private void setUserInfo() {
        LogUtil.i("userinfo,tostring:"+userInfo.toString());
        BitmapManager.INSTANCE.loadBitmap(userInfo.userphoto, headImg, R.drawable.default_avatar, true);
        account.setText(userInfo.userId);
        nick.setText(userInfo.userNick);
        gender.setText(userInfo.getSexString());
        birthday.setText(userInfo.birthday);
        location.setText(userInfo.useraddress);
        personState.setText(userInfo.remark);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(PersonInfoActivity.this, ChangeInfoActivity.class);

        switch (v.getId()) {
            case R.id.person_info_activity_nick_rela: //昵称
                intent.putExtra("changeInfo", "nick");
                intent.putExtra("content", userInfo.userNick);
                startActivityForResult(intent, NICK_CODE);
                break;
            case R.id.person_info_activity_location_rela://地区
                intent.putExtra("changeInfo", "address");
                intent.putExtra("content", userInfo.useraddress);
                startActivityForResult(intent, ADDRESS_CODE);
                break;
            case R.id.person_info_activity_person_state_rela:  //个人说明
                intent.putExtra("changeInfo", "remark");
                intent.putExtra("content", userInfo.remark);
                startActivityForResult(intent, REMARK_CODE);
                break;

            case R.id.person_info_activity_head_img_rela:  //头像
                showCameraDialog(mHandler, true, 200, 200, true);
                break;
            case R.id.person_info_activity_gender_rela:  //性别
                showSelectSexDialog();
                break;
            case R.id.person_info_activity_birthday_rela:  //出生生日
                showTimeView();
                break;

            case R.id.head_left:
                showIsAgreeToast();
                break;
        }
    }

    private void showIsAgreeToast() {
        new AlertDialog.Builder(mContext)
                .setTitle("保存资料？")
                .setNegativeButton("保存", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updataUserInfo();
                    }
                })
                .setNeutralButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showIsAgreeToast();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showTimeView() {

        new DatePickerDialog(PersonInfoActivity.this, mDateSetListener,
                mYear, mMonth, mDay).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NICK_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String nickStr = data.getStringExtra("nick_info");
                    if (null != data && !nickStr.equals("")) {
                        nick.setText(nickStr);
                        userInfo.userNick = nickStr;
                    }
                }
                break;
            case ADDRESS_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String addressStr = data.getStringExtra("address_info");

                    if (null != data && !addressStr.equals("")) {
                        location.setText(addressStr);
                        userInfo.useraddress = addressStr;
                    }
                }
                break;
            case REMARK_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String str = data.getStringExtra("remark_info");
                    if (null != data && !str.equals("")) {
                        personState.setText(str);
                        userInfo.remark = str;
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showSelectSexDialog() {
        final ScoreDialog dialog = new ScoreDialog(this, R.layout.select_gender_dialog, R.style.dialog_more_style);
        dialog.setParamsBotton();
        dialog.show();

        dialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                gender.setText(R.string.man);
                userInfo.sex = "1";
            }
        });
        dialog.findViewById(R.id.album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                gender.setText(R.string.woman);
                userInfo.sex = "0";
            }
        });
        dialog.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_more_style);
    }


    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;

            data_01 = new StringBuilder()
                    .append(mYear)
                    .append("-")
                    .append((mMonth + 1) < 10 ? "0" + (mMonth + 1)
                            : (mMonth + 1)).append("-")
                    .append((mDay < 10) ? "0" + mDay : mDay).toString();

            birthday.setText(data_01);
            userInfo.birthday = data_01;
        }
    };

    private void updataUserInfo() {
        showProgressDialog("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    UpdateUser user = new UpdateUser();
                    user.id = userInfo.id;
                    if (null != headBmp)
                        user.userphoto = Utils.imgToBase64(headBmp);
                    user.username = userInfo.userNick;
                    user.remark = userInfo.remark;
                    user.sex = Integer.parseInt(userInfo.sex);
                    user.birthday = userInfo.birthday;
                    user.useraddress = userInfo.useraddress;
                    LogUtil.i("user,up:"+user.toString());

                    msg.obj = JsonUtils.updateUser(user);
                    msg.what = MyApplication.SUCCESS;

                } catch (Exception e) {
                    LogUtil.e("exception,updataUser" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public class UpdateUser {
        public String id;
        public String userphoto;
        public String username; // 用户名不能改，代表昵称
        public String remark;
        public int sex;
        public String birthday;
        public String useraddress;

        @Override
        public String toString() {
            return "UpdateUser{" +
                    "id='" + id + '\'' +
                    ", userphoto='" + userphoto + '\'' +
                    ", username='" + username + '\'' +
                    ", remark='" + remark + '\'' +
                    ", sex='" + sex + '\'' +
                    ", birthday='" + birthday + '\'' +
                    ", useraddress='" + useraddress + '\'' +
                    '}';
        }
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    Object[] objects = (Object[]) msg.obj;
                    MyToast.showShort(mContext, "" + objects[0]);
                    userInfo = (UserInfo) objects[2];
                    UserInfoUtil.rememberUserInfo(mContext, userInfo);
                    LogUtil.i("userinfo:" + userInfo.toString());
                    finish();
                    break;
                case MyApplication.ERROR:
                    MyToast.showShort(mContext, "资料更新失败");
                    finish();
                    break;
                case RESULT_CUT_IMG:
                    headBmp = Utils.compressImage((Bitmap) msg.obj);
                    headImg.setImageBitmap(headBmp);
                    break;
            }
        }
    };
}