package com.inwhoop.gameproduct.acitivity;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gameproduct.R;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @Project: MainActivity
 * @Title: GameAllListActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 全部游戏列表
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-15 下午5:10:23
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GameAllListActivity extends BaseActivity implements
		OnClickListener {

	private TextView netTextView, singleTextView, phoneTextView,yyTextView;

	private View netView, singleView, phoneView,yyView;

	private RelativeLayout netLayout, singleLayout, phoneLayout,yyLayout;

	private ViewPager viewpager = null;

	private List<TextView> titleList = new ArrayList<TextView>();

	private List<View> lineList = new ArrayList<View>();

	private List<View> list = new ArrayList<View>();
	
	private int type = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_all_layout);
		mContext = GameAllListActivity.this;
		type = getIntent().getIntExtra("type", 0);
		init();
	}

	@Override
	public void init() {
		super.init();
		setTitle(R.string.all_game);
		setHeadLeftButton(R.drawable.back);
		setRightSecondBt(R.drawable.search);
		head_right_bt_2.setOnClickListener(this);
		netTextView = (TextView) findViewById(R.id.t1);
//		singleTextView = (TextView) findViewById(R.id.t2);
		phoneTextView = (TextView) findViewById(R.id.t3);
//		yyTextView = (TextView) findViewById(R.id.t4);
		titleList.add(netTextView);
		titleList.add(phoneTextView);
//		titleList.add(yyTextView);
//		titleList.add(singleTextView);
		netView = findViewById(R.id.line);
//		singleView = findViewById(R.id.line2);
		phoneView = findViewById(R.id.line3);
//		yyView = findViewById(R.id.line4);
		lineList.add(netView);
		lineList.add(phoneView);
//		lineList.add(yyView);
//		lineList.add(singleView);
		netLayout = (RelativeLayout) findViewById(R.id.netlayout);
		netLayout.setOnClickListener(this);
//		singleLayout = (RelativeLayout) findViewById(R.id.singlelayout);
//		singleLayout.setOnClickListener(this);
		phoneLayout = (RelativeLayout) findViewById(R.id.phonelayout);
		phoneLayout.setOnClickListener(this);
//		yyLayout = (RelativeLayout) findViewById(R.id.yylayout);
//		yyLayout.setOnClickListener(this);
		viewpager = (ViewPager) findViewById(R.id.gameviewpager);
		list.add(swithActivity(1, 1));
		list.add(swithActivity(1, 2));
//		list.add(swithActivity(1, 3));
//		list.add(swithActivity(1, 4));
		viewpager.setAdapter(new MypageAdater());
		viewpager.setOnPageChangeListener(new MyPagelistener());
		switchItem(type);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.netlayout:
			switchItem(0);
			break;

//		case R.id.singlelayout:
//			switchItem(3);
//			break;
		case R.id.phonelayout:
			switchItem(1);
			break;
//		case R.id.yylayout:
//			switchItem(2);
//			break;
			
		case R.id.head_right_bt_2:
			Intent intent = new Intent(mContext, SearchGameActivity.class);
			startActivity(intent);
			break;

		default:
			break;
		}
	}

	private void switchItem(int position) {
		for (int i = 0; i < titleList.size(); i++) {
			titleList.get(i).setTextColor(
					getResources().getColor(R.color.text_color));
			lineList.get(i).setVisibility(View.INVISIBLE);
		}
		titleList.get(position).setTextColor(
				getResources().getColor(R.color.blue_33a6ff));
		lineList.get(position).setVisibility(View.VISIBLE);
		viewpager.setCurrentItem(position);
	}

//	private class MyFragmentPageAdapter extends FragmentPagerAdapter {
//		private FragmentTransaction mFt;
//
//		public MyFragmentPageAdapter(FragmentManager fm) {
//			super(fm);
//			mFt = fm.beginTransaction();
//		}
//
//		@Override
//		public Fragment getItem(int position) {
//			return flist.get(position);
//		}
//
//		@Override
//		public int getCount() {
//			return lineList.size();
//		}
//
//		@Override
//		public int getItemPosition(Object object) {
//			return super.getItemPosition(object);
//		}
//
//		@Override
//		public void destroyItem(ViewGroup container, int position, Object object) {
//			super.destroyItem(container, position, object);
//		}
//
//		// @Override
//		// public Object instantiateItem(ViewGroup container, int position) {
//		// getSupportFragmentManager().beginTransaction()
//		// .add(R.id.gameviewpager, flist.get(position)).commit();
//		// return flist.get(position);
//		// }
//
//	}

	private class MyPagelistener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int position) {
			switchItem(position);
		}

	}

	private class MypageAdater extends PagerAdapter {
		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(list.get(arg1 % list.size()));
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(list.get(position % list.size()));
			return list.get(position % list.size());
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}

	/**
	 * activity跳转
	 * 
	 * @Title: swithActivity
	 * @Description: TODO
	 * @param @param position
	 * @return void
	 */
	private View swithActivity(int type, int gametype) {
		Intent intent = new Intent(this, GamelistitemActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.putExtra("type", type);
		intent.putExtra("gametype", gametype);
		String name = gametype + " subactivity";
		@SuppressWarnings("deprecation")
		View subActivity = getLocalActivityManager()
				.startActivity(name, intent).getDecorView();
		return subActivity;
	}
}
