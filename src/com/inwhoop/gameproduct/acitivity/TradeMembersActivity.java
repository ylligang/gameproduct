package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.MeberlistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.entity.LoopInfoBean;
import com.inwhoop.gameproduct.entity.SociatyBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.MypullListView;
import com.inwhoop.gameproduct.view.SelectLetterView;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.*;

/**
 * TODO 公会成员 列表
 */
public class TradeMembersActivity extends BaseActivity implements MypullListView.OnRefreshListener {
    private WindowManager mWindowManager;
    private MypullListView listView;
    private List<FriendData> memberListData = new ArrayList<FriendData>();
    private TextView mDialogText;
    private UserInfo userInfo;
    private SociatyBean bean;
    private List<FriendData> presidentList = new ArrayList<FriendData>();
    private List<FriendData> memList = new ArrayList<FriendData>();
    private TextView nowletter;
    private SelectLetterView letterView;
    private List<FriendData> list = null;
    private String MemberTag = "";
    private LoopInfoBean loopBean;
    private DialogShowStyle dialogShowStyle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_members_activity);
        SociatyBean sBean = (SociatyBean) getIntent().getSerializableExtra("bean");
        bean = (null != sBean) ? sBean : new SociatyBean();

        mContext = TradeMembersActivity.this;

        init();
        initView();
    }

    private void initView() {
        userInfo = UserInfoUtil.getUserInfo(this);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            MemberTag = bundle.getString("MemberTag");
            loopBean = (LoopInfoBean) getIntent().getSerializableExtra("loopBean");
        }
        setHeadLeftButton(R.drawable.back);
        nowletter = (TextView) findViewById(R.id.trade_members_activity_nowletter);
        letterView = (SelectLetterView) findViewById(R.id.trade_members_activity_letter);
        listView = (MypullListView) findViewById(R.id.trade_members_activity_listview);
        listView.setonRefreshListener(this);
        // String jsonString = FileReadWriteUtil.readFileSdcardFile(path, "friendListInfo" + userInfo.userId);
        if (MemberTag.equals("Loop")) {
            setStringTitle(getResources().getString(R.string.loop_members));
            if (null != loopBean) {
                getMemberListDatas(loopBean.id);
            }
        } else if (MemberTag.equals("Trade")) {
            setStringTitle(getResources().getString(R.string.trade_members));
            if (null != bean) {
                getMemberListDatas(bean.id);
            }
        }
       /* if (null != bean)
            getMemberListDatas(bean.id);*/
        // System.out.println("用户的ID==" + (bean.id));
    }

    private void read() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    list = getlist();
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            dialogShowStyle.dialogDismiss();
            listView.onRefreshComplete();
            letterView.setList(list);
            letterView.setPuListView(listView);
            letterView.setNowTextView(nowletter);
            MeberlistAdapter adapter = new MeberlistAdapter(TradeMembersActivity.this, list);
            listView.setAdapter(adapter);
            letterView.setNoStr(list.get(0).letter);
        }

        ;
    };

    private List<FriendData> getlist() throws Exception {
        List<FriendData> list = new ArrayList<FriendData>();
        FriendData info = null;
        for (int i = 0; i < memList.size(); i++) {
            info = new FriendData();
            info.userPhoto = memList.get(i).userPhoto;
            info.userName = memList.get(i).userName;
            list.add(info);
        }
        List<FriendData> otherlist = new ArrayList<FriendData>();
        List<FriendData> pinlist = new ArrayList<FriendData>();
        for (int i = 0; i < list.size(); i++) {
            if (isChinese(list.get(i).userName.charAt(0))) {
                pinlist.add(list.get(i));
            } else {
                otherlist.add(list.get(i));
            }
        }
        pinlist = getSortListFrompin(pinlist);
        otherlist = getSortListFrome(otherlist);
        list.clear();
        if (MemberTag.equals("Trade") && presidentList.size() > 0) {
            FriendData hui = new FriendData();
            hui.letter = "会长";
            hui.userPhoto = presidentList.get(0).userPhoto;
            hui.userName = presidentList.get(0).userName;
            list.add(hui);
        }
        for (int i = 0; i < otherlist.size(); i++) {
            list.add(otherlist.get(i));
        }
        for (int i = 0; i < pinlist.size(); i++) {
            list.add(pinlist.get(i));
        }
        return list;
    }

    /**
     * 是否是汉字和字母
     *
     * @param @param  a
     * @param @return
     * @return boolean
     * @Title: isChinese
     * @Description: TODO
     */
    public boolean isChinese(char a) {
        int v = (int) a;
        return (v >= 19968 && v <= 171941) || (v >= 65 && v <= 90) || (v >= 97 && v <= 122);
    }

    @Override
    public void onRefresh() {
        read();
    }

    /**
     * 对对非数字客户名进行排序
     *
     * @return
     * @throws BadHanyuPinyinOutputFormatCombination
     */
    private List<FriendData> getSortListFrompin(List<FriendData> list)
            throws BadHanyuPinyinOutputFormatCombination {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                try {
                    return getPinYin(arg0.userName.toLowerCase()).compareTo(
                            getPinYin(arg1.userName.toLowerCase()));
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = strTOup(getPinYin(list.get(i).userName));
        }
        return list;
    }

    private String strTOup(String str) {
        return ("" + str.charAt(0)).toUpperCase();
    }

    /**
     * 对对非数字客户名进行排序
     *
     * @return
     */
    private List<FriendData> getSortListFrome(List<FriendData> list) {
        Collections.sort(list, new Comparator<FriendData>() {
            public int compare(FriendData arg0, FriendData arg1) {
                return arg0.userName.compareTo(arg1.userName);
            }
        });
        for (int i = 0; i < list.size(); i++) {
            list.get(i).letter = "#";
        }
        return list;
    }

    /**
     * 将中文转换成拼音
     *
     * @param -汉字
     * @return
     */
    public static String getPinYin(String zhongwen)
            throws BadHanyuPinyinOutputFormatCombination {
        String zhongWenPinYin = "";
        char[] chars = zhongwen.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            String[] pinYin = PinyinHelper.toHanyuPinyinStringArray(chars[i],
                    getDefaultOutputFormat());
            // 当转换不是中文字符时,返回null
            if (pinYin != null) {
                zhongWenPinYin += pinYin[0];
            } else {
                zhongWenPinYin += chars[i];
            }
        }

        return zhongWenPinYin + "-" + zhongwen;
    }

    /**
     * 输出格式
     *
     * @return
     */
    public static HanyuPinyinOutputFormat getDefaultOutputFormat() {
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);// 小写
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);// 没有音调数字
        format.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);// u显示
        return format;
    }

    @Override
    public void scrollitem(int first) {
        if (null != letterView && null != list && !letterView.isFlag()) {
            letterView.setNoStr(list.get(first).letter);
        }
    }

    private void getMemberListDatas(final int guildid) {
        dialogShowStyle = new DialogShowStyle(TradeMembersActivity.this, "");
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                Message msg = new Message();
                try {
                    memberListData.clear();
                    memberListData = JsonUtils.getUserByGuildMem(guildid, MemberTag);
                    //System.out.println("圈子名称==="+memberListData.get(0).userName);
                    if (null != memberListData && memberListData.size() > 0) {
                      /*  FileReadWriteUtil.write2SDFromInput2(path,
                                "friendListInfo"+userInfo.userId, jsonData);*/
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    LogUtil.e("exception,公会成员:" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
                Looper.loop();
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    List<String> list = new ArrayList<String>();
                    for (FriendData friendData : memberListData) {
                        list.add(friendData.userId);
                        if (friendData.role == 1) {
                           /* System.out.println("会长信息111=" +
                                    friendData.userId + "=" + friendData.userName +
                                    "=" + friendData.userPhoto);*/
                            FriendData friendData1 = new FriendData();
                            friendData1.userId = friendData.userId;
                            friendData1.userName = friendData.userName;
                            friendData1.userPhoto = friendData.userPhoto;
                            presidentList.add(friendData1);
                        } else if (friendData.role == 0) {
                           /* System.out.println("会长信息222=" +
                                    friendData.userId + "=" + friendData.userName +
                                    "=" + friendData.userName);*/
                            FriendData friendData2 = new FriendData();
                            friendData2.userId = friendData.userId;
                            friendData2.userName = friendData.userName;
                            friendData2.userPhoto = friendData.userPhoto;
                            memList.add(friendData2);
                        }
                    }
                    read();
                    //setListDataShow(list, memberListData,presidentList,memList);
                    break;
                case MyApplication.FAIL:
                    dialogShowStyle.dialogDismiss();
                    Toast.makeText(TradeMembersActivity.this,
                            getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    dialogShowStyle.dialogDismiss();
                    MyToast.showShort(mContext, "数据异常");
                    break;
            }
        }
    };

   /* *//*
    好友列表适配器
     *//*
    static class ContactAdapter extends BaseAdapter implements SectionIndexer {
        private Context mContext;
        private String[] mNicks;
        private List<FriendData> listFriend;
        private List<FriendData> presidentList;
        private List<FriendData> memList;

        @SuppressWarnings("unchecked")
        public ContactAdapter(Context mContext, List<String> list,
                              List<FriendData> listFriend,
                              List<FriendData> presidentList,
                              List<FriendData> memList) {
            this.mContext = mContext;
            String[] str = new String[list.size()];
            this.mNicks = list.toArray(str);
            this.listFriend = listFriend;
            this.presidentList=presidentList;
            this.memList=memList;
            Arrays.sort(mNicks, new PinyinComparator());
            //排序(实现了中英文混排)
            // Collections.sort(mNicks, new PinyinComparator());
        }

        @Override
        public int getCount() {
            return mNicks.length;
        }

        @Override
        public Object getItem(int position) {
            return mNicks[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final String nickName = mNicks[position];
            System.out.println("昵称为===" + nickName + "====" + mNicks[0]);
            ViewHolder viewHolder = null;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.trade_members_item, null);
                viewHolder = new ViewHolder();
                viewHolder.tvCatalog = (TextView) convertView.findViewById(R.id.trade_members_item_catalog);
                viewHolder.ivAvatar = (ImageView) convertView.findViewById(R.id.trade_members_item_avatar_iv);
                viewHolder.tvNick = (TextView) convertView.findViewById(R.id.trade_members_item_nick);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            String catalog = "";
            if (converterToFirstSpell(nickName).length() > 1) {
                catalog = converterToFirstSpell(nickName).substring(0, 1);
                if (position == 0) {
                    viewHolder.tvCatalog.setVisibility(View.VISIBLE);
                    viewHolder.tvCatalog.setText(mContext.getResources().getString(R.string.president));
                } else {
                    String lastCatalog = converterToFirstSpell(mNicks[position - 1].substring(0, 1));
                    if (catalog.equals(lastCatalog)) {
                        viewHolder.tvCatalog.setVisibility(View.GONE);
                    } else {
                        viewHolder.tvCatalog.setVisibility(View.VISIBLE);
                        viewHolder.tvCatalog.setText(catalog);
                    }
                }
            }
            if (null != presidentList && presidentList.size() > 0&&
                    null != memList && memList.size() > 0) {
                if (position==0){
                    BitmapManager.INSTANCE.loadBitmap(presidentList.get(0).userphoto, viewHolder.ivAvatar,
                            R.drawable.default_avatar, true);
                }else {
                    BitmapManager.INSTANCE.loadBitmap(memList.get(position-1).userphoto, viewHolder.ivAvatar,
                            R.drawable.default_avatar, true);
                    //viewHolder.tvNick.setText(memList.get(position-2).username);
                }
                *//*BitmapManager.INSTANCE.loadBitmap(memList.get(position-2).userphoto, viewHolder.ivAvatar,
                        R.drawable.default_avatar, true);*//*
                if (position==0){
                    viewHolder.tvNick.setText(presidentList.get(0).username);
                }else {
                    viewHolder.tvNick.setText(memList.get(position-1).username);
                }
            }
            //viewHolder.ivAvatar.setImageResource(R.drawable.default_avatar);
            //viewHolder.tvNick.setText(nickName);

            return convertView;
        }

        static class ViewHolder {
            TextView tvCatalog;//目录
            ImageView ivAvatar;//头像
            TextView tvNick;//昵称
        }

        @Override
        public int getPositionForSection(int section) {
            for (int i = 0; i < mNicks.length; i++) {
                String l = converterToFirstSpell(mNicks[i]).substring(0, 1);
                char firstChar = l.toUpperCase().charAt(0);
                if (firstChar == section) {
                    return i;
                }
            }
            return -1;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public Object[] getSections() {
            return null;
        }
    }

    *//**
     * 昵称
     *//*
    // private static String[] nicks = {"阿雅", "北风", "张山", "李四", "欧阳锋", "郭靖", "黄蓉", "杨过", "移联网", "樱木花道", "风清扬", "张三丰", "梅超风"};

    *//**
     * 汉字转换位汉语拼音首字母，英文字符不变
     *
     * @param chines 汉字
     *
     * @return 拼音
     *//*
    public static String converterToFirstSpell(String chines) {
        String pinyinName = "";
        char[] nameChar = chines.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < nameChar.length; i++) {
            if (nameChar[i] > 128) {
                try {
                    pinyinName += PinyinHelper.toHanyuPinyinStringArray(nameChar[i], defaultFormat)[0].charAt(0);
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                pinyinName += nameChar[i];
            }
        }
        return pinyinName;
    }*/
}