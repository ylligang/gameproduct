package com.inwhoop.gameproduct.acitivity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GamelistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;

/**  
 * @Project: MainActivity
 * @Title: GamelistitemActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-17 下午12:01:11
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class GameRecommenditemActivity extends BaseActivity implements IXListViewListener{
	
	private XListView listView = null;

	private  int type; // 是否是热门游戏或者活动游戏

	private int page = 1; // 分页标签

	private List<GameInfo> list = null; // 游戏信息

	private GamelistAdapter adapter = null; // listview的adapter

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = GameRecommenditemActivity.this;
		setContentView(R.layout.game_list_layout);
		type = getIntent().getIntExtra("type", 0);
		initData();
	}
	
	private void initData() {
		listView = (XListView)findViewById(R.id.gamelistview);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(true);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int postion,
					long arg3) {
				Intent intent = new Intent(mContext,
						GameInfosActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("bean",
						adapter.getAll().get(postion-1));
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		read();
	}

	/**
	 * 根据id读取游戏信息
	 * 
	 * @Title: read
	 * @Description: TODO
	 * @param @param id 类型
	 * @return void
	 */
	private void read() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getRecommendGamelistBytypeid(type, -1);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			listView.stopRefresh();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if(list.size()<10){
					listView.setPullLoadEnable(false);
				}
				adapter = new GamelistAdapter(mContext, list);
				listView.setAdapter(adapter);
				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onRefresh() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getRecommendGamelistBytypeid(type, -1);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				rehandler.sendMessage(msg);
			}
		}).start();

	}
	
	private Handler rehandler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			listView.stopRefresh();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if(list.size() == 0){
					listView.setPullLoadEnable(false);
					return;
				}
				if(list.size()<10){
					listView.setPullLoadEnable(false);
				}
				adapter = new GamelistAdapter(mContext, list);
				listView.setAdapter(adapter);
				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onLoadMore() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getGamelistBytypeid(type, adapter.getAll().get(adapter.getAll().size()-1).gameId,1);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				addhandler.sendMessage(msg);
			}
		}).start();
	}
	
	private Handler addhandler = new Handler() {
		public void handleMessage(Message msg) {
			listView.stopLoadMore();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;
			case MyApplication.READ_SUCCESS:
				if(list.size()<10){
					listView.setPullLoadEnable(false);
				}
				adapter.add(list);
				adapter.notifyDataSetChanged();
				break;

			default:
				break;
			}

		};
	};
	
}
