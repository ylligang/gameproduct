package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.MyGiftAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.MyGift;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.utils.SyncHttp;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.XListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/7/1.
 */
public class MyGiftActivity extends BaseActivity implements XListView.IXListViewListener {
    private XListView xList;
    private ProgressBar progressBar;
    private MyGiftAdapter adapter;
    private String minId = "-1";
    private List<MyGift> giftListData = new ArrayList<MyGift>();
    private List<MyGift> giftList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_gift_activity);
        init();
        initView();
    }

    private void initView() {
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.my_gift);
        xList = (XListView) findViewById(R.id.my_gift_activity_list);
        progressBar = (ProgressBar) findViewById(R.id.my_gift_activity_bar);
        xList.setPullRefreshEnable(false);
        xList.setPullLoadEnable(true);
        getGiftListDatas();
    }

    private void getGiftListDatas() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String jsonData = "";
                    String str = "/gamegiftservice_getGiftByTemp.action?userId=" + UserInfoUtil.getUserInfo(MyGiftActivity.this).id + "&minId=" + minId;
                    jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    giftListData.clear();
                    giftListData = JsonUtils.getGiftData(jsonData);
                    if (null != giftListData && giftListData.size() > 0) {
                        xList.setPullLoadEnable(false);
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }


    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            progressBar.setVisibility(View.GONE);
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    adapter = new MyGiftAdapter(MyGiftActivity.this, giftListData);
                    xList.setAdapter(adapter);
                    break;
                case MyApplication.FAIL:
                    MyToast.showShort(MyGiftActivity.this, getResources().getString(R.string.no_data));
                    break;
                case MyApplication.ERROR:
                    break;
            }
        }
    };

    private void getReGiftListDatas() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/gamegiftservice_getGiftByTemp.action?userId=" + UserInfoUtil.getUserInfo(MyGiftActivity.this).id + "&minId=" + minId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    giftList = JsonUtils.getGiftData(jsonData);
                    if (null != giftList && giftList.size() != 0) {
                        if (giftList.size() < 10) {
                            xList.setPullLoadEnable(false);
                        }
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mrHandler.sendMessage(msg);
            }
        }).start();
    }


    public Handler mrHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            xList.stopLoadMore();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    if (giftList.size() < 10) {
                        // list.setPullLoadEnable(false);
                    }
                    adapter.add(giftList);
                    adapter.notifyDataSetChanged();
                    break;
                case MyApplication.FAIL:
                    MyToast.showShort(MyGiftActivity.this, getResources().getString(R.string.no_data));
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(MyGiftActivity.this, getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onRefresh() {

    }

    @Override
    public void onLoadMore() {
        minId = adapter.getAll().get(adapter.getAll().size() - 1).gameId;
        getReGiftListDatas();
    }
}