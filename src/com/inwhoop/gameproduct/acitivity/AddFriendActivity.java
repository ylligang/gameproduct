package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.FriendListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.FriendData;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.view.CustomList;
import com.inwhoop.gameproduct.view.PullToRefreshView;

import java.util.ArrayList;

/**
 * @Describe: TODO   添加好友 界面,可模糊搜索出现列表
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/28 15:31
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class AddFriendActivity extends BaseActivity implements View.OnClickListener,
        PullToRefreshView.OnHeaderRefreshListener, PullToRefreshView.OnFooterRefreshListener {

    private FriendListAdapter adapter;
    private PullToRefreshView my_pull_view;  //刷新组件
    private EditText imputEt;
    private String searchStr = "";

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        mContext = AddFriendActivity.this;

        initData();
    }

    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        getLeftBt().setOnClickListener(this);
        setTitle(R.string.add_friend);

        findViewById(R.id.search_btn).setOnClickListener(this);
        imputEt = (EditText) findViewById(R.id.import_ET);
        KeyBoard.showKeyBoard(imputEt);
        imputEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH)
                    search();
                return true;
            }
        });

        CustomList listView = (CustomList) findViewById(R.id.all_friend_listview);
        adapter = new FriendListAdapter(mContext, new ArrayList<FriendData>());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle b = new Bundle();
                b.putSerializable("bean", adapter.getList().get(position));
                Act.toAct(mContext, AddFriendSureActivity.class, b);
            }
        });

        my_pull_view = (PullToRefreshView) findViewById(R.id.add_friend_pull_refresh_view);
        my_pull_view.setOnHeaderRefreshListener(this);
        my_pull_view.setOnFooterRefreshListener(this);
        my_pull_view.mHeaderView.setVisibility(View.INVISIBLE);  //可以看不到上下，但是能滑着看
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.search_btn:
                search();
                break;
        }
    }

    private void search() {
        String searchStr = imputEt.getText().toString().trim();
        if ("".equals(searchStr)) {
            MyToast.showShort(mContext, R.string.please_import);
            return;
        }
        search(searchStr);
        this.searchStr = searchStr;
        KeyBoard.HiddenInputPanel(mContext);
    }

    @Override
    public void onHeaderRefresh(PullToRefreshView view) {
        my_pull_view.postDelayed(new Runnable() {
            @Override
            public void run() {
                //不做
                my_pull_view.onHeaderRefreshComplete();
            }
        }, 5);
    }

    @Override
    public void onFooterRefresh(PullToRefreshView view) {
        my_pull_view.postDelayed(new Runnable() {
            @Override
            public void run() {
//                adapter.addNoMeList(testAddList("refresh"));
                try {
                    ArrayList<FriendData> sslist = adapter.getList();
                    if (sslist.size() >= 10)    //传最后个数据的id
                        adapter.addNoMeList(JsonUtils.getAllFriendByUser(sslist.get(sslist.size() - 1).id + "", searchStr)
                            ,UserInfoUtil.getUserInfo(mContext).id);
                } catch (Exception e) {
                    LogUtil.e("exception" + e.toString());
                }
                my_pull_view.onFooterRefreshComplete();
            }
        }, 500);
    }

    private ArrayList<FriendData> testAddList(String tag) {
        ArrayList<FriendData> list = new ArrayList<FriendData>();
        for (int i = 0; i < 6; i++) {
            FriendData bean = new FriendData();
            bean.username = "name" + i;
//            bean.age = i + "";
//            bean.sex = "妖";
            bean.userphoto = "asdasdad";

            list.add(bean);
        }
        return list;
    }

    //TODO  查询接口
    private void search(final String searchStr) {
        showProgressDialog("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getAllFriendByUser("-1", searchStr);
//                    msg.obj = testAddList("查询");
                    msg.what = MyApplication.SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.FAIL:
                    MyToast.showShort(mContext, "数据异常");
                    break;

                case MyApplication.SUCCESS:
                    ArrayList<FriendData> newList = (ArrayList<FriendData>) msg.obj;
                    if (newList.size() == 0)
                        MyToast.showLong(mContext, R.string.search_no_friend);
                    else {
                        adapter.getList().clear();
                        adapter.addNoMeList(newList,UserInfoUtil.getUserInfo(mContext).id);
                    }
                    break;
            }
        }
    };
}
