package com.inwhoop.gameproduct.acitivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GiftViewPagerAdapters;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.dao.DialogLinsener;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.fragment.GameInfoBaggiftFragment;
import com.inwhoop.gameproduct.fragment.GameInformationFragment;
import com.inwhoop.gameproduct.utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZK
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 游戏详情页面,需要右上已搜藏按钮，需要接口返回是否搜藏状态
 * @date 2014/4/17 16:27
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GameInfosActivity extends BaseFragmentActivity implements
		View.OnClickListener, DialogLinsener {

	private static final char COLLECT_SUCCESS = 0x01;

	private static final char IMGS_SUCCESS = 0x02;

	private ImageView titleIMG;// 上面大图，显示游戏的

	private RadioGroup radioGroup;

	private ViewPager viewPager;

	private RadioButton[] btns;

	// 定义数组来存放按钮图片
	private int drawableArray[] = { R.drawable.gameinfo_left_btn,
			R.drawable.gameinfo_right_btn };

	// 定义数组来存放按钮图片
	private int drawableArray_select[] = {
			R.drawable.gameinfo_left_btn_pressed,
			R.drawable.gameinfo_right_btn_pressed };

	// // 定义数组来存放按钮图片 :灰色
	// private int drawableArray_nomal[] = {R.drawable.icon_home_nor,
	// R.drawable.icon_home_nor, R.drawable.icon_home_nor,
	// R.drawable.icon_home_nor};

	private GameInfo gameInfo = new GameInfo();// 传到此界面的游戏对象

	private FragmentManager fm = getSupportFragmentManager();
	private UserInfo userInfo = new UserInfo();

    @Override
    protected void onResume() {
        super.onResume();
        userInfo=UserInfoUtil.getUserInfo(mContext);
        if (!"".equals(userInfo.id)){
            getGameinfoByRec();
        }
    }

    public void onCreate(Bundle savedInstanceState) { // TODO onCreate
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_infos);

		mContext = GameInfosActivity.this;
		userInfo = UserInfoUtil.getUserInfo(mContext);

		gameInfo = (GameInfo) getIntent().getSerializableExtra("bean");
		initData();

		getGameinfoByRec();
	}

	private void initData() {// TODO 初始化数据
		init();
		setHeadLeftButton(R.drawable.back);
		setRightSecondBt(R.drawable.soucang_ico);
		// setTitle(R.string.game_infos);
		setTitleStr(gameInfo.gameName);// 改成显示游戏名
		getRightSecondBt().setOnClickListener(this);

		titleIMG = (ImageView) findViewById(R.id.game_infos_titleImg_IV);
		titleIMG.setOnClickListener(this);
		BitmapManager.INSTANCE.loadBitmap(gameInfo.imgUrl, titleIMG,
				R.drawable.loading, true);
		titleIMG.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, MyApplication.heightPixels / 3));
		// 底部viewpager切换后，实际radiobtn没有切换，radiobtn再次点击则切换不了界面，所以改用单独按钮的点击事件
		// radioGroup = (RadioGroup) findViewById(R.id.game_infos_radiogroup);
		// radioGroup.setOnCheckedChangeListener(new
		// MyRadioGroupCheckListener());
		RadioButton btn1 = (RadioButton) findViewById(R.id.game_infos_first_radioBtn);
		RadioButton btn2 = (RadioButton) findViewById(R.id.game_infos_second_radioBtn);
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btns = new RadioButton[] { btn1, btn2 };
		setRadioButton(0);

		setCanCheckViewPager();

	}

	/**
	 * TODO 设置viewpager
	 */
	private void setCanCheckViewPager() {
		Fragment[] frags = { new GameInformationFragment(gameInfo.gameId),
				new GameInfoBaggiftFragment(gameInfo.gameId) };
		List<Fragment> fragments = new ArrayList<Fragment>();
		for (int i = 0; i < frags.length; i++) {
			fragments.add(frags[i]);
		}
		viewPager = (ViewPager) findViewById(R.id.game_infos_bottom_viewpager);
		viewPager.setCurrentItem(0);
		viewPager.setOffscreenPageLimit(2);
		viewPager.setAdapter(new GiftViewPagerAdapters(fm, fragments));
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int i, float v, int i2) {

			}

			@Override
			public void onPageSelected(int position) {
				setRadioButton(position);
			}

			@Override
			public void onPageScrollStateChanged(int i) {

			}
		});
	}

	/**
	 * TODO 设置radioButton被点击时的颜色切换
	 * 
	 * @param index
	 *            被选中下标
	 */
	private void setRadioButton(int index) {
		for (int i = 0; i < btns.length; i++) {
			btns[i].setTextColor(mContext.getResources().getColor(
					R.color.blue_33a6ff));
			btns[i].setBackgroundResource(drawableArray[i]);
		}
		btns[index].setTextColor(mContext.getResources()
				.getColor(R.color.white));
		btns[index].setBackgroundResource(drawableArray_select[index]);
	}

	/**
	 * TODO 点击事件
	 * 
	 * @param v
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.head_right_second: //搜藏
			if (isGotoLoginDialog())
				collect();
			break;
		case R.id.game_infos_titleImg_IV:  //图集
			Bundle b = new Bundle();
			b.putSerializable("bean", gameInfo);
			Act.toAct(mContext, GameImagesActivity.class, b);
			break;

		case R.id.game_infos_first_radioBtn: //左游戏资讯
			setRadioButton(0);
			viewPager.setCurrentItem(0);
			break;
		case R.id.game_infos_second_radioBtn: //右活动礼包
			setRadioButton(1);
			viewPager.setCurrentItem(1);
			break;
		}
	}

	protected boolean isGotoLoginDialog() {
		UserInfo userInfo = UserInfoUtil.getUserInfo(mContext);
		if (!"".equals(userInfo.id)) {
			return true;
		}

		buildDialog(this, R.string.alert, R.string.login_alert);

		return false;
	}

	// TODO 获取游戏详情接口，目前只有上面大图集合，以及一句话
	private void getGameinfoByRec() {
		showProgressDialog("");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					// String gameid = "" + gameInfo.gameId;
					// String userid = "1";
					JsonUtils.getGameinfoByRec(userInfo.id,gameInfo);
					msg.what = IMGS_SUCCESS;
				} catch (Exception e) {
                    LogUtil.e("exception,游戏详情(╯▽╰ )："+e.toString());
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	// TODO 收藏接口
	private void collect() {
		showProgressDialog("");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					String gameid = "" + gameInfo.gameId;
					String userid = "" + userInfo.id;
					msg.obj = JsonUtils.recGameinfoByUser(gameid, userid);
					msg.what = COLLECT_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				MyToast.showShort(mContext, "数据异常");
				break;
			case COLLECT_SUCCESS:
                setRightSecondBt(R.drawable.soucang_ico_colect);
                MyToast.showShort(mContext, msg.obj + "");
				break;
			case IMGS_SUCCESS: // 此时已经得到图集的图片。在gameInfo的infoimages里
                setColectbtn();
				break;
			}
		}

		;
	};

    //接口返回是否搜藏
    private void setColectbtn() {
        if (!gameInfo.isState && !"".equals(userInfo.id)){
           setRightSecondBt(R.drawable.soucang_ico_colect);
        }
    }

    @Override
	public void onSure() {
		Bundle bundle = new Bundle();
		Intent intent = new Intent(mContext, LoginActivity.class);
		intent.putExtras(bundle);
		startActivity(intent);

	}

	@Override
	public void onCancle() {

	}

}