package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.LoopInfoBean;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

/**
 * @Describe: TODO       圈子资料界面，如果是自己圈子，等后台判断
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/05/30 10:49
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class AddLoopSureActivity extends BaseActivity implements View.OnClickListener {

    private static final char ADD_OK = 0x01;
    private UserInfo userInfo;
    private LoopInfoBean loopBean = new LoopInfoBean();
    private ImageView headImg;
    private TextView loopNameEt, loopNumEt, loopInfoEt;  //需要根据状态是否可编译
    private Button getOutLoopBtn;

    private int CAMERA = 1001; // 调用相机的返回码
    private int PICTURE = 1002; // 调用相册的返回码
    private String picpath = "";

    private PopupWindow popupWindow = null;
    private boolean isEditTag = false;//表示是否可编译状态

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loop_info);
        mContext = AddLoopSureActivity.this;
        userInfo = UserInfoUtil.getUserInfo(mContext);
        loopBean = (LoopInfoBean) getIntent().getSerializableExtra("bean");

        initData();

        setLoopInfo();

//        initNetData();
    }

    private void initNetData() {
        getLoopInfo(userInfo.id, loopBean.id);
    }


    private void initData() {
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.loop_info);
        getLeftBt().setOnClickListener(this);

        headImg = (ImageView) findViewById(R.id.head_img);
        findViewById(R.id.loop_head_layout).setOnClickListener(this);
        loopNameEt = (TextView) findViewById(R.id.loop_name);
        findViewById(R.id.loop_name_layout).setOnClickListener(this);
        loopNumEt = (TextView) findViewById(R.id.loop_num);
        findViewById(R.id.loop_number_layout).setOnClickListener(this);

        loopInfoEt = (TextView) findViewById(R.id.loop_instructions);
        getOutLoopBtn = (Button) findViewById(R.id.btn_add_or_getout);
        getOutLoopBtn.setOnClickListener(this);

        findViewById(R.id.headupdate).setVisibility(View.GONE);
        findViewById(R.id.nameimg).setVisibility(View.GONE);
        findViewById(R.id.introimg).setVisibility(View.GONE);


        initPopwindow();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.head_left:
                finish();
                break;

            case R.id.loop_head_layout:
                if (isEditTag) {
                    showPopupWindow();
                }
                break;
            case R.id.loop_name_layout:
                if (isEditTag) {
                    loopNameEt.setEnabled(true);
                }
                break;
            case R.id.loop_number_layout:
                if (isEditTag) {

                }
                break;
            case R.id.loop_instructions:
                if (isEditTag) {
                    loopInfoEt.setEnabled(true);
                }
                break;
            case R.id.btn_add_or_getout:
                if (!isEditTag) {  //主要此不可编译表示按钮是“加入圈子”
                    getinLoop(loopBean.id, userInfo.id);
                }else {

                }
                break;
        }
    }

    //TODO 加入圈子
    private void getinLoop(final int circleid, final String userId) {
        showProgressDialog("");
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.joinCircle("" + circleid, userId);
                    msg.what = ADD_OK;
                } catch (Exception e) {
                    LogUtil.e("exception"+e.toString());
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    //TODO 查询圈子详情，无用
    private void getLoopInfo(final String userId, final int loopId) {
        showProgressDialog("");
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.getLoopInfo(userId, loopId);
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_SUCCESS:
                    loopBean = (LoopInfoBean) msg.obj;
                    setLoopInfo();
                    break;
                case MyApplication.READ_FAIL:
                    MyToast.showShort(mContext, "数据异常");
                    break;

                case ADD_OK:
                    String ss=msg.obj+"";
                    if(ss.contains("成功")){
                    	ClientGroupServer.getInstance(mContext).addMultiuserchat(""+loopBean.id, loopBean.circlename);
                    }
                    MyToast.showShort(mContext, "" + ss);
                    break;
            }
        }
    };

    //TODO  设置圈子信息，开始进来设置下，查询详情后再设置下
    private void setLoopInfo() {
        BitmapManager.INSTANCE.loadBitmap(loopBean.circleheadphoto, headImg, R.drawable.loading, true);
        loopNameEt.setText("" + loopBean.circlename);
        loopNumEt.setText("" + loopBean.personNum + "人");
        loopInfoEt.setText("" + loopBean.circleremark);
        getOutLoopBtn.setText("" + loopBean.isLoopHead(mContext));
    }


    /**
     * 初始化播放进度条popupwindow
     */
    private void initPopwindow() {
        View view = LayoutInflater.from(mContext).inflate(
                R.layout.sendpic_popupwindow_layout, null);
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, true);
        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        RelativeLayout cancelLayout = (RelativeLayout) view
                .findViewById(R.id.canclelayout);
        cancelLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
                return false;
            }
        });
        TextView takepic = (TextView) view.findViewById(R.id.takepic);
        takepic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(camera, CAMERA);
            }
        });
        TextView photo = (TextView) view.findViewById(R.id.photo);
        photo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Intent picture = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(picture, PICTURE);
            }
        });
        Button cancle = (Button) view.findViewById(R.id.cancel);
        cancle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
            }
        });
    }

    /**
     * @param @param view
     *
     * @return void
     *
     * @Title: showPopupWindow
     * @Description: TODO 弹出选择照片小窗
     */
    public void showPopupWindow() {
        popupWindow.setAnimationStyle(R.style.dialogstyle);
        popupWindow.showAsDropDown(LayoutInflater.from(mContext).inflate(
                R.layout.create_loop_activity, null));
    }

    @SuppressWarnings("static-access")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA && resultCode == Activity.RESULT_OK
                && null != data) {
            String sdState = Environment.getExternalStorageState();
            if (!sdState.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
                return;
            }
            String name = new DateFormat().format("yyyyMMdd_hhmmss",
                    Calendar.getInstance(Locale.CHINA))
                    + ".jpg";
            Bundle bundle = data.getExtras();
            Bitmap bitmap = (Bitmap) bundle.get("data");// 获取相机返回的数据，并转换为Bitmap图片格式
            FileOutputStream b = null;
            File file = new File(Environment.getExternalStorageDirectory()
                    + "/gameproduct/camera");
            file.mkdirs();// 创建文件夹
            String fileName = Environment.getExternalStorageDirectory()
                    + "/gameproduct/camera/" + name;
            try {
                b = new FileOutputStream(fileName);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// 把数据写入文件
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    b.flush();
                    b.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            picpath = fileName;
        }

        if (requestCode == PICTURE && resultCode == Activity.RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumns = {MediaStore.Images.Media.DATA};
            Cursor c = this.getContentResolver().query(selectedImage,
                    filePathColumns, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePathColumns[0]);
            String picturePath = c.getString(columnIndex);
            picpath = picturePath;
            c.close();
        }
        if (!"".equals(picpath)) {
            headImg.setImageBitmap(BitmapFactory.decodeFile(picpath));
        }
    }
}
