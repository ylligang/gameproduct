package com.inwhoop.gameproduct.acitivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import android.widget.RelativeLayout.LayoutParams;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.GameImagesInfo;
import com.inwhoop.gameproduct.entity.GameInfo;
import com.inwhoop.gameproduct.utils.BitmapManager;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.utils.MyToast;
import com.inwhoop.gameproduct.view.HackyViewPager;
import com.inwhoop.gameproduct.view.PhotoView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: GameImagesActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * @date 2014-4-26 上午11:06:17
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GameImagesActivity extends BaseActivity {

    private static final String ISLOCKED_ARG = "isLocked";

    private ViewPager viewPager = null;

    private LinearLayout textLayout = null;

    private TextView contentTextView = null;

    private List<GameImagesInfo> list = null;

//    private List<View> imgs = new ArrayList<View>();

    private LayoutInflater inflater = null;

    private boolean flag = true;

    private GameInfo info = null;

//    private MenuItem menuLockItem;

    private String textInfo = "";//下面要显示的信息


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_img_layout);
        mContext = GameImagesActivity.this;
        inflater = LayoutInflater.from(mContext);
        info = (GameInfo) getIntent().getSerializableExtra("bean");

        init(savedInstanceState);
        read();
    }

    public void init(Bundle savedInstanceState) {
        super.init();
        setTitle(R.string.gameimages);
        setHeadLeftButton(R.drawable.back);
        viewPager = (HackyViewPager) findViewById(R.id.gameviewpager);
        textLayout =(LinearLayout) findViewById(R.id.lin_layout);
        contentTextView = (TextView) findViewById(R.id.imgtv);
        RelativeLayout.LayoutParams parm = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, getWindowManager()
                .getDefaultDisplay().getHeight() / 4
        );
        parm.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        findViewById(R.id.scroll).setLayoutParams(parm);

        textLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInfoIfVisible();
            }
        });

//        if (savedInstanceState != null) {
//            boolean isLocked = savedInstanceState.getBoolean(ISLOCKED_ARG, false);
//            ((HackyViewPager) viewPager).setLocked(isLocked);
//        }

        setIsLockBtn();
    }



    //读取网络图集
    private void read() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    list = JsonUtils.getGameImages(info.gameId);
                    msg.what = MyApplication.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = MyApplication.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case MyApplication.READ_FAIL:

                    break;

                case MyApplication.READ_SUCCESS:
                    if (list.size() == 0) {
                        MyToast.showShort(mContext, "暂时没有任何图片");
                        return;
                    }
//                    for (int i = 0; i < list.size(); i++) {
//                        View view = inflater.inflate(R.layout.show_img_item,
//                                null);
//                        PhotoView img = (PhotoView) view.findViewById(R.id.img);
//                        img.setImageURIByBitmapManager(list.get(i).imgUrl);
////                        BitmapManager.INSTANCE.loadBitmap(list.get(i).imgUrl,
////                                img, R.drawable.loading, true);
//                        imgs.add(view);
//                    }
                    viewPager.setAdapter(new MyAdapter());
                    viewPager.setOnPageChangeListener(new MypageListener());
                    textInfo = 1 + "/" + list.size() + "\n" +
                            list.get(0).imgUrlTxt;
                    contentTextView.setText(textInfo);

                    break;

                default:
                    break;
            }
        }
    };

    private void setInfoIfVisible() {
        if (flag) {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.dialog_exit);
            contentTextView.startAnimation(animation);
            flag = false;
        } else {
            contentTextView.setText(textInfo);
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.dialog_enter);
            contentTextView.startAnimation(animation);
            flag = true;
        }
    }

    class MypageListener implements OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int position) {
            textInfo = (position + 1) + "/" + list.size() + "\n" +
                    list.get(position).imgUrlTxt;

            contentTextView.setText(textInfo);
        }
    }

    class MyAdapter extends PagerAdapter {

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return (arg0 == arg1);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            PhotoView photoView = new PhotoView(container.getContext());
            photoView.setImageURIByBitmapManager(list.get(position).imgUrl);

            // Now just add PhotoView to ViewPager and return it
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            return photoView;

        }
    }

     //以下注释为设置图片是否锁定而放缩，目前有bug滑动回来之后不能放缩了
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.viewpager_menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        menuLockItem = menu.findItem(R.id.menu_lock);
//        toggleLockBtnTitle();
//        menuLockItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                toggleViewPagerScrolling();
//                toggleLockBtnTitle();
//                return true;
//            }
//        });
//
//        return super.onPrepareOptionsMenu(menu);
//    }
//
    private void toggleViewPagerScrolling() {
        if (isViewPagerActive()) {
            ((HackyViewPager) viewPager).toggleLock();
        }
    }
//
//    private void toggleLockBtnTitle() {
//        boolean isLocked = false;
//        if (isViewPagerActive()) {
//            isLocked = ((HackyViewPager) viewPager).isLocked();
//        }
//        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
//        if (menuLockItem != null) {
//            menuLockItem.setTitle(title);
//        }
//    }
//
    private boolean isViewPagerActive() {
        return (viewPager != null && viewPager instanceof HackyViewPager);
    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        if (isViewPagerActive()) {
//            outState.putBoolean(ISLOCKED_ARG, ((HackyViewPager) viewPager).isLocked());
//        }
//        super.onSaveInstanceState(outState);
//    }

    //设置右上角按钮，以使图片固定此页可最大化
    private void setIsLockBtn() {
        boolean isLocked = false;
        isLocked = ((HackyViewPager) viewPager).isLocked();
        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
        setRightSecondBtText(title);
        head_right_bt_2.setTextSize(15);
        toBtnTitle();

        getRightSecondBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViewPagerScrolling();
                toBtnTitle();
            }
        });
    }

    private void toBtnTitle() {
        boolean isLocked = false;
        if (isViewPagerActive()) {
            isLocked = ((HackyViewPager) viewPager).isLocked();
        }
        String title = (isLocked) ? getString(R.string.menu_unlock) : getString(R.string.menu_lock);
        setRightSecondBtText(title);
        head_right_bt_2.setTextSize(15);
    }
}
