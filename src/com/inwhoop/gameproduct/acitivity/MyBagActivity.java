package com.inwhoop.gameproduct.acitivity;

import android.os.Bundle;
import android.view.View;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GetGiftBagOkAdapter;
import com.inwhoop.gameproduct.entity.Gift;
import com.inwhoop.gameproduct.view.SlipListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO  已领取礼包 界面
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/03 15:03
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MyBagActivity extends BaseActivity {

    private SlipListView listView;
    private List<Gift> giftLists = new ArrayList<Gift>();   //存放数据的list
    private GetGiftBagOkAdapter adapter;  //Listview适配器

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bag);
        mContext = MyBagActivity.this;
        init();
        setHeadLeftButton(R.drawable.back);
        setTitle(R.string.get_bag_ok_title);

        listView = (SlipListView) findViewById(R.id.listView_my_bag);
        adapter = new GetGiftBagOkAdapter(mContext, giftLists);
        listView.setAdapter(adapter);
        adapter.setOnRightItemClickListener(new GetGiftBagOkAdapter.onRightItemClickListener() {
            @Override
            public void onRightItemClick(View v, int position) {
                giftLists.remove(position);
                adapter.notifyDataSetChanged();
            }
        });

        initListData();
    }

    private void initListData() {
        for (int i = 0; i < 3; i++) {
            Gift gift=new Gift();
            gift.name=i+"";
            gift.type="特权卡";
            gift.cd_key="SFBJB789NJS";

            giftLists.add(gift);
        }

        adapter.notifyDataSetChanged();
    }

}
