package com.inwhoop.gameproduct.acitivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.fragment.GameLoopFragment;
import com.inwhoop.gameproduct.fragment.GiftBagPlatformFragment;
import com.inwhoop.gameproduct.fragment.HomeFragment;
import com.inwhoop.gameproduct.fragment.PersonFragment;

import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;
import com.inwhoop.gameproduct.fragment.MessageFragment;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;
import com.inwhoop.gameproduct.xmpp.MypacketListener;
import com.inwhoop.gameproduct.xmpp.XmppConnectionUtil;

/**
 * 首页主体框架
 *
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: MainActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * @date 2014-4-2 下午2:26:26
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class MainActivity extends BaseFragmentActivity {

    public TabHost mTabHost;
    private TabWidget tabWidget;
    private LayoutInflater layoutInflater;
    // Tab选项卡的文字
    private String mTextviewArray[];
    // 存放Fragment界面
    private Class<?> fragmentArray[] = {HomeFragment.class,
            GiftBagPlatformFragment.class, MessageFragment.class,
            GameLoopFragment.class, PersonFragment.class};

    private HomeFragment hoFragment = null;

    private GiftBagPlatformFragment bagPlatformFragment = null;

    public MessageFragment messageFragment = null;

    private GameLoopFragment gameLoopFragment = null;

    private PersonFragment personFragment = null;

    // 定义数组来存放按钮图片
    private int drawableArray[] = {R.drawable.xml_tabhost_home,
            R.drawable.xml_tabhost_gift, R.drawable.xml_tabhost_msg,
            R.drawable.xml_tabhost_cicle1, R.drawable.xml_tabhost_mine};

    private FragmentManager mFm = null;

    private FragmentTransaction mFt;

    public static int old_pos = 2;// 上一次的选择 .写成静态公共，如登录后跳转较方便

    @Override
    protected void onResume() {
        super.onResume();
        if (MyApplication.isLoginBack) {
            mTabHost.setCurrentTab(old_pos);
            MyApplication.isLoginBack = false;
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = MainActivity.this;
        MyApplication.context = mContext;
        MyApplication.widthPixels = getWindowManager().getDefaultDisplay()
                .getWidth();
        MyApplication.heightPixels = getWindowManager().getDefaultDisplay()
                .getHeight();
        MyApplication.packName = getPackageName();
        MyApplication.APP_PATH = Environment.getExternalStorageDirectory()
                .getPath()
                + "/Android/data/"
                + MyApplication.packName
                + "/gamecache/";
        mFm = getSupportFragmentManager();
        initView();

        langLight();
    }

    private void langLight() {  //屏幕 常亮
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
// in onResume() call

        mWakeLock.acquire();
// in onPause() call
        mWakeLock.release();
    }
    

    /**
     * 初始化
     *
     * @Title: initView
     * @Description: TODO void
     */
    private void initView() {
        IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
        filter.addAction(MyApplication.AGAIN_CONNECT);
        registerReceiver(mRecever, filter);
        layoutInflater = LayoutInflater.from(this);
        mTextviewArray = getResources().getStringArray(R.array.tab);
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabWidget = (TabWidget) findViewById(android.R.id.tabs);
        mTabHost.setup();
        // 得到fragment的个数
        final int count = fragmentArray.length;

        TabHost.OnTabChangeListener tabChangeListener = new TabHost.OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
                mFt = mFm.beginTransaction();
                if (null != hoFragment) {
                    mFt.hide(hoFragment);
                }
                if (null != bagPlatformFragment) {
                    mFt.hide(bagPlatformFragment);
                }
                if (null != messageFragment) {
                    mFt.hide(messageFragment);
                }
                if (null != gameLoopFragment) {
                    mFt.hide(gameLoopFragment);
                }
                if (null != personFragment) {
                    mFt.hide(personFragment);
                }
                if (tabId.equalsIgnoreCase(mTextviewArray[0])) {
                    setFragment(0);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[1])) {
                    setFragment(1);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[2])) {
                    setFragment(2);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[3])) {
                    setFragment(3);
                } else if (tabId.equalsIgnoreCase(mTextviewArray[4])) {
                    setFragment(4);
                }
                mFt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                mFt.commit();
            }
        };
        mTabHost.setOnTabChangedListener(tabChangeListener);
        for (int i = 0; i < count; i++) {
            // 为每一个Tab按钮设置图标、文字和内容
            TabSpec tabSpec = mTabHost.newTabSpec(mTextviewArray[i])
                    .setIndicator(getTabItemView(i));
            tabSpec.setContent(new DummyTabContent(this));
            mTabHost.addTab(tabSpec);
            // 将Tab按钮添加进Tab选项卡中
            // mTabHost.addTab(tabSpec, fragmentArray[i], null);
            // // 设置Tab按钮的背景
            // mTabHost.getTabWidget().getChildAt(i)
            // .setBackgroundResource(R.drawable.home_btn_bg);
        }
        mTabHost.setCurrentTab(0);
    }

    private void setFragment(int pos) {
        if ((pos == 2 || pos == 3 || pos == 4)
                && UserInfoUtil.getUserInfo(mContext).id.equals("")) {
            Act.toAct(mContext, LoginActivity.class);
        } else
            old_pos = pos;

        switch (pos) {
            case 0:
                if (null == hoFragment) {
                    hoFragment = new HomeFragment();
                    mFt.add(R.id.realtabcontent, hoFragment, mTextviewArray[pos]);
                } else {
                    mFt.show(hoFragment);
                }
                break;
            case 1:
                if (null == bagPlatformFragment) {
                    bagPlatformFragment = new GiftBagPlatformFragment(mFm);
                    mFt.add(R.id.realtabcontent, bagPlatformFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(bagPlatformFragment);
                }
                break;
            case 2:
                if (null == messageFragment) {
                    messageFragment = new MessageFragment();
                    mFt.add(R.id.realtabcontent, messageFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(messageFragment);
                }
                break;
            case 3:
                if (null == gameLoopFragment) {
                    gameLoopFragment = new GameLoopFragment();
                    mFt.add(R.id.realtabcontent, gameLoopFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(gameLoopFragment);
                }
                break;
            case 4:
                if (null == personFragment) {
                    personFragment = new PersonFragment();
                    mFt.add(R.id.realtabcontent, personFragment,
                            mTextviewArray[pos]);
                } else {
                    mFt.show(personFragment);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 给Tab按钮设置图标和文字
     *
     * @param index tab下标
     *
     * @return View
     *
     * @Title: getTabItemView
     * @Description: TODO
     */

    private View getTabItemView(int index) {
        View view = layoutInflater.inflate(R.layout.tab_item_view, null);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
        imageView.setImageResource(drawableArray[index]);

        TextView textView = (TextView) view.findViewById(R.id.textview);
        textView.setText(mTextviewArray[index]);

        return view;
    }


    private boolean isFinished = false; // 返回键是否一定时间内第一次按，标识

    @SuppressWarnings("deprecation")
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isFinished) {
                try {
                    ClientGroupServer.client = null;
                    ClienConServer.client = null;
                    if (null != messageFragment && null != messageFragment.mRecever) {
                        unregisterReceiver(messageFragment.mRecever);
                    }
                    MyApplication.context = null;
                    MypacketListener.getInstance().loginout();
                    XmppConnectionUtil.getInstance().closeConnection();
                } catch (Exception e) {
                }
                System.exit(0);
            }else {
                MyToast.showShort(mContext, "再按一次返回键退出");
                new Thread() {
                    public void run() {
                        isFinished = true;
                        try {
                            Thread.sleep(2000);
                            isFinished = false;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
        }
        return false;
    }

    public BroadcastReceiver mRecever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MyApplication.AGAIN_CONNECT)) {
                LoginOpenfireUtil.loginOpenfire(mContext);
                Intent intent1 = new Intent();
                intent1.setAction(MyApplication.CHAT_AGAIN_INIT);
                sendBroadcast(intent1);
            }
        }
    };

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try{
			unregisterReceiver(mRecever);
		}catch(Exception e){
			
		}
	}
    
    

}
