package com.inwhoop.gameproduct.acitivity;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.MailListInfo;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.utils.UserInfoUtil;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @Project: MainActivity
 * @Title: MailListActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-5-30 下午3:04:57
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MailListActivity extends BaseActivity {

	private ListView mailListView = null;

	public LayoutInflater inflater = null;

	private List<MailListInfo> maillist = new ArrayList<MailListInfo>();

	private List<MailListInfo> netlist = new ArrayList<MailListInfo>();

	private List<MailListInfo> alllist = new ArrayList<MailListInfo>();

	private IntentFilter mFilter01;

	private String SMS_SEND_ACTIOIN = "SMS_SEND_ACTIOIN";

	private MyServiceReceiver mReceiver01 = null;

	private int nowposition = 0;
	
	private MyAdapter adapter = null;
	
	private int teltype = 1;
	
	private int id = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mail_list_layout);
		mContext = MailListActivity.this;
		inflater = LayoutInflater.from(mContext);
		teltype = getIntent().getIntExtra("teltype", 1);
		id = getIntent().getIntExtra("id", 0);
		init();
		read(true);
	}

	@Override
	public void init() {
		super.init();
		setTitle(R.string.loop_mail);
		setHeadLeftButton(R.drawable.back);
		mailListView = (ListView) findViewById(R.id.maillist);
	}

	private void read(boolean flag) {
		if (flag) {
			showProgressDialog("正在加载中...");
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					netlist = JsonUtils.getMaillist(UserInfoUtil
							.getUserInfo(mContext).id,""+teltype);
					maillist = getPhoneContacts();
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
				showToast("数据读取失败~~");
				break;

			case MyApplication.READ_SUCCESS:
				alllist = compareLocalAndNet();
				adapter = new MyAdapter(alllist);
				mailListView.setAdapter(adapter);
				break;

			default:
				break;
			}

		};
	};

	private List<MailListInfo> compareLocalAndNet() {
		List<MailListInfo> invatelist = new ArrayList<MailListInfo>();
		List<MailListInfo> novatelist = new ArrayList<MailListInfo>();
		List<MailListInfo> alllist = new ArrayList<MailListInfo>();
		for (int i = 0; i < maillist.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < netlist.size(); j++) {
				if (maillist.get(i).tel.equals(netlist.get(j).tel)) {
					flag = true;
					break;
				}
			}
			if (flag) {
				maillist.get(i).isInvate = true;
				invatelist.add(maillist.get(i));
			} else {
				maillist.get(i).isInvate = false;
				novatelist.add(maillist.get(i));
			}
		}
		for (int i = 0; i < novatelist.size(); i++) {
			alllist.add(novatelist.get(i));
		}
		for (int i = 0; i < invatelist.size(); i++) {
			alllist.add(invatelist.get(i));
		}
		return alllist;
	}

	/**
	 * 
	 * @Title: getPhoneContacts
	 * @Description: TODO 获取通讯录信息
	 * @param @return
	 * @return ArrayList<PhoneContactor>
	 */
	private List<MailListInfo> getPhoneContacts() {
		ContentResolver resolver = this.getContentResolver();
		ArrayList<MailListInfo> list = new ArrayList<MailListInfo>();
		String[] columns = new String[] { Phone.DISPLAY_NAME, Phone.NUMBER,
				Phone.PHOTO_ID, Phone.CONTACT_ID };
		Cursor cursor = resolver.query(Phone.CONTENT_URI, columns, null, null,
				null);
		while (cursor.moveToNext()) {
			MailListInfo pc = new MailListInfo();
			String name = cursor.getString(cursor
					.getColumnIndex(Phone.DISPLAY_NAME));
			String number = cursor.getString(cursor
					.getColumnIndex(Phone.NUMBER));
			if (TextUtils.isEmpty(number)) { // 判断号码是否为空
				continue;
			}
			pc.mailname = name;
			pc.tel = number;
			list.add(pc);
		}
		cursor.close();
		return list;
	}

	class MyServiceReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			dismissProgressDialog();
			try {
				switch (getResultCode()) {
				/* 发送短信成功 */
				case Activity.RESULT_OK:
					System.out.println("=========RESULT_OK============");
					upload();
					showToast("发送成功");
					alllist.get(nowposition).isInvate = true;
					adapter.notifyDataSetChanged();
					break;
				/* 发送短信失败 */
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					showToast("发送失败");
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:

					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:

					break;
				default:
					break;
				}
			} catch (Exception e) {
				e.getStackTrace();
			}
		}
	}

	private void upload() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					boolean flag = JsonUtils.UploadMaillist(
							UserInfoUtil.getUserInfo(mContext).id,
							alllist.get(nowposition).tel);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				uphandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler uphandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case MyApplication.READ_FAIL:
//				showToast("数据读取失败~~");
				break;

			case MyApplication.READ_SUCCESS:
//				read(false);
				break;

			default:
				break;
			}

		};
	};

	class MyAdapter extends BaseAdapter {

		public List<MailListInfo> list = null;

		public MyAdapter(List<MailListInfo> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			Holder holder = null;
			if (null == convertView) {
				holder = new Holder();
				convertView = inflater.inflate(R.layout.mail_list_item, null);
				holder.name = (TextView) convertView.findViewById(R.id.name);
				holder.invate = (TextView) convertView.findViewById(R.id.send);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.name.setText(list.get(position).mailname);
			if (!list.get(position).isInvate) {
				holder.invate.setBackgroundResource(R.drawable.sub_back_shape);
				holder.invate.setText("发送邀请");
				holder.invate.setTag(position);
				holder.invate.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int pos = (Integer) v.getTag();
						nowposition = pos;
						mFilter01 = new IntentFilter(SMS_SEND_ACTIOIN);
						mReceiver01 = new MyServiceReceiver();
						registerReceiver(mReceiver01, mFilter01);
						SmsManager smsManager = SmsManager.getDefault();
//						Intent itSend = new Intent(SMS_SEND_ACTIOIN);
						Intent itDeliver = new Intent(SMS_SEND_ACTIOIN);
						/* sentIntent参数为传送后接受的广播信息PendingIntent */
						PendingIntent mSendPI = PendingIntent.getBroadcast(
								getApplicationContext(), 0, itDeliver, 0);
						/* deliveryIntent参数为送达后接受的广播信息PendingIntent */
//						PendingIntent mDeliverPI = PendingIntent.getBroadcast(
//								getApplicationContext(), 0, itDeliver, 0);
						/* 发送SMS短信，注意倒数的两个PendingIntent参数 */
						smsManager.sendTextMessage(alllist.get(pos).tel, null,
                                getResources().getString(R.string.app_name), mSendPI, null);
						showProgressDialog("正在发送...");
//						Intent intent = new Intent(Intent.ACTION_VIEW);
//						intent.putExtra("address", "" + alllist.get(pos).tel);
//						intent.putExtra("sms_body", "The SMS text");  
//						intent.setType("vnd.android-dir/mms-sms");
//						startActivity(intent);
					}
				});
			} else {
				holder.invate
						.setBackgroundResource(R.drawable.invate_back_shape);
				holder.invate.setText("已邀请");
			}

			return convertView;
		}

		class Holder {
			public TextView name;
			public TextView invate;
		}

	}

}
