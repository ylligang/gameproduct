package com.inwhoop.gameproduct.acitivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.*;

/**
 * 登录页面-dingwenlong
 * Created by Administrator on 2014/4/4.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    public static final int CHANGE_PWD = 202;
    private EditText userName;
    private EditText passWord;
    private Button login;
    private TextView registered;
    private TextView forgetPassword;
    private RelativeLayout back;
    private UserInfo userInfo;
    private DialogShowStyle dialogShowStyle;
    private String loginTag = "";
    private Context mContext;

    private int changePwd;//如果从修改密码界面跳转而来

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=LoginActivity.this;
        setContentView(R.layout.login_activity);
        changePwd=getIntent().getIntExtra("changePwd",0);

        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            loginTag = bundle.getString("LoginTag");
        }
        back = (RelativeLayout) findViewById(R.id.login_activity_back);
        userName = (EditText) findViewById(R.id.login_activity_username);
        passWord = (EditText) findViewById(R.id.login_activity_password);
//        userName.setText("Ddddd");  //此俩行后面需要取消
//        passWord.setText("123456");

        login = (Button) findViewById(R.id.login_activity_login);
        registered = (TextView) findViewById(R.id.login_activity_registered);
        forgetPassword = (TextView) findViewById(R.id.login_activity_forget_password);

        back.setOnClickListener(this);
        login.setOnClickListener(this);
        registered.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
        passWord.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_NULL:
                        break;
                    case EditorInfo.IME_ACTION_SEND:
                        break;
                    case EditorInfo.IME_ACTION_DONE:
                        break;
                    case EditorInfo.IME_ACTION_GO:
                        gotoLogin();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_activity_back:
                if (changePwd==CHANGE_PWD)
                    MainActivity.old_pos=0;
                MyApplication.isLoginBack=true;
                finish();
                break;
            case R.id.login_activity_login:
                gotoLogin();
                break;
            case R.id.login_activity_registered:
                startActivity(new Intent(LoginActivity.this, RegisteredActivity.class));
                break;
            case R.id.login_activity_forget_password:
                break;
        }
    }

    private void gotoLogin() {
        dialogShowStyle = new DialogShowStyle(LoginActivity.this, getResources().getString(R.string.logining));
        dialogShowStyle.dialogShow();
        if (userName.getText().toString().equals("")) {
            dialogShowStyle.dialogDismiss();
            Toast.makeText(LoginActivity.this, this.getString(R.string.noemail), Toast.LENGTH_SHORT).show();
        } else if (passWord.getText().toString().equals("")) {
            dialogShowStyle.dialogDismiss();
            Toast.makeText(LoginActivity.this, this.getString(R.string.registered_pwd_toast), Toast.LENGTH_SHORT).show();
        } else {
            userLogin();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK){
            if (changePwd==CHANGE_PWD)
                MainActivity.old_pos=0;
            MyApplication.isLoginBack=true;
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void userLogin() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/userinfoservice_getLogin.action?userName=" + userName.getText().toString()
                            + "&password=" + passWord.getText().toString();
                    //SyncHttp http = new SyncHttp();
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    userInfo = JsonUtils.getLoginData(jsonData);
                    if (null != userInfo && userInfo.isSuccess.equals("true")) {
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    UserInfoUtil.rememberUserInfo(LoginActivity.this, userInfo);
                    LoginOpenfireUtil.loginOpenfire(mContext);
//                    if (loginTag.equals("")) {
//                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
//                    }
                    finish();
                    break;
                case MyApplication.FAIL:
                    Toast.makeText(LoginActivity.this, userInfo.message, Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}