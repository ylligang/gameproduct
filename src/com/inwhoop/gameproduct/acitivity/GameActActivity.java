package com.inwhoop.gameproduct.acitivity;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.GameActlistAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.Actvitieslist;
import com.inwhoop.gameproduct.utils.JsonUtils;
import com.inwhoop.gameproduct.view.XListView;
import com.inwhoop.gameproduct.view.XListView.IXListViewListener;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 
 * 游戏活动
 * 
 * @Project: MainActivity
 * @Title: GameActActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-4-26 上午9:49:38
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GameActActivity extends BaseActivity implements IXListViewListener {

	private XListView listView = null;
	
	private List<Actvitieslist> list = null;
	
	private GameActlistAdapter adapter = null;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.game_act_list_layout);
		mContext = GameActActivity.this;
		init();
	}

	@Override
	public void init() {
		super.init();
		setTitle(R.string.gameact_tv);
		setHeadLeftButton(R.drawable.back);
		listView = (XListView) findViewById(R.id.actlistview);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(true);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Intent intent = new Intent(mContext,
						ActInfoActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("info",
						adapter.getAll().get(position-1));
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		read();
	}

	private void read() {
		showProgressDialog("正在加载...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getActlist(2, -1);
					msg.what = MyApplication.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = MyApplication.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			listView.stopRefresh();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;

			case MyApplication.READ_SUCCESS:
				if(list.size()<10){
					listView.setPullLoadEnable(false);
				}
				adapter = new GameActlistAdapter(mContext, list);
				listView.setAdapter(adapter);
				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onRefresh() {
		read();
	}

	@Override
	public void onLoadMore() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Message msg = new Message();
				try{
					list = JsonUtils.getActlist(2, adapter.getAll().get(adapter.getAll().size()-1).actId);
					msg.what = MyApplication.READ_SUCCESS;
				}catch(Exception e){
					msg.what = MyApplication.READ_FAIL;
				}
				addhandler.sendMessage(msg);
			}
		}).start();
	}
	
	private Handler addhandler = new Handler(){
		public void handleMessage(Message msg) {
			listView.stopLoadMore();
			switch (msg.what) {
			case MyApplication.READ_FAIL:

				break;

			case MyApplication.READ_SUCCESS:
				if(list.size()<10){
					listView.setPullLoadEnable(false);
				}
				adapter.add(list);
				adapter.notifyDataSetChanged();
				break;

			default:
				break;
			}
		};
	};
	
	private List<Actvitieslist> getList(){
		List<Actvitieslist> list = new ArrayList<Actvitieslist>();
		for (int i = 0; i < 10; i++) {
			Actvitieslist info = new Actvitieslist();
			info.actId = i;
			info.actName = "";
			info.gameName = "剑灵 ";
			info.introduce = "剑灵4月28日成都玩家见面会！前往查看吧！";
			info.imgUrl = "http://115.28.139.158/File/news_slide_635299945936155201.png";
			list.add(info);
		}
		return list;
	}

}
