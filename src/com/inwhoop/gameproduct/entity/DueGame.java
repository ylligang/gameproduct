package com.inwhoop.gameproduct.entity;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 * @date 2014/5/8   19:04
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class DueGame {
    public String gameId="";
    public String gameName="";
    public String imgUrl="";
    public String imgUrlPosition="";
    public String schTxt="";
}
