package com.inwhoop.gameproduct.entity;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 * @date 2014/4/28 18:33
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class UserInfo {
    public String isSuccess = "";
    public String message = "";   //登录后等 返回 操作成功 的信息，没必要这个参数
    public String chatId = "";
    public String id = ""; // 注意此项是登录后返回的"id"字段,
    public String userphoto = "";
    public String userId = ""; // 登录后返回的userId字段
    public String userNick = "";// 用户昵称
    public String openfirepassword = "";

    public String remark = "";//个人说明
    public String sex = "-1";   //服务器返回-1保密/0女/1男
    public String birthday = "";
    public String useraddress = "";

    //把服务器数据 返回男女字符
    public String getSexString() {
        if (sex.equals("1")) {
            return "男";
        } else if (sex.equals("0")) {
            return "女";
        } else
            return "保密";
    }
    @Override
    public String toString() {
        return "UserInfo{" +
                "isSuccess='" + isSuccess + '\'' +
                ", message='" + message + '\'' +
                ", chatId='" + chatId + '\'' +
                ", id='" + id + '\'' +
                ", userphoto='" + userphoto + '\'' +
                ", userId='" + userId + '\'' +
                ", userNick='" + userNick + '\'' +
                ", openfirepassword='" + openfirepassword + '\'' +
                ", remark='" + remark + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", useraddress='" + useraddress + '\'' +
                '}';
    }
}
