package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 * @date 2014/4/19   17:27
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GiftBagInfo implements Serializable {
    public String gameId="";
    public int giftBagId=0;
    public String giftBagName="";
    public String giftBagDescription="";
    public String imgUrl="";
    public int giftNum=0;
    public int giftBagType=0;
    public int updateTime=0;
}
