package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**
 * @Describe: TODO  验证消息 实体类
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/08 16:24
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class AgreeMessage implements Serializable {

    public String userName = "";  //申请人  姓名
    public String logo = "";  //申请人  头像
    public String info = "";  //申请    详情
    public String sex = "";   //申请人  性别
    public String address= "";//申请人 地址
    public boolean isAgree=false;//是否已经同意
    public boolean isManage=false; //此消息是否被处理
    public int id=0;//申请人的id
    public String birthday="";
    public String personState="";//个人说明
}
