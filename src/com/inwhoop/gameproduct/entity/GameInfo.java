package com.inwhoop.gameproduct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: GameInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO 首页游戏信息
 * @date 2014-4-3 下午12:49:40
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GameInfo implements Serializable {

    //活动类型静态变量
//    public static final int TYPE_ACTIVITY=0;
//    public static final int TYPE_HOT=1;
//    public static final int TYPE_HOME_BANNER=2;
//    public static final int TYPE_INTRASTING=4;


    public String infoimageUrl="";///upload/gameinfo/info/
    public String gameremark="";  // 游戏标签
    public List<String> infoimages=new ArrayList<String>();// 图集的图片地址集合  infoimagePosition
//	private static final long serialVersionUID = 1L;
	public int typeid; //类型id
	public String gameMode; //游戏类型
	public int collectCount;  //多少人收藏
	public boolean isActive; //是否有活动
	public boolean isActcode; //是否有激活码
	public boolean isGift; //是否有礼包
	public String gameState; //是否在公测
	public int gameId; //游戏id
	public String gameName; //游戏名称
	public String logoTitle; //游戏说明
	public String imgUrl; //游戏缩略图
	public int gametype; //游戏是否属于活动、热门、首页banner、感兴趣4
	public String imgUrlPosition;
    public int gameStateCode;//游戏运营状态数字，对应gameState标识

    public String gameIcon=""; //游戏icon图片，目前是直接有全路径

    public boolean isState=true;//游戏详情接口返回的?是否可以搜藏，可以搜藏代表用户未收藏
}
