package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * @Project: MainActivity
 * @Title: SubscibeInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-4 下午3:32:54
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class SubscibeInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public int count;
	public GameInfo info;

}
