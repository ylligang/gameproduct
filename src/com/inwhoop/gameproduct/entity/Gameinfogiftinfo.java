package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * @Project: MainActivity
 * @Title: Gameinfogiftinfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-29 下午6:58:20
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Gameinfogiftinfo implements Serializable{

	public int gameId;
	public String giftBagDescription;
	public int giftBagId;
	public String giftBagName;
	public int giftNum;
	public String imgUrl;
	public String imgUrlPosition;
	
}
