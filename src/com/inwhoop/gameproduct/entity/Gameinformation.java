package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * @Project: MainActivity
 * @Title: Gameinformation.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-28 下午8:56:52
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class Gameinformation implements Serializable{
	
	public int newsid;
	public String newsimage;
	public String newsimagePosition;
	public String newsname;
	public String newsremark;

}
