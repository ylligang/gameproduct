package com.inwhoop.gameproduct.entity;

/**
 * 游戏类别实体
 * 
 * @author lg
 * 
 */
public class GameCategory {
	private int g_categoryId;
	private String g_categoryName;

	public int getG_categoryId() {
		return g_categoryId;
	}

	public void setG_categoryId(int g_categoryId) {
		this.g_categoryId = g_categoryId;
	}

	public String getG_categoryName() {
		return g_categoryName;
	}

	public void setG_categoryName(String g_categoryName) {
		this.g_categoryName = g_categoryName;
	}

}
