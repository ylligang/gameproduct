package com.inwhoop.gameproduct.entity;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 * @date 2014/4/30   10:20
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class DetailsGift {
    public String giftName="";
    public String html="";
    public String imgUrl="";
    public String imgUrlPosition="";
    public String spareCount="";
    public String isSuccess="";
    public String message="";
}
