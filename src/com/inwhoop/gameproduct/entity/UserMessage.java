package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**
 * @Describe: TODO  用户的消息 实体，目前不需要礼包、预定
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/03 16:07
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class UserMessage implements Serializable {
    public static final int GET_BAG = 0;    //类别：已领取礼包，【去掉】
    public static final int CHAT_ONE = 1;       //类别：单聊聊天消息
    public static final int CHAT_GROUP = 2;    //类别：群聊 圈子
    public static final int CHAT_GROUP_SOC = 3;    //类别：群聊 公会
    public static final int GIFT_BAG = 4;   //类别：已订阅游戏礼包资讯【去掉】
    public static final int IS_AGREE = 5;   //类别：是否同意申请

    public String logo_url = "";
    public String title = "";//标题：已领取礼包|王二狗|XX工会名|验证消息|“礼包的话不显示”
    public String info = "";//主要内容
    public String type = GET_BAG + "";//类别：1.已领取礼包；2，聊天信息，3圈子聊天信息，4验证消息，5，礼包发放
    public int number = 0;//消息数量 .“礼包的话不显示”
    public String time = "";//消息时间   .“礼包的话不显示”。消息只有一条则显示
    public String chatid = "";  //聊天id
}
