package com.inwhoop.gameproduct.entity;

import com.inwhoop.gameproduct.utils.LogUtil;

import java.io.Serializable;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 * @date 2014/5/29   19:38
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class FriendData implements Serializable {
    public String id = "";

    public String userName="";
    public String userId="";
    public String userPhoto = "";


    public String sex = "保密";   //返回的状态，1男0女-1保密
    public String userid = "";
    public String username = "";
    public String userphoto = "";
    public String letter = "";
    public String birthday = "";
    public String useraddress = "";      //地区
    public String remark = "";//个人说明
    public boolean isselect = false;
    public double range = 0;
    public int role = 0;   //1表示会长0表示普通成员

    //根据状态码返回性别字符串
    public String realSex() {
        String ss;
        int i = -1;
        try {
            i = Integer.parseInt(sex);
        } catch (Exception e) {
//            LogUtil.e("sex:" + sex + "---realSex:e:" + e.toString());
            return sex;
        }
        switch (i) {
            case 0:
                ss = "女";
                break;
            case 1:
                ss = "男";
                break;

            case -1:
            default:
                ss = "保密";
                break;
        }
        return ss;
    }
}
