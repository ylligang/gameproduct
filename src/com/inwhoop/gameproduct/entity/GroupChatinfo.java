package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * @Project: WZSchool
 * @Title: Chatinfo.java
 * @Package com.wz.model
 * @Description: TODO 聊天内容
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-1-17 下午1:22:25
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */

public class GroupChatinfo implements Serializable{

	private static final long serialVersionUID = 1L;
	public String content;  //消息文字内容
	public String userid;  //登陆用户的的id
	public String usernick; //用户昵称
	public String groupid;   //圈子或者工会ID
	public String groupname;  //圈子或者工会名
	public String userheadpath; //用户头像
	public String time;  // 发送时间
	public boolean isMymsg;  //是否是我的消息
	public int msgtype;  //消息类型  1表示文字信息 2表示图片信息 3表示音频信息
	public String audiopath;  //音频文件路径
	public int audiolength;  //音频长度
	public int isloop;  //1表示圈子2表示公会
	public String groupphoto;  //公会logo
	public String chatid;   //
	
}
