package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * 
 * 游戏图集
 * @Project: MainActivity
 * @Title: GameImagesInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-5-8 下午7:48:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class GameImagesInfo implements Serializable{
	
	public int id;
	public String imgUrl;
	public String imgUrlPosition;
	public String imgUrlTxt;

}
