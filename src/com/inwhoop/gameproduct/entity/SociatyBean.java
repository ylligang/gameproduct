package com.inwhoop.gameproduct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO   公会对象
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/06/13 15:43
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SociatyBean implements Serializable {
    public int id = 0;
    public String guildname = "";
    public String guildheadphoto = "";     //logo图  //上传时是头像转成base64
    public String guildremark = "";//简介
    public String guildment= "";//公告
    public int userid;//公会会长Id
    public String createtime = "";//公会创建时间
//    public int role = 0;        //？好像暂时没用到的样子
    public String username="";//公会会长名字
    public int personNum = 0;//公会成员数
    public List<GameInfo> gameinfos = new ArrayList<GameInfo>();//入驻游戏
    public String gameids = "";  //创建公会时传的游戏id字符串，以逗号隔开
}
