package com.inwhoop.gameproduct.entity;

import java.io.Serializable;

/**  
 * @Project: MainActivity
 * @Title: SubscibeGuideInfo.java
 * @Package com.inwhoop.gameproduct.entity
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-4 下午4:15:32
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class SubscibeGuideInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String guide;
	public boolean isSelect;

}
