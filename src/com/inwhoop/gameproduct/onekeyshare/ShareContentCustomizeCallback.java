package com.inwhoop.gameproduct.onekeyshare;

import cn.sharesdk.framework.Platform;

public interface ShareContentCustomizeCallback {

	public void onShare(Platform platform, Platform.ShareParams paramsToShare);

}
