package com.inwhoop.gameproduct.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.adapter.NewGiftGridAdapter;
import com.inwhoop.gameproduct.application.MyApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 新手礼包页面-dingwenlong
 */
public class GiftPlatformNewGiftFragment extends Fragment {

    private GridView gameGridView;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_gift_fragment, null);
        context=getActivity();
        gameGridView=(GridView)view.findViewById(R.id.new_gift_fragment);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        Map<String, String> map;
        for (int i = 0; i < 5; i++) {
            map = new HashMap<String, String>();
            map.put("id", "1");
            list.add(map);
        }
        NewGiftGridAdapter adapter = new NewGiftGridAdapter(context, list, MyApplication.widthPixels);
        gameGridView.setAdapter(adapter);
    }
}
