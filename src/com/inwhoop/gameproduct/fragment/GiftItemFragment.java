package com.inwhoop.gameproduct.fragment;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import cn.sharesdk.framework.ShareSDK;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.DueGameDetailsActivity;
import com.inwhoop.gameproduct.acitivity.GameNewsActivity;
import com.inwhoop.gameproduct.acitivity.GiftBagDetailsActivity;
import com.inwhoop.gameproduct.acitivity.MainActivity;
import com.inwhoop.gameproduct.adapter.DueGameListAdapter;
import com.inwhoop.gameproduct.adapter.GiftPlatformGameInfoListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.ActivtyCode;
import com.inwhoop.gameproduct.entity.DueGame;
import com.inwhoop.gameproduct.utils.*;
import com.inwhoop.gameproduct.onekeyshare.Share;
import com.inwhoop.gameproduct.view.XListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.fragment
 * @Description: TODO
 * @date 2014/4/16   19:49
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
@SuppressLint("ValidFragment")
public class GiftItemFragment extends BaseFragment implements XListView.IXListViewListener {

    private XListView giftList;

    private Boolean isShowGift = true;//是否各Item显示礼包布局

    private int fromType = 0;//从哪个界面跳转过来

    private Context context;
    private int dataType = 0;
    private int maxId = -1;
    private List<ActivtyCode> activityCode;
    GiftPlatformGameInfoListAdapter adapter;
    private List<DueGame> dueGame;
    DueGameListAdapter adapter1;
    private DialogShowStyle dialogShowStyle;

    public GiftItemFragment() {
        super();
    }

    public GiftItemFragment(boolean isShowGift, int fromType) {
        super();
        this.isShowGift = isShowGift;
        this.fromType = fromType;
    }

    public GiftItemFragment(boolean isShowGift, int fromType, int dataType) {
        super();
        this.isShowGift = isShowGift;
        this.fromType = fromType;
        this.dataType = dataType;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gift_fragment_item, null);
        context = getActivity();
        ShareSDK.initSDK(getActivity());
        initView(view);
        return view;
    }

    private void initView(View view) {
        giftList = (XListView) view.findViewById(R.id.gift_fragment_item_list);
        giftList.setXListViewListener(this);
        giftList.setPullLoadEnable(true);
        if (dataType == 1) {
            getActivationCode(1);
        }
        if (dataType == 2) {
            getActivationCode(2);
        }
        if (dataType == 3) {
            getActivationCode(3);
        }
        if (dataType == 4) {
            getDueGame();
        }
    }

    private void listDataShow(final List<ActivtyCode> list) {
        if (fromType == MyApplication.DUE_GAME) {
            adapter = new GiftPlatformGameInfoListAdapter(getActivity(), list, isShowGift, true);
        } else {
            adapter = new GiftPlatformGameInfoListAdapter(getActivity(), list, isShowGift);
        }
        giftList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        giftList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (fromType == MyApplication.GAME_INFO_SELECT_BUTTON_ONE) { //暂时是游戏资讯，至于静态变量放哪里
                    // MyToast.showShort(context,"跳到游戏资讯item");
//                    Bundle b=new Bundle();
//                    b.putSerializable("bean",list.get(position).); //怎么传map你看着办，
//                    Act.toAct(context,XXX.class,b);
                    startActivity(new Intent(getActivity(), GameNewsActivity.class));
                } else if (fromType == MyApplication.GAME_INFO_SELECT_BUTTON_TWO) {
                    // MyToast.showShort(context,"跳到活动礼包item");
                    startActivity(new Intent(getActivity(), GiftBagDetailsActivity.class));
                } else if (fromType == MyApplication.GIFT_BAG_PLATFORM) {
                    Bundle bundle = new Bundle();
                    bundle.putString("giftBagId", list.get(position - 1).giftBagId);
                    bundle.putInt("type", dataType);
                    Intent intent = new Intent(getActivity(), GiftBagDetailsActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public void onRefresh() {
        if (dataType != MyApplication.DUE_GAME) {
            maxId = -1;
            getActivationCode(dataType);
        } else {
            maxId = -1;
            getDueGame();
        }
    }

    @Override
    public void onLoadMore() {
        if (dataType != MyApplication.DUE_GAME) {
            maxId = Integer.valueOf(adapter.getAll().get(adapter.getAll().size() - 1).giftBagId);
            getReActivationCode(dataType);
        } else {
            maxId = Integer.valueOf(adapter.getAll().get(adapter.getAll().size() - 1).giftBagId);
            getReDueGame();
        }
    }

    private void getActivationCode(final int dataTy) {
        dialogShowStyle = new DialogShowStyle(getActivity(),
                getResources().getString(R.string.load_data));
        dialogShowStyle.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/gamegiftservice_getAllGamegiftList.action?giftBagType=" + dataTy + "&minId=" + maxId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    activityCode = JsonUtils.getActivityCode(jsonData);
                    if (null != activityCode && activityCode.size() != 0) {
                        if (activityCode.size() < 10) {
                            giftList.setPullLoadEnable(false);
                        }
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowStyle.dialogDismiss();
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    giftList.stopRefresh();
                    listDataShow(activityCode);
                    break;
                case MyApplication.FAIL:
                    giftList.stopRefresh();
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    giftList.stopRefresh();
                    Toast.makeText(getActivity(), getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void getReActivationCode(final int dataTy) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/gamegiftservice_getAllGamegiftList.action?giftBagType=" + dataTy + "&minId=" + maxId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    activityCode = JsonUtils.getActivityCode(jsonData);
                    if (null != activityCode && activityCode.size() != 0) {
                        if (activityCode.size() < 10) {
                            giftList.setPullLoadEnable(false);
                        }
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mmHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mmHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    giftList.stopLoadMore();
                    if (activityCode.size() < 10) {
                        // list.setPullLoadEnable(false);
                    }
                    adapter.add(activityCode);
                    adapter.notifyDataSetChanged();
                    break;
                case MyApplication.FAIL:
                    giftList.stopLoadMore();
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    giftList.stopLoadMore();
                    Toast.makeText(getActivity(), getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void getDueGame() {
        maxId = -1;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/usergameRelationshipservice_getGameBySchedule.action?minId=" + maxId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    dueGame = JsonUtils.getDueGameData(jsonData);
                    if (null != dueGame && dueGame.size() != 0) {
                        if (dueGame.size() < 10) {
                            giftList.setPullLoadEnable(false);
                        }
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                rmHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler rmHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    giftList.stopRefresh();
                    dueListDataShow(dueGame);
                    break;
                case MyApplication.FAIL:
                    giftList.stopRefresh();
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    giftList.stopRefresh();
                    Toast.makeText(getActivity(), getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void dueListDataShow(final List<DueGame> dueGame) {
        adapter1 = new DueGameListAdapter(getActivity(), dueGame, false, true);
        giftList.setAdapter(adapter1);
        adapter1.notifyDataSetChanged();
        giftList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getActivity(),dueGame.get(position - 1).gameId,Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("gameId", dueGame.get(position - 1).gameId);
                Intent intent = new Intent(getActivity(), DueGameDetailsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void getReDueGame() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    String str = "/usergameRelationshipservice_getGameBySchedule.action?minId=" + maxId;
                    String jsonData = SyncHttp.Get(MyApplication.HOST, str);
                    Log.v("json==发送服务器的数据为", MyApplication.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    dueGame = JsonUtils.getDueGameData(jsonData);
                    if (null != dueGame && dueGame.size() != 0) {
                        if (dueGame.size() < 10) {
                            giftList.setPullLoadEnable(false);
                        }
                        msg.what = MyApplication.SUCCESS;
                    } else {
                        msg.what = MyApplication.FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("数据异常" + e.toString());
                    msg.what = MyApplication.ERROR;
                }
                mrHandler.sendMessage(msg);
            }
        }).start();
    }


    public Handler mrHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MyApplication.SUCCESS:
                    giftList.stopLoadMore();
                    if (dueGame.size() < 10) {
                        // list.setPullLoadEnable(false);
                    }
                    adapter1.add(dueGame);
                    adapter1.notifyDataSetChanged();
                    break;
                case MyApplication.FAIL:
                    giftList.stopLoadMore();
                    Toast.makeText(getActivity(), getResources().getString(R.string.no_data), Toast.LENGTH_SHORT).show();
                    break;
                case MyApplication.ERROR:
                    giftList.stopLoadMore();
                    Toast.makeText(getActivity(), getResources().getString(R.string.getdata_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
