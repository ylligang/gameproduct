package com.inwhoop.gameproduct.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.inwhoop.gameproduct.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.gameproduct.acitivity.LoginActivity;
import com.inwhoop.gameproduct.entity.UserInfo;
import com.inwhoop.gameproduct.utils.UserInfoUtil;
import com.inwhoop.gameproduct.view.CustomProgressDialog;

/**
     * 
     * Fragment基类，子类必须实现init方法
     *
     * @author ylligang118@126.com
     * @version V1.0
     * @Project: GameProduct
     * @Title: BaseFragment.java
     * @Package com.inwhoop.gameproduct.fragment
     * @Description: TODO
     * @date 2014-4-3 下午1:52:52
     * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
     */
    public class BaseFragment extends Fragment {
        /**
         * 标题栏左侧按钮或者文字
         */
        private RelativeLayout head_left = null;
        private TextView head_left_bt = null;
        /**
         * 标题栏第一个按钮，相对靠左一点的按钮
         */
        private RelativeLayout head_right_first = null;
        private TextView head_right_bt_1 = null;
        /**
         * 标题栏最右边的按钮
         */
        private RelativeLayout head_right_second = null;
        private TextView head_right_bt_2 = null;
        /**
         * 标题
         */
        private TextView titleView = null;
        public CustomProgressDialog progressDialog;// 加载进度框
        public Context mContext = null;

        /**
         * 控件初始化
         *
         * @param view void
         * @Title: init
         * @Description: TODO
         */
        public void init(View view) {
            head_left = (RelativeLayout) view.findViewById(R.id.head_left);
            head_left_bt = (TextView) view.findViewById(R.id.head_left_button);
            head_right_first = (RelativeLayout) view
                    .findViewById(R.id.head_right_first);
            head_right_second = (RelativeLayout) view
                    .findViewById(R.id.head_right_second);
            head_right_bt_2 = (TextView) view.findViewById(R.id.head_right_bt_2);
            head_right_bt_1 = (TextView) view.findViewById(R.id.head_right_bt_1);
            titleView = (TextView) view.findViewById(R.id.head_title);
        }

        /**
         * @param imgId void
         * @Title: setHeadButton
         * 设置左边按钮显示图片
         * @Title: setHeadLeftButton
         * @Description: TODO
         */
        public void setHeadLeftButton(int imgId) {
            head_left.setVisibility(View.VISIBLE);
            head_left_bt.setBackgroundResource(imgId);
        }

        /**
         * @param textId void
         * @Title: setHeadText
         * 设置左边显示文字
         * @Title: setHeadLeftText
         * @Description: TODO
         */
        public void setHeadLeftText(int textId) {
            head_left.setVisibility(View.VISIBLE);
            head_left_bt.setText(textId);
        }

        /**
         * 设置标题
         *
         * @param textId void
         * @Title: setTitle
         * @Description: TODO
         */
        public void setTitle(int textId) {
            titleView.setText(textId);
        }

        /**
         * 设置右边第一个按钮
         *
         * @param imgId void
         * @Title: setRightFirstBt
         * @Description: TODO
         */
        public void setRightFirstBt(int imgId) {
            head_right_first.setVisibility(View.VISIBLE);
            head_right_bt_1.setBackgroundResource(imgId);
        }

        /**
         * 设置右边第二个按钮
         *
         * @param imgId void
         * @Title: setRightSecondBt
         * @Description: TODO
         */
        public void setRightSecondBt(int imgId) {
            head_right_second.setVisibility(View.VISIBLE);
            head_right_bt_2.setBackgroundResource(imgId);
        }

        /**
         * 获取左边按钮
         *
         * @return RelativeLayout
         * @Title: getLeftBt
         * @Description: TODO
         */
        public RelativeLayout getLeftBt() {
            return head_left;
        }

        /**
         * 获取右边第一个按钮
         *
         * @return RelativeLayout
         * @Title: getRightFirstBt
         * @Description: TODO
         */
        public RelativeLayout getRightFirstBt() {
            return head_right_first;
        }

        /**
         * 获取右边第二个按钮
         *
         * @return RelativeLayout
         * @Title: getRightSecondBt
         * @Description: TODO
         */
        public RelativeLayout getRightSecondBt() {
            return head_right_second;
        }
        
        /**
    	 * 显示进度框
    	 * 
    	 * @Title: showProgressDialog
    	 * @Description: TODO
    	 * @param @param msg
    	 * @return void
    	 */
    	protected void showProgressDialog(String msg) {
            if (progressDialog == null) {
                progressDialog = CustomProgressDialog.createDialog(mContext);
            }
            progressDialog.setMessage(msg);
            progressDialog.setCancelable(true);
            progressDialog.show();
    	}

    	/**
    	 * 关闭进度框
    	 * 
    	 * @Title: dismissProgressDialog
    	 * @Description: TODO
    	 * @param
    	 * @return void
    	 */
    	protected void dismissProgressDialog() {
    		if (null != progressDialog && progressDialog.isShowing()) {
    			progressDialog.dismiss();
    			progressDialog = null;
    		}
    	}

    	/**
    	 * 显示toast
    	 * 
    	 * @Title: showToast
    	 * @Description: TODO
    	 * @param @param msg
    	 * @return void
    	 */
    	protected void showToast(String msg) {
    		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    	}


    /**
     * TODO 弹出窗口跳转到登录界面
     * @par23am actName   用于标识哪个界面跳转而来，目前传的类名
     * @return  返回是否登录
     */
    protected boolean isGotoLoginDialog(final Context mContext) {
        UserInfo userInfo = UserInfoUtil.getUserInfo(mContext);
        if (!"".equals(userInfo.id)){
            return true;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("请先登录");
        builder.setTitle("提示");

        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Bundle bundle = new Bundle();
//                bundle.putString("LoginTag", actName);
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();

        return false;
    }
}