package com.inwhoop.gameproduct.fragment;

import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.*;
import com.inwhoop.gameproduct.adapter.MessageListAdapter;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.UserMessage;
import com.inwhoop.gameproduct.utils.Act;
import com.inwhoop.gameproduct.utils.LogUtil;
import com.inwhoop.gameproduct.utils.MessageListUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 消息界面 ,目前隐藏了listview * * * ****** Created by ZK ********
 * @Date: 2014/04/03 14:07
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MessageFragment extends BaseFragment implements
        View.OnClickListener {

    private ListView listView; // 显示信息列表

    private Context mContext;

    private MessageListAdapter adapter;// 显示消息adapter

    private List<UserMessage> userMsgsList = new ArrayList<UserMessage>();// 存放所有消息数据的List

    private boolean isResult = false;

    // 初始化组件，切换跳转回来后需要再加载
    // 此方法不能加载数据，会重复加载，应该在onActivityCreated加载数据
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LogUtil.i("onCreateView");
        View view = inflater.inflate(R.layout.fragment_message, null);
        mContext = getActivity();
        init(view);
        setTitle(R.string.fragment_message_title);

        View bagLayout = view.findViewById(R.id.my_bag_layout);
        bagLayout.setOnClickListener(this);

        // 已领取礼包item，点击后跳转,目前无用，隐藏了
        ImageView logo = (ImageView) bagLayout
                .findViewById(R.id.imageView_logo);
        logo.setBackgroundResource(R.drawable.home_setting_bg);
        TextView title = (TextView) bagLayout.findViewById(R.id.textView_title);
        title.setText("测试跳转到B程序，需要有安装B（IMgroupchat）");// R.string.get_bag_ok_text

        listView = (ListView) view.findViewById(R.id.listView);
        View addview = LayoutInflater.from(mContext).inflate(
                R.layout.item_message_listview, null);
        TextView agreeTitle = (TextView) addview
                .findViewById(R.id.textView_title);
        agreeTitle.setText(R.string.checkout_msg);
        listView.addHeaderView(addview);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MyApplication.messagelistishiden = true;
        initData();
        setMsg();
    }

    private void initData() {
        IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
        filter.addAction(MyApplication.ACTION_MESSAGE);
        getActivity().registerReceiver(mRecever, filter);
        userMsgsList.clear();
        adapter = new MessageListAdapter(mContext, userMsgsList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position <= 0) {
                    Act.toAct(mContext, IsAgreeMessageActivity.class);
                    return;
                }
                UserMessage bean = userMsgsList.get(position - 1);
                int type = Integer.parseInt(bean.type);
                Bundle b = new Bundle();
                switch (type) {
                    case UserMessage.GET_BAG: // 取消
                        Act.toAct(mContext, MyBagActivity.class);
                        break;
                    case UserMessage.CHAT_ONE:
                        b.putString("username", bean.chatid);
                        b.putString("usernick", bean.title);
                        b.putString("userphoto", bean.logo_url);
                        Act.toAct(mContext, FriendChatActivity.class, b);
                        isResult = true;
                        break;
                    case UserMessage.CHAT_GROUP:
                        b.putString("groupid", bean.chatid);
                        b.putString("groupname", bean.title);
                        b.putString("groupphoto", bean.logo_url);
                        Act.toAct(mContext, LoopGroupChatActivity.class, b);
                        isResult = true;
                        break;
                    case UserMessage.CHAT_GROUP_SOC:
                        b.putString("groupid", bean.chatid);
                        b.putString("groupname", bean.title);
                        b.putString("groupphoto", bean.logo_url);
                        Act.toAct(mContext, GroupChatActivity.class, b);
                        isResult = true;
                        break;
                    case UserMessage.GIFT_BAG:

                        break;
                }
            }
        });
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            MyApplication.messagelistishiden = false;
        } else {
            MyApplication.messagelistishiden = true;
            List<UserMessage> list = MessageListUtil.getInstance(mContext).getUserMsglist();
            userMsgsList.clear();
            for (int i = 0; i < list.size(); i++) {
                userMsgsList.add(list.get(i));
            }
            adapter.notifyDataSetChanged();
        }
    }

    // 获取消息列表
    private void setMsg() {
        List<UserMessage> list = MessageListUtil.getInstance(mContext).getUserMsglist();
        userMsgsList.clear();
        for (int i = 0; i < list.size(); i++) {
            userMsgsList.add(list.get(i));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isResult) {
            isResult = false;
            setMsg();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.my_bag_layout:
                // 测试跳转到B程序
                Intent intent = new Intent();
                ComponentName cn = new ComponentName("com.inwhoop.imgroupchat",
                        "com.inwhoop.imgroupchat.activity.MainActivity");
                intent.setComponent(cn);
                Uri uri = Uri
                        .parse("com.inwhoop.imgroupchat.activity.MainActivity"); // 此处应与B程序中Data中标签一致
                intent.setData(uri);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onDestroy() {
        LogUtil.i("onDestroy");
        super.onDestroy();
        if (userMsgsList.size() > 0) { // 跳转Fragment，没有执行此方法
            userMsgsList.clear();
            // userMsgsList = new ArrayList<UserMessage>();
        }
        try {
            if (null != mRecever) {
                LogUtil.i("========messagegetBody=unregister====");
                mContext.unregisterReceiver(mRecever);
            }
        } catch (Exception e) {
        }
    }

    public BroadcastReceiver mRecever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MyApplication.ACTION_MESSAGE)) {
                List<UserMessage> list = MessageListUtil.getInstance(mContext).getUserMsglist();
                userMsgsList.clear();
                for (int i = 0; i < list.size(); i++) {
                    userMsgsList.add(list.get(i));
                }
                adapter.notifyDataSetChanged();
            }
        }
    };

}
