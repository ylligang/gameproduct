package com.inwhoop.gameproduct.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.acitivity.SearchGameActivity;
import com.inwhoop.gameproduct.adapter.GiftViewPagerAdapters;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.view.CustomList;

import java.util.ArrayList;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: GameProduct
 * @Title: GiftBagPlatformFragment.java
 * @Description: TODO  礼包平台页面
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class GiftBagPlatformFragment extends BaseFragment implements View.OnClickListener {
    private RadioGroup gift_RadioGroup;
    private Context context;
    private RadioButton index;           //首页
    private RadioButton newGift;              //新手礼包
    private TextView activationCode;        //激活码
    private RadioButton reserveGame;           //预订游戏
    private int isCheckStatus = 0;
    private final int INDEX_IS_CHECK = 0;
    private final int NEW_GIFT_IS_CHECK = 1;
    private final int ACTIVATION_CODE_IS_CHECK = 2;
    private final int RESERVE_GAME_IS_CHECK = 3;
    private CustomList gameInfoList; //游戏信息列表

    private View activationCodeLine;
    private TextView activitiesGiftBag;
    private View activitiesGiftBagLine;
    private TextView newGiftBag;
    private View newGiftBagLine;
    private TextView dueGame;
    private View dueGameLine;
    private GiftItemFragment activationCodeFragment =
            new GiftItemFragment(true, MyApplication.GIFT_BAG_PLATFORM,1);
    private GiftItemFragment activitiesGiftBagFragment =
            new GiftItemFragment(true, MyApplication.GIFT_BAG_PLATFORM,2);
    private GiftItemFragment newGiftBagFragment =
            new GiftItemFragment(true, MyApplication.GIFT_BAG_PLATFORM,3);
    private GiftItemFragment dueGameFragment =
            new GiftItemFragment(false, MyApplication.DUE_GAME,4);
    private RelativeLayout activationCodeRela;
    private RelativeLayout activitiesGiftBagRela;
    private RelativeLayout newGiftBagRela;
    private RelativeLayout dueGameRela;
    private ViewPager viewPager;
    private ArrayList<Fragment> fragments;
    private FragmentManager fm;
    private TextView[] textView;
    private View[] view;
    private View layout;

    public GiftBagPlatformFragment(FragmentManager fm) {
        this.fm = fm;
    }
    
    public GiftBagPlatformFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.gift_bag_platform_fragment, null);
        context = getActivity();
        return layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView(layout);
        setViewPager();
        activationCodeRela.setOnClickListener(this);
        activitiesGiftBagRela.setOnClickListener(this);
        newGiftBagRela.setOnClickListener(this);
        dueGameRela.setOnClickListener(this);
    }

    private void initView(View view) {
        activationCode = (TextView) view.findViewById(R.id.gift_bag_platform_fragment_t1);
        activationCodeLine = view.findViewById(R.id.gift_bag_platform_fragment_line);
        activationCodeRela = (RelativeLayout) view.findViewById(R.id.gift_bag_platform_fragment_r1);

        activitiesGiftBag = (TextView) view.findViewById(R.id.gift_bag_platform_fragment_t2);
        activitiesGiftBagLine = view.findViewById(R.id.gift_bag_platform_fragment_line2);
        activitiesGiftBagRela = (RelativeLayout) view.findViewById(R.id.gift_bag_platform_fragment_r2);

        newGiftBag = (TextView) view.findViewById(R.id.gift_bag_platform_fragment_t3);
        newGiftBagLine = view.findViewById(R.id.gift_bag_platform_fragment_line3);
        newGiftBagRela = (RelativeLayout) view.findViewById(R.id.gift_bag_platform_fragment_r3);

        dueGame = (TextView) view.findViewById(R.id.gift_bag_platform_fragment_t4);
        dueGameLine = view.findViewById(R.id.gift_bag_platform_fragment_line4);
        dueGameRela = (RelativeLayout) view.findViewById(R.id.gift_bag_platform_fragment_r4);

        viewPager = (ViewPager) view.findViewById(R.id.gift_bag_platform_fragment_banner_viewpager);

        init(view);
        setRightSecondBt(R.drawable.search);
        setRightClick();
        setTitle(R.string.gift);
    }

    private void setRightClick() {
        getRightSecondBt().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SearchGameActivity.class));
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gift_bag_platform_fragment_r1:
                setColor(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.gift_bag_platform_fragment_r2:
                setColor(1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.gift_bag_platform_fragment_r3:
                setColor(2);
                viewPager.setCurrentItem(2);
                break;
            case R.id.gift_bag_platform_fragment_r4:
                setColor(3);
                viewPager.setCurrentItem(3);
                break;
        }
    }

    public void setViewPager() {
        textView = new TextView[]{activationCode, activitiesGiftBag, newGiftBag, dueGame};
        view = new View[]{activationCodeLine, activitiesGiftBagLine, newGiftBagLine, dueGameLine};
        Fragment[] frag = {activationCodeFragment, activitiesGiftBagFragment,
                newGiftBagFragment, dueGameFragment};
        fragments = new ArrayList<Fragment>();
        for (int i = 0; i < frag.length; i++) {
            fragments.add(frag[i]);
        }

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(new GiftViewPagerAdapters(fm, fragments));
        viewPager.setCurrentItem(1);
        setColor(1);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int position) {
                setColor(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void setColor(int position) {
        for (int i = 0; i < view.length; i++) {
            textView[i].setTextColor(getResources().getColor(R.color.text_color));
            view[i].setBackgroundColor(getResources().getColor(R.color.white));
        }
        textView[position].setTextColor(getResources().getColor(R.color.blue_33a6ff));
        view[position].setBackgroundColor(getResources().getColor(R.color.blue_33a6ff));
    }


}