package com.inwhoop.gameproduct.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @Project: WZSchool
 * @Title: GroupMemberDatabaseDBhelper.java
 * @Package com.wz.db
 * @Description: TODO 好友聊天记录列表
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午2:44:09
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class FriendChatuserlistDatabaseDBhelper extends SQLiteOpenHelper {

	private static final String name = "wenzhouchatlistdb"; // 数据库名称
	private static final int version = 1; // 数据库版本

	public FriendChatuserlistDatabaseDBhelper(Context context) {
		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS chattagetlist (id integer primary"
				+ " key autoincrement,userid varchar(20),type" +
				" integer,newtime varchar(20),myid integer,tagethead varchar(50),tagetnick varchar(20))");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS chattagetlist");
		onCreate(db);
	}

}
