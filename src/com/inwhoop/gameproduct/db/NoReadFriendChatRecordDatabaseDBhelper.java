package com.inwhoop.gameproduct.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @Project: WZSchool
 * @Title: GroupMemberDatabaseDBhelper.java
 * @Package com.wz.db
 * @Description: TODO 好友聊天记录数据库
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午2:44:09
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class NoReadFriendChatRecordDatabaseDBhelper extends SQLiteOpenHelper {

	private static final String name = "gameproductnoreadcrdb"; // 数据库名称
	private static final int version = 1; // 数据库版本

	public NoReadFriendChatRecordDatabaseDBhelper(Context context) {
		super(context, name, null, version);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS noreadfriendchatrec(id integer " +
				"primary key autoincrement,userid integer,username varchar(20),usernick varchar(20)," +
				"userheadpath varchar(80),chatcontent varchar(200),stime integer,ismsg integer," +
				"msgtype integer,audiopath varchar(100),audiolength integer)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS noreadfriendchatrec"); 
        onCreate(db);
	}

}
