package com.inwhoop.gameproduct.db;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.gameproduct.entity.ChatTagetList;
import com.inwhoop.gameproduct.entity.FriendChatinfo;
import com.inwhoop.gameproduct.utils.TimeRender;
import com.inwhoop.gameproduct.utils.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * @Project: WZSchool
 * @Title: HandleGroupmemberDB.java
 * @Package com.wz.db
 * @Description: TODO 操作聊天记录列表
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午3:00:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HandleFriendchatlistRecordDB {
	private FriendChatuserlistDatabaseDBhelper dbhelper = null;

	private SQLiteDatabase readDB, writeDB;

	// "CREATE TABLE IF NOT EXISTS chattagetlist (id integer primary"
	// + " key autoincrement,userid varchar(20),usernick varchar(40),type" +
	// " integer,newtime varchar(20),myid integer)");
	private String saveSql = "insert into chattagetlist(userid,type,newtime,myid,tagethead,tagetnick) values(?,?,?,?,?,?)";

	public HandleFriendchatlistRecordDB(Context context) {
		dbhelper = new FriendChatuserlistDatabaseDBhelper(context);
	}

	/**
	 * 
	 * @Title: saveGmember
	 * @Description: TODO
	 * @param @param chatinfo
	 * @return void
	 */
	public void saveGmember(String userid, int type, String newtime, String myid,String photo,String nick) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL(saveSql, new String[] { userid, "" + type,
					"" + Utils.getLongTime(newtime), myid,photo,nick});
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}

		}

	}

	/**
	 * 
	 * @Title: deleteGmember
	 * @Description: TODO
	 * @param @param groupid
	 * @return void
	 */
	public void deleteGmember(int userid) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL("delete from chattagetlist where userid=" + userid);
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}
		}
	}

	/**
	 * 修改最新时间
	 * 
	 * @Title: UpdateChatagetTime
	 * @Description: TODO
	 * @param @param userid
	 * @param @param type
	 * @param @param newtime
	 * @return void
	 */
	public void UpdateChatagetTime(String userid, int type, String myid,
			String newtime ,String head,String nick) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL("update chattagetlist set newtime=? where userid=?" +
					" and type=? and myid=? and tagethead=? and tagetnick=?",
					new String[]{""+Utils.getLongTime(newtime),userid,""+type,myid,head,nick});
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writeDB) {
				writeDB.endTransaction();
				writeDB.close();
			}
		}
	}

	/**
	 * 
	 * 按时间的倒序查询所有的聊天对象
	 * 
	 * @Title: getAlltagetlist
	 * @Description: TODO
	 * @param @param myid
	 * @param @return
	 * @return List<FriendChatinfo>
	 */
	public List<ChatTagetList> getAlltagetlist(String myid) {
		List<ChatTagetList> list = new ArrayList<ChatTagetList>();
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			Cursor cursor = readDB
					.rawQuery(
							"select * from chattagetlist where myid=? order by newtime desc",
							new String[] { myid });
			if (cursor != null && cursor.getCount() > 0) {
				ChatTagetList info = null;
				while (cursor.moveToNext()) {
					info = new ChatTagetList();
					info.tagetid = cursor.getString(cursor
							.getColumnIndex("userid"));
					info.myid = cursor.getString(cursor
							.getColumnIndex("myid"));
					info.newtime = TimeRender.getStandardDateBylongtime(Long
							.parseLong(cursor.getString(cursor
									.getColumnIndex("newtime"))));
					info.msgtype = cursor.getInt(cursor
							.getColumnIndex("type"));
					info.tagethead = cursor.getString(cursor
							.getColumnIndex("tagethead"));
					info.tagetnick = cursor.getString(cursor
							.getColumnIndex("tagetnick"));
					list.add(info);
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return list;
	}

	/**
	 * 
	 * @Title: isExist
	 * @Description: TODO 判断用户是否存在
	 * @param @param userid
	 * @param @return
	 * @return boolean
	 */
	public boolean isExist(String userid, int type, String newtime, String myid) {
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB
					.rawQuery(
							"select * from chattagetlist where userid=? and myid=? and type=?",
							new String[] { userid, myid, "" + type });
			String isuserid = "";
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					isuserid = cursor.getString(cursor
							.getColumnIndex("userid"));
				}
			}
			if (!"".equals(isuserid)) {
				return true;
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return false;
	}

}
