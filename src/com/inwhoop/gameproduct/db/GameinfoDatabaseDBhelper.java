package com.inwhoop.gameproduct.db;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class GameinfoDatabaseDBhelper extends SQLiteOpenHelper {

	public static final String DB_PATH = "/data/data/com.inwhoop.gameproduct/databases/";
	public static final String DB_NAME = "gameproductdb";
	public static final int DB_VERSION = 1;

	public static final String DB_TYPE_PRIMARY_KEY = " PRIMARY KEY";
	public static final String DB_TYPE_INTEGER = "integer";
	public static final String DB_TYPE_TEXT = "text";

	private SQLiteDatabase myDataBase;
	private final Context mContext;

	private static boolean mIsExsit = false;
	private static GameinfoDatabaseDBhelper mInstance = null;

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 */
	private GameinfoDatabaseDBhelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		this.mContext = context;

		try {
			if (!mIsExsit) {
				createDataBase();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public synchronized static GameinfoDatabaseDBhelper getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new GameinfoDatabaseDBhelper(context);
		}

		return mInstance;
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
        // db.execSQL("CREATE TABLE IF NOT EXISTS gameinfo(id integer primary key autoincrement,gameid integer,gamename varchar(20),gamexplain varchar(200),gamemainimage varchar(100),gameimgage varchar(100),gametype integer)");
        db.execSQL("CREATE TABLE IF NOT EXISTS giftBagTable(id integer primary key autoincrement," +
                "giftBagId integer,giftBagName varchar(20),giftBagDescription varchar(200)," +
                "imgUrl varchar(100),giftNum integer,giftBagType integer,updateTime integer)");
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// db.execSQL("DROP TABLE IF EXISTS gameinfo");
		// onCreate(db);
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {
		boolean dbExist = checkDataBase();
		// Log.e("DataBaseHelper", "checkDataBase = " + dbExist);

		if (!dbExist) {
			// By calling this method and empty database will be created into
			// the default system path

			// of your application so we are gonna be able to overwrite that
			// database with our database.

			SQLiteDatabase db_Read = this.getReadableDatabase();
			db_Read.close();
			try {
				copyDataBase();
				mIsExsit = true;
			} catch (IOException e) {
				e.printStackTrace();
				throw new Error("Error copying database");
			}
		}
	}


	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn’t
	 */
	private boolean checkDataBase() {
		SQLiteDatabase checkDB = null;
		try {
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY
							+ SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		} catch (SQLiteException e) {
			// database does’t exist yet.
			e.printStackTrace();
		} catch (Exception e) {
		}

		if (checkDB != null) {
			checkDB.close();
		}

		return checkDB != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {
		// Open your local db as the input stream
		InputStream myInput = mContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	public void openDataBase() throws SQLException {
		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READONLY
						+ SQLiteDatabase.NO_LOCALIZED_COLLATORS);
	}

	@Override
	public synchronized void close() {
		if (myDataBase != null)
			myDataBase.close();
		super.close();
	}

//	@Override
//	public void onCreate(SQLiteDatabase db) {
//	}
//
//	@Override
//	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//
//	}
}
