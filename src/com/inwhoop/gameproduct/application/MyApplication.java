package com.inwhoop.gameproduct.application;

import android.app.Application;
import android.content.Context;

/**
 * 公共静态类
 *
 * @Project: GameProduct
 * @Title: MyApplication.java
 * @Package com.inwhoop.gameproduct.application
 * @Description: TODO
 *
 * @author ylligang118@126.com
 * @date 2014-4-2 上午11:09:29
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MyApplication extends Application {

    public static final String SETTING_INFO = "SETTING_INFO";
    public static final String ID ="ID" ;
    public static final String USER_ID = "USER_ID";
    public static final String CHATID = "CHATID";
    public static final String HEADIMGURL ="HEADIMGURL";
    public static final String PASSWORD ="PASSWORD";
    public static final String USERNAME = "USERNAME";
    public static final String EXCHANGE = "EXCHANGE";
    public static final String USER_NICK = "USER_NICK";
    public static final String USER_REMARK = "USER_REMARK";
    public static final String USER_SEX = "USER_SEX";
    public static final String USER_BIRTHDAY = "USER_BIRTHDAY";
    public static final String USER_ADDRESS = "USER_ADDRESS";

    public static Context context;

    public final static String ACTION_MESSAGE = "com.inwhoop.gameproduct.addmessage";
    public final static String AGAIN_CONNECT = "com.inwhoop.gameproduct.againconnect";
    public final static String CHAT_AGAIN_INIT = "com.inwhoop.gameproduct.chatinit";

    public static boolean messagelistishiden = false;

    public final static int MSG_BODY = 88;  //消息

    public static final int SUCCESS = 1001;
    public static final int FAIL = 1002;
    public static final int ERROR = 1003;
    public static final int REFRESH_SUCCESS = 1004; //下拉刷新
    public static final int LOAD_SUCCESS = 1005;    //  上拉加载

    public static final int TYPE_INTEREST = 2001;//游戏信息感兴趣的type类型
    /** 屏幕分辨率宽度 */
	public static int widthPixels = 0;
	/** 屏幕分辨率高度 */
	public static int heightPixels = 0;

    /**是否是登录界面的左上角返回*/
    public static boolean isLoginBack=false;

    // 记录礼包平台中的listView的选中位置
    public static int position=0;
	/** 主机地址 */
	public static final String HOST= "http://203.195.199.216:8080";     //"http://192.168.0.132:8080";
//			  //http://192.168.0.103:8080
    /** 项目文件路径 */
    public static  String APP_PATH= "";
    /**读取网络成功*/
    public static final int READ_SUCCESS = 200;
    /**读取网络失败*/
    public static final int READ_FAIL= 500;
    /**应用包名*/
    public static  String packName="";

    /**游戏资讯标识*/
    public static final int GAME_INFO_SELECT_BUTTON_ONE = 101;
    /**活动礼包标识*/
    public static final int GAME_INFO_SELECT_BUTTON_TWO = 102;
    /**首页banner接口*/
    public static final String HOME_BANNER = "/gameinfoservice_getBanners.action";
    /**首页热门、活动接口*/
    public static final String HOME_HOTANDACT = "/gameinfoservice_getGameAndActive.action";
    /**全部热门游戏*/
    public static final String HOT_ALLGAME = "/gameinfoservice_getAllGameOrActive.action";
    /**首页的推荐游戏*/
    public static final String HOME_GAME_MODE = "/gameinfoservice_getGameByMode.action";
    /**图集*/
    public static final String GAMEIMAGES = "/gameinfoservice_getGameImageByGameid.action";
    /**活动详情*/
    public static final String ACTINFO = "/gameactiveservice_getActinfoById.action";
    /**游戏详情-游戏资讯*/
    public static final String GAMEINFORMATION = "/gamenewsservice_getGamenewsByGameid";
    /**游戏详情-活动礼包*/
    public static final String GAMEINFOGIFT= "/gamegiftservice_getGamegiftByGameid";

    /**活动礼包及新手礼包接口*/
    public static final String GIFT_BAY = "/gamegiftservice_getAllGamegiftList.action";
    /**11.游戏详情接口*/
    public static final String GAME_INFO_BY_REC = "/usergameRelationshipservice_getGameinfoByRec.action";

    public static final int GIFT_BAG_PLATFORM = 103; //礼包平台礼包标识
    public static final int DUE_GAME = 104;  //预定游戏
    /**14.游戏详情-收藏 -接口*/
    public static final String REC_GAME_INFO_BY_USER ="/usergameRelationshipservice_recGameinfoByUser.action" ;
    /**21个人收藏游戏的列表，显示该用户的所有收藏游戏的列表*/
    public static final String GET_GAME_BY_COLLECT = "/gameinfoservice_getGameByCollect.action";
    /**20 搜索*/
    public static final String GET_GAME_BY_SEARCH ="/gameinfoservice_getGameBySearch.action";
    /**1-1 模糊查询查找圈子*/
    public static final String GET_LOOP_LIST_BY_SEARCH = "/circleinfoService_getCircleListByTemp.action";
    /**1-1 创建圈子*/
    public static final String CREATE_LOOP = "/circleinfoService_uploadCircleinfo.action";
    /**1-1/1-2/1-3 查询圈子*/
    public static final String GET_CIRCLE_LIST_BY_TEMP = "/circleinfoService_getCircleListByTemp.action";
    /**2.加入圈子*/
    public static final String JOIN_CIRCLE = "/circleinfoService_joinCircle.action";
    /**4，查询所有好友*/
    public static final String GET_ALL_FRIEND_BY_USER = "/userinfoservice_getAllFriendByUser.action";
    /**4，根据传过来的id查询是否被邀请过了，返回的数据就是被邀请了*/
    public static final String GET_MAILLIST = "/circleinfoService_checkTelByUser.action";
    /**4，上传通讯录*/
    public static final String UPLOAD_MAIL = "/circleinfoService_addTelinfo.action";
    /**4，邀请好友*/
    public static final String INVATE_FRIENDS = "/circleinfoService_inviteFriendInCircle.action";
    /**4，邀请公会好友*/
    public static final String INVATE_TRADE_FRIENDS = "/guildinfoService_inviteFriendInGuild.action";
    /**2模糊查询好友接口：昵称/邮箱 */
    public static final String  GET_ALL_USER_BY_TEMP="/userinfoservice_getAllUserByTemp.action";
    /**8，请求添加好友*/
    public static final String ADD_FRIEND_BY_USER = "/userinfoservice_addFriendByUser.action";
    /**9  显示所有请求（加为好友）*/
    public static final String GET_REQUEST_BY_USER = "/userinfoservice_getRequestByUser.action";
    /**附近的人*/
    public static final String NEAR_PEOPLE_LIST = "/userinfoservice_getAllUserByTemp.action";
    /**当用户访问指定页面的时候 就给用户绑定新的坐标地址*/
    public static final String UPLOAD_LOCATION = "/userinfoservice_setXYByUser.action";
    /**用户自定义修改自己的被搜索的状态，1为可以被搜索到，0为搜索不*/
    public static final String UPDATE_LOCATION_STATE = "/userinfoservice_setIsLookByUser.action";
   /**10 处理请求（加为好友，拒绝加为好友）*/
   public static final String SOLVE_FRIEND = "/userinfoservice_solveFriend.action";
   /**创建公会*/
   public static final String CREATE_SOCIATY = "/guildinfoService_uploadGuildinfo.action";
   /**15,查询公会，条件查询*/
   public static final String GET_GUILD_LIST_BY_TEMP = "/guildinfoService_getGuildListByTemp.action";
   /**16，申请加入公会*/
   public static final String ASK_FOR_GUILD = "/guildinfoService_askForGuild.action";
   /**16，修改公会资料*/
   public static final String UPDATE_SOCIATY_INFO = "/guildinfoService_updateGuild.action";
   /**15-4 单个查询某个公会的信息(Long)*/
   public static final String GET_GUILD = "/guildinfoService_getGuild.action";
   /**19，获取某个公会的所有公会成员*/
   public static final String GET_USER_BY_GUILD = "/guildinfoService_getUserByGuild.action";
   /**19，解散或者退出公会*/
   public static final String EXIT_SOCIATY = "/guildinfoService_exitForGuild.action";
   /**20，创建公会活动*/
   public static final String CREATE_SOCIATY_ACT = "/guildactinfoService_uploadActinfo.action";
   /**21，查询公会活动*/
   public static final String SELECT_SOCIATY_ACT = "/guildactinfoService_getActListByTemp.action";
   /**22，获取公会申请消息列表*/
   public static final String SELECT_SOCIATY_APPLYLIST = "/guildinfoService_getAskForGuildByUser.action";
   /**23，处理公会申请消息接口*/
   public static final String HANDLE_SOCIATY_APPLYINFO = "/guildinfoService_solveAskForGuild.action";
   /**22 修改圈子*/
   public static final String UPDATE_CIRCLE = "/circleinfoService_updateCircle.action";
   /**1-5 单个查询某个圈子的信息*/
   public static final String GET_CIRCLE = "/circleinfoService_getCircle.action";
   /**11 完善用户资料，修改用户资料*/
   public static final String UPDATE_UPDATE_USER = "/userinfoservice_updateUser.action";
   /**11 获取群组信息*/
   public static final String GET_GROUPS = "/guildinfoService_getCidsGidsByUser.action";
   /**11 用户查询自己被邀请公会的所有记录*/
   public static final String GET_INVAITE_SOCIATY = "/guildinfoService_getAskGuild.action";
   /**用户处理公会邀请，接受或者拒绝*/
   public static final String HANDLE_INVATE_SOCIATY = "/guildinfoService_solveGuild.action";
   /**23 转让公会*/
   public static final String TRANSFER_GUILD = "/guildinfoService_transferGuild.action";
   /**退出圈子*/
   public static final String EXIT_CIRCLE = "/circleinfoService_exitCircle.action";
   /**精彩推荐每日推荐*/
   public static final String RECOMMEND_GAMELIST = "/gameinfoservice_getGameByRecomd.action";
   /**创建房间失败需要服务器刚创建的圈子回滚掉*/
   public static final String DELETE_CIRECLE = "/circleinfoService_delCircle.action";
   /**创建房间失败需要服务器刚创建的公会回滚掉*/
   public static final String DELETE_SOCIATY = "/guildinfoService_delGuild.action";
   /**创建用户失败需要服务器刚创建的用户回滚掉*/
   public static final String DELETE_USER = "/userinfoservice_delUser.action";
   /**修改密码*/
   public static final String CHANGE_PASS = "/userinfoservice_updatePwd";
}
