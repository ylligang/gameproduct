package com.inwhoop.gameproduct.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.gameproduct.acitivity.PersonInfoActivity;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * json解析工具
 * 
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: JsonUtils.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * @date 2014-4-8 下午2:33:47
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class JsonUtils {

	/**
	 * 获取首页banner信息
	 * 
	 * @param @return
	 * @return List<GameInfo>
	 * @throws Exception
	 * @Title: getHomeBannerList
	 * @Description: TODO
	 */
	public static List<GameInfo> getHomeBannerList() throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpPost(MyApplication.HOST
				+ MyApplication.HOME_BANNER, null);
		JSONObject obj = new JSONObject(jsonStr);
		JSONObject data = obj.getJSONObject("data");
		if (data.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = data.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 获取首页活动热门信息
	 * 
	 * @param @param hlist
	 * @param @param alist
	 * @param @return
	 * @param @throws Exception
	 * @return List<GameInfo>
	 * @Title: getHomeHotAndActList
	 * @Description: TODO
	 */
	public static void getHomeHotAndActList(List<GameInfo> hlist,
			List<Actvitieslist> alist) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpPost(MyApplication.HOST
				+ MyApplication.HOME_HOTANDACT, null);
		JSONObject obj = new JSONObject(jsonStr);
		JSONObject data1 = obj.getJSONObject("data1");
		JSONObject hotdata = data1.getJSONObject("data");
		JSONObject data2 = obj.getJSONObject("data2");
		JSONObject actdata = data2.getJSONObject("data");
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		if (hotdata.getBoolean("isSucess")) {
			JSONArray jsonArray = hotdata.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				hlist.add(info);
			}
		}
		if (actdata.getBoolean("isSucess")) {
			JSONArray jsonArray = actdata.getJSONArray("result");
			Actvitieslist info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Actvitieslist.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				alist.add(info);
			}
		}
	}

	/**
	 * 获取游戏详情-游戏资讯
	 * 
	 * @param @param gameId
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Gameinformation>
	 * @Title: getGameinformationlist
	 * @Description: TODO
	 */
	public static List<Gameinformation> getGameinformationlist(int gameId,
			int minid) throws Exception {
		List<Gameinformation> list = new ArrayList<Gameinformation>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GAMEINFORMATION, "gameId=" + gameId + "&minId="
				+ minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Gameinformation info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Gameinformation.class);
				if (info.newsimage.indexOf(",") != -1) {
					info.newsimage = MyApplication.HOST
							+ info.newsimagePosition
							+ info.newsimage.substring(0,
									info.newsimage.indexOf(","));
				} else {
					info.newsimage = MyApplication.HOST
							+ info.newsimagePosition + info.newsimage;
				}
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * @param @param gameId
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Gameinfogiftinfo>
	 * @Title: getGamegiftlist
	 * @Description: TODO 13.游戏详情-活动礼包
	 */
	public static List<Gameinfogiftinfo> getGamegiftlist(int gameId, int minid)
			throws Exception {
		List<Gameinfogiftinfo> list = new ArrayList<Gameinfogiftinfo>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GAMEINFOGIFT, "gameId=" + gameId + "&minId="
				+ minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Gameinfogiftinfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Gameinfogiftinfo.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 获取全部活动
	 * 
	 * @param @param typeid
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Actvitieslist>
	 * @Title: getActlist
	 * @Description: TODO
	 */
	public static List<Actvitieslist> getActlist(int typeid, int minid)
			throws Exception {
		List<Actvitieslist> list = new ArrayList<Actvitieslist>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.HOT_ALLGAME, "typeid=" + typeid + "&minid="
				+ minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Actvitieslist info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), Actvitieslist.class);
				if (info.imgUrl.indexOf(",") != -1) {
					info.imgUrl = MyApplication.HOST
							+ info.imgUrlPosition
							+ info.imgUrl
									.substring(0, info.imgUrl.indexOf(","));
				} else {
					info.imgUrl = MyApplication.HOST + info.imgUrlPosition
							+ info.imgUrl;
				}
				list.add(info);
			}
		}
		return list;
	}

	/*
	 * 用户登录
	 */
	public static UserInfo getLoginData(String jsonString) throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		UserInfo userInfo = new UserInfo();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		userInfo.isSuccess = jsonObject1.getString("isSucess");
		userInfo.message = jsonObject1.getString("message");
		String result = jsonObject1.getString("result");
		if ("true".equals(userInfo.isSuccess)) {
			// {"data":{"isSucess":true,"message":"执行成功","result":{"birthday":"2005-06-21","chatId":"123456","id":11,"remark":"专业打臊","sex":1,"us23erId":"123456","userNick":"123456","useraddress":"",
			// "userphoto":"http://192.168.0.121:8080//upload/userinfo/head/default.png"},"status":"200"}}
			JSONObject jsonObject2 = new JSONObject(result);
			userInfo.id = jsonObject2.getString("id");
			userInfo.userId = jsonObject2.getString("userId");
			userInfo.chatId = jsonObject2.getString("chatId");
			userInfo.userphoto = jsonObject2.getString("userphoto");
			userInfo.userNick = jsonObject2.getString("userNick");
			userInfo.birthday = jsonObject2.getString("birthday");
			userInfo.useraddress = jsonObject2.getString("useraddress");
			userInfo.remark = jsonObject2.getString("remark");
			userInfo.openfirepassword = jsonObject2.getString("openfirepassword");
			userInfo.sex = "" + jsonObject2.getInt("sex");

			// String userPhotoPosition = jsonObject2
			// .getString("userPhotoPosition");
			// if (userInfo.userphoto.indexOf(",") != -1) {
			// userInfo.userphoto = MyApplication.HOST
			// + userPhotoPosition
			// + userInfo.userphoto.substring(0,
			// userInfo.userphoto.indexOf(","));
			// } else {
			// userInfo.userphoto = MyApplication.HOST + userPhotoPosition
			// + userInfo.userphoto;
			// }
		}
		return userInfo;
	}

	/*
	 * 获取礼包平台列表数据
	 */
	public static List<ActivtyCode> getActivityCode(String jsonString)
			throws Exception {
		List<ActivtyCode> list = new ArrayList<ActivtyCode>();
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			ActivtyCode activtyCode = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				activtyCode = gson.fromJson(json.toString(), ActivtyCode.class);
				if (activtyCode.imgUrl.indexOf(",") != -1) {
					activtyCode.imgUrl = MyApplication.HOST
							+ activtyCode.imgUrlPosition
							+ activtyCode.imgUrl.substring(0,
									activtyCode.imgUrl.indexOf(","));
				} else {
					activtyCode.imgUrl = MyApplication.HOST
							+ activtyCode.imgUrlPosition + activtyCode.imgUrl;
				}
				list.add(activtyCode);
			}
		}
		return list;

	}

	/*
	 * 获取预定游戏列表数据
	 */
	public static List<DueGame> getDueGameData(String jsonString)
			throws Exception {
		List<DueGame> list = new ArrayList<DueGame>();
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			DueGame dueGame = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				dueGame = gson.fromJson(json.toString(), DueGame.class);
				if (dueGame.imgUrl.indexOf(",") != -1) {
					dueGame.imgUrl = MyApplication.HOST
							+ dueGame.imgUrlPosition
							+ dueGame.imgUrl.substring(0,
									dueGame.imgUrl.indexOf(","));
				} else {
					dueGame.imgUrl = MyApplication.HOST
							+ dueGame.imgUrlPosition + dueGame.imgUrl;
				}
				list.add(dueGame);
			}
		}
		return list;

	}

	/**
	 * TODO 11.获取游戏详情接口。注意因为改变不需要传userid，不传的话写0
	 * 
	 * @param gameinfo
	 * @throws Exception
	 */
	public static void getGameinfoByRec(String userId,GameInfo gameinfo) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpPost(MyApplication.HOST
				+ MyApplication.GAME_INFO_BY_REC + "?userId=" + userId + "&gameId="
				+ gameinfo.gameId, null);
		JSONObject obj = new JSONObject(jsonStr);
		LogUtil.i("getGameinfoByRec:" + obj);
		JSONObject data = obj.getJSONObject("data");

		if (data.getBoolean("isSucess")) {
			JSONObject result = data.getJSONObject("result");

			// GsonBuilder gsonBuilder = new GsonBuilder();
			// Gson gson = gsonBuilder.create();

			// gameinfo.imgUrl = result.getString("infoimagePosition");
			gameinfo.gameremark = result.getString("gameremark");
            gameinfo.isState = result.getBoolean("isState");

			String infoimage = result.getString("infoimage");
			// String s = data.getString("infoimage");
			String a[] = infoimage.split(",");
			for (int i = 0; i < a.length; i++) {
				gameinfo.infoimages.add(MyApplication.HOST
						+ "/upload/gameinfo/main/" + a[i]);
			}
		}
	}

	/*
	 * 领取礼包
	 */
	public static Messages getGiftExange(String jsonString) throws Exception {
		System.out.println("jsonString=  " + jsonString);
		JSONObject jsonObject = new JSONObject(jsonString);
		Messages messages = new Messages();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		messages.isSuccess = jsonObject1.getString("isSucess");
		messages.message = jsonObject1.getString("message");
		return messages;
	}

	/*
	 * 获取礼包详情页面数据
	 */
	public static DetailsGift getDetailsGiftData(String jsonString)
			throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		DetailsGift detailsGift = new DetailsGift();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		detailsGift.isSuccess = jsonObject1.getString("isSucess");
		detailsGift.message = jsonObject1.getString("message");
		String result = jsonObject1.getString("result");
		JSONObject jsonObject2 = new JSONObject(result);
		detailsGift.giftName = jsonObject2.getString("giftName");
		detailsGift.html = jsonObject2.getString("html");
		detailsGift.imgUrlPosition = jsonObject2.getString("imgUrlPosition");
		detailsGift.imgUrl = MyApplication.HOST + detailsGift.imgUrlPosition
				+ jsonObject2.getString("imgUrl");
		detailsGift.spareCount = jsonObject2.getString("spareCount");
		return detailsGift;
	}

	/*
	 * 获取礼包详情页面数据
	 */
	public static DetailsDue getDetailsDueData(String jsonString)
			throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		DetailsDue detailsDue = new DetailsDue();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		detailsDue.isSucess = jsonObject1.getString("isSucess");
		detailsDue.message = jsonObject1.getString("message");
		String result = jsonObject1.getString("result");
		JSONObject jsonObject2 = new JSONObject(result);
		detailsDue.gameId = jsonObject2.getString("gameId");
		detailsDue.schTxt = jsonObject2.getString("schTxt");
		detailsDue.imgUrlPosition = jsonObject2.getString("imgUrlPosition");
		detailsDue.imgUrl = MyApplication.HOST + detailsDue.imgUrlPosition
				+ jsonObject2.getString("imgUrl");
		return detailsDue;
	}

	/*
	 * 获取游戏资讯详情
	 */
	public static GameNews getGameNews(String jsonString) throws Exception {
        GameNews gameNews=new GameNews();
		JSONObject jsonObject = new JSONObject(jsonString);
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		String result = jsonObject1.getString("result");
		JSONObject jsonObject2 = new JSONObject(result);
		String newsremark = "";
		if (jsonString.contains("newsremark")) {
            gameNews.newsremark = jsonObject2.getString("newsremark");
		}
        gameNews.createtime=jsonObject2.getString("createtime");
		return gameNews;
	}

	/**
	 * 热门的全部游戏通过typeid获取游戏列表
	 * 
	 * @param @param typeid
	 * @param @param minid
	 * @param @return
	 * @return List<GameInfo>
	 * @throws Exception
	 * @Title: getGamelistBytypeid
	 * @Description: TODO 此接口不能传参数
	 */
	public static List<GameInfo> getGamelistBytypeid(int typeid, int minid,
			int plattypeid) throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.HOT_ALLGAME, "typeid=" + typeid + "&minid="
				+ minid + "&plattypeid=" + plattypeid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				info.imgUrl = MyApplication.HOST + info.imgUrl;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 获取推荐游戏
	 * @Title: getRecommendGamelistBytypeid 
	 * @Description: TODO
	 * @param @param typeid
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception     
	 * @return List<GameInfo>
	 */
	public static List<GameInfo> getRecommendGamelistBytypeid(int typeid,
			int minid) throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(
				MyApplication.HOST
				+ MyApplication.RECOMMEND_GAMELIST, "type=" + typeid
				+ "&minId=" + minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				info.imgUrl = MyApplication.HOST +info.imgUrlPosition+ info.imgUrl;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 首页获取分类推荐游戏
	 * 
	 * @param @param platId
	 * @param @return
	 * @param @throws Exception
	 * @return List<GameInfo>
	 * @Title: getHomegamelist
	 * @Description: TODO
	 */
	public static List<GameInfo> getHomegamelist(int platId) throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.HOME_GAME_MODE, "platId=" + platId);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				info.imgUrl = MyApplication.HOST + info.imgUrlPosition
						+ info.imgUrl;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 获取活动详情
	 * 
	 * @param @param actid
	 * @param @return
	 * @param @throws Exception
	 * @return ActInfo
	 * @Title: getActinfo
	 * @Description: TODO
	 */
	public static ActInfo getActinfo(int actid) throws Exception {
		SyncHttp http = new SyncHttp();
		ActInfo info = null;
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.ACTINFO, "gameId=" + actid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONObject json = obj.getJSONObject("result");
			info = gson.fromJson(json.toString(), ActInfo.class);
		}
		return info;
	}

	/*
	 * 用户注册
	 */
	public static UserInfo getRegistData(String jsonString) throws Exception {
		JSONObject jsonObject = new JSONObject(jsonString);
		UserInfo userInfo = new UserInfo();
		String data = jsonObject.getString("data");
		JSONObject jsonObject1 = new JSONObject(data);
		userInfo.isSuccess = jsonObject1.getString("isSucess");
		userInfo.message = jsonObject1.getString("message");
		String result = jsonObject1.getString("result");
		if ("true".equals(userInfo.isSuccess)) {
			JSONObject jsonObject2 = new JSONObject(result);
			userInfo.id = jsonObject2.getString("id");
			userInfo.userId = jsonObject2.getString("userId");
			userInfo.chatId = jsonObject2.getString("chatId");
			userInfo.userphoto = jsonObject2.getString("userphoto");
			userInfo.userNick = jsonObject2.getString("userNick");
            userInfo.birthday=jsonObject2.getString("birthday");
            userInfo.sex=""+jsonObject2.getInt("sex");
            userInfo.useraddress =jsonObject2.getString("useraddress");
            userInfo.remark=jsonObject2.getString("remark");

		} else {
			JSONObject jsonObject2 = new JSONObject(result);
			userInfo.message = jsonObject2.getString("msg");
		}
		return userInfo;
	}

	/**
	 * TODO 14.游戏详情-收藏 -接口
	 * 
	 * @param gameid
	 * @param userid
	 * @return 返回后台给的数据，如 【收藏失败，已收藏】
	 * @throws Exception
	 */
	public static String recGameinfoByUser(String gameid, String userid)
			throws Exception {
		String message = "";
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.REC_GAME_INFO_BY_USER, "userId=" + userid
				+ "&gameId=" + gameid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
		}
		message = obj.getString("message");

		return message;
	}

	/**
	 * TODO 21、个人收藏游戏的列表，显示该用户的所有收藏游戏的列表
	 * 
	 * @param minId
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public static List<GameInfo> getGameByCollect(String minId, String userId)
			throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GAME_BY_COLLECT, "minId=" + minId
				+ "&userId=" + userId);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				info.imgUrl = MyApplication.HOST + info.imgUrlPosition
						+ info.imgUrl;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * TODO 20.搜索
	 * 
	 * @param searchStr
	 * @param minId
	 * @return
	 * @throws Exception
	 */
	public static List<GameInfo> getGameBySearch(String searchStr, String minId)
			throws Exception {
		List<GameInfo> list = new ArrayList<GameInfo>();
		SyncHttp http = new SyncHttp();
		String getStr = URLEncoder.encode(searchStr, "UTF-8");// "minId=" +
		// minId+
		// "&gamename="+searchStr;
		// String getStr = new String(searchStr.getBytes(),"UTF-8");

		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GAME_BY_SEARCH, "minId=" + minId
				+ "&gamename=" + searchStr);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameInfo.class);
				info.imgUrl = MyApplication.HOST + info.imgUrlPosition
						+ info.imgUrl;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * 获取游戏图集
	 * 
	 * @param @param gameid
	 * @param @return
	 * @param @throws Exception
	 * @return List<GameImagesInfo>
	 * @Title: getGameImages
	 * @Description: TODO
	 */
	public static List<GameImagesInfo> getGameImages(int gameid)
			throws Exception {
		List<GameImagesInfo> list = new ArrayList<GameImagesInfo>();
		SyncHttp http = new SyncHttp();
		@SuppressWarnings("static-access")
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GAMEIMAGES, "gameId=" + gameid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			GameImagesInfo info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), GameImagesInfo.class);
				info.imgUrl = MyApplication.HOST + info.imgUrlPosition
						+ info.imgUrl;
				list.add(info);
			}
		}
		return list;
	}

	/**
	 * TODO 1-1 搜索圈子接口
	 * 
	 * @param searchStr
	 * @return
	 * @throws Exception
	 */
	public static List<LoopInfoBean> searchLoop(String minId, String searchStr)
			throws Exception {
		List<LoopInfoBean> lists = new ArrayList<LoopInfoBean>();
		SyncHttp http = new SyncHttp();
		String getStr = URLEncoder.encode(searchStr, "UTF-8");

		// String jsonStr = http.httpGet("http://192.168.0.119:8080"
		// + MyApplication.GET_CIRCLE_LIST_BY_TEMP
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_CIRCLE_LIST_BY_TEMP, "minId=" + minId
				+ "&circlename=" + searchStr);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			LoopInfoBean info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), LoopInfoBean.class);
				lists.add(info);
			}
		}
		return lists;
	}

	/**
	 * TODO 推荐的圈子接口
	 * 
	 * @param minId
	 * @throws Exception
	 */
	public static List<LoopInfoBean> recommendLoop(String minId)
			throws Exception {
		List<LoopInfoBean> lists = new ArrayList<LoopInfoBean>();
		SyncHttp http = new SyncHttp();

		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_CIRCLE_LIST_BY_TEMP, "minId=" + minId
				+ "&recommend=1");
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			LoopInfoBean info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), LoopInfoBean.class);
				lists.add(info);
			}
		}
		return lists;
	}

	/**
	 * TODO 附近的圈子接口
	 * 
	 * @param minId
	 * @return
	 * @throws Exception
	 */
	public static List<LoopInfoBean> nearLoop(String minId, String xlat,
			String ylong) throws Exception {
		List<LoopInfoBean> lists = new ArrayList<LoopInfoBean>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_CIRCLE_LIST_BY_TEMP, "minId=" + minId
				+ "&xlat=" + xlat + "&ylong" + ylong);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			LoopInfoBean info = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				info = gson.fromJson(json.toString(), LoopInfoBean.class);
				lists.add(info);
			}
		}
		return lists;
	}

	/*
	 * 获取我的礼包数据
	 */
	public static List<MyGift> getGiftData(String jsonStr) throws Exception {
		List<MyGift> list = new ArrayList<MyGift>();
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			MyGift myGift = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				myGift = gson.fromJson(json.toString(), MyGift.class);
				myGift.imgUrl = MyApplication.HOST + myGift.imgUrlPosition
						+ myGift.imgUrl;
				list.add(myGift);
			}
		}
		return list;
	}

	/*
	 * 获取好友数据
	 */
	public static List<FriendData> getFriendListData(String jsonString)
			throws Exception {
		List<FriendData> list = new ArrayList<FriendData>();
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			FriendData friendData = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				friendData = gson.fromJson(json.toString(), FriendData.class);
				list.add(friendData);
			}
		}
		return list;
	}

	/**
	 * 创建圈子
	 * 
	 * @param @param info
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: createLoop
	 * @Description: TODO
	 */
	public static CreateLoopInfo createLoop(CreateLoopInfo info)
			throws Exception {
		CreateLoopInfo cinfo = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(info);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("circleJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		// MyApplication.HOST+MyApplication.CREATE_LOOP
		String result = http.httpPost(MyApplication.HOST
				+ MyApplication.CREATE_LOOP, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			JSONObject json = obj.getJSONObject("result");
			cinfo = gson.fromJson(json.toString(), CreateLoopInfo.class);
		}
		return cinfo;
	}

	/**
	 * TODO 圈子详情接口
	 * 
	 * @param userId
	 *            用于判断这个圈子的用户级别，用于写是否可编辑圈子
	 * @param loopId
	 * @return
	 * @throws Exception
	 */
	public static LoopInfoBean getLoopInfo(String userId, int loopId)
			throws Exception {
		LoopInfoBean info = new LoopInfoBean();
		SyncHttp http = new SyncHttp();

		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_CIRCLE_LIST_BY_TEMP, "userId=" + userId
				+ "&loopId=" + loopId);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		JSONObject result = obj.getJSONObject("result");
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		info = gson.fromJson(result.toString(), LoopInfoBean.class);
		info.circleheadphoto = MyApplication.HOST + info.circleheadphoto;
		return info;
	}

	/**
	 * TODO 加入圈子
	 * 
	 * @param circleid
	 * @param userId
	 * @return message加入怎样的信息，加入成功
	 * @throws Exception
	 */
	public static String joinCircle(String circleid, String userId)
			throws Exception {
		String message = "";
		SyncHttp http = new SyncHttp();

		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.JOIN_CIRCLE, "circleid=" + circleid
				+ "&userid=" + userId);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		message = obj.getString("message");

		return message;
	}

	/**
	 * TODO 关键字搜索朋友
	 * 
	 * @param minId
	 * @param searchStr
	 * @return
	 * @throws Exception
	 */
	public static List<FriendData> getAllFriendByUser(String minId,
			String searchStr) throws Exception {
		List<FriendData> list = new ArrayList<FriendData>();
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_ALL_USER_BY_TEMP
		// String jsonString = http.httpGet("http://192.168.0.119:8080"+
		// MyApplication.GET_ALL_USER_BY_TEMP
				, "minId=" + minId + "&keyword=" + searchStr);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			FriendData friendData = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				friendData = gson.fromJson(json.toString(), FriendData.class);
				list.add(friendData);
			}
		}
		return list;
	}

	/*
	 * 获取用户所在圈子列表数据
	 */
	public static List<LoopInfoBean> geLoopListData(String jsonString)
			throws Exception {
		List<LoopInfoBean> list = new ArrayList<LoopInfoBean>();
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			LoopInfoBean loopListData = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				loopListData = gson.fromJson(json.toString(),
						LoopInfoBean.class);
				list.add(loopListData);
			}
		}
		return list;
	}

	/**
	 * TODO 查询通讯录
	 * 
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static List<MailListInfo> getMaillist(String userid, String teltype)
			throws Exception {
		List<MailListInfo> list = new ArrayList<MailListInfo>();
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_MAILLIST, "userid=" + userid + "&teltype="
				+ teltype);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			MailListInfo friendData = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				friendData = gson.fromJson(json.toString(), MailListInfo.class);
				list.add(friendData);
			}
		}
		return list;
	}

	/**
	 * 上传通讯录信息
	 * 
	 * @param @param userid
	 * @param @param tel
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: UploadMaillist
	 * @Description: TODO
	 */
	public static boolean UploadMaillist(String userid, String tel)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http
				.httpGet(MyApplication.HOST + MyApplication.UPLOAD_MAIL,
						"userid=" + userid + "&tel=" + tel);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * 邀请好友
	 * 
	 * @param @param userids
	 * @param @param circleid
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: invateFriends
	 * @Description: TODO
	 */
	public static boolean invateFriends(String userids, String circleid)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.INVATE_FRIENDS, "circleid=" + circleid
				+ "&userids=" + userids);
		System.out.println("邀请圈子111" + MyApplication.HOST
				+ MyApplication.INVATE_FRIENDS + "?circleid=" + circleid
				+ "&userids=" + userids);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

    /**
     * TODO 邀请朋友来xx公会
     * @param userids
     * @param guildid
     * @return
     * @throws Exception
     */
	public static boolean invateTradeFriends(String userids, String guildid)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.INVATE_TRADE_FRIENDS, "guildid=" + guildid
				+ "&userids=" + userids);
		System.out.println("邀请公会" + MyApplication.HOST
				+ MyApplication.INVATE_TRADE_FRIENDS + "?guildid=" + guildid
				+ "&userids=" + userids);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * TODO 添加好友接口
	 * 
	 * @param zid
	 *            自己的id
	 * @param bid
	 *            想添加的对方的id
	 * @param askremark
     * @return 第一个是信息，第二个是是否成功的布尔值 .其实可以用Message这个bean对象
	 * @throws Exception
	 */
	public static Object[] addFriendByUser(String zid, String bid, String askremark)
			throws Exception {
		String msg = "";
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.ADD_FRIEND_BY_USER,
                "zid=" + zid + "&bid=" + bid + "&askremark=" +
                        URLEncoder.encode(askremark, "UTF-8") );
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");

		msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess };
		return objects;
	}

	/**
	 * TODO 9.显示所有请求（加为好友）
	 * 
	 * @param bid
	 * @return
	 * @throws Exception
	 */
	public static List<ApplyInfo> getRequestByUser(String bid) throws Exception {
		List<ApplyInfo> list = new ArrayList<ApplyInfo>();
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_REQUEST_BY_USER, "bid=" + bid);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			ApplyInfo friendData;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				friendData = gson.fromJson(json.toString(), ApplyInfo.class);
				friendData.isF = 1;
				list.add(friendData);
			}
		}
		return list;
	}

	/**
	 * TODO 10 处理请求（加为好友，拒绝加为好友）
	 * 
	 * @param zid
	 * @param userId
	 * @param solveState
	 * @return 第一个是信息，第二个是是否成功的布尔值,
	 */
	public static Object[] solveFriend(int zid, String userId, int solveState)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.SOLVE_FRIEND, "zid=" + zid + "&bid=" + userId
				+ "&solveState=" + solveState);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");

		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess };

		return objects;
	}

	/**
	 * 当用户访问指定页面的时候 就给用户绑定新的坐标地址
	 * 
	 * @param @param userid
	 * @param @param xlat
	 * @param @param ylong
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: uploadLocation
	 * @Description: TODO
	 */
	public static boolean uploadLocation(String userid, double xlat,
			double ylong) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.UPLOAD_LOCATION, "xlat=" + xlat + "&ylong="
				+ ylong + "&userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * 获取周围附近的用户
	 * 
	 * @param @param userid
	 * @param @param xlat
	 * @param @param ylong
	 * @param @return
	 * @param @throws Exception
	 * @return List<FriendData>
	 * @Title: getNearPeopleList
	 * @Description: TODO
	 */
	public static List<FriendData> getNearPeopleList(String userid,
			double xlat, double ylong) throws Exception {
		List<FriendData> list = new ArrayList<FriendData>();
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.NEAR_PEOPLE_LIST, "xlat=" + xlat + "&ylong="
				+ ylong + "&userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			FriendData friendData = null;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				friendData = gson.fromJson(json.toString(), FriendData.class);
                if ("".equals(friendData.username))
                    friendData.username=friendData.userid;
				list.add(friendData);
			}
		}
		return list;
	}

	/**
	 * 用户自定义修改自己的被搜索的状态，1为可以被搜索到，0为搜索不到
	 * 
	 * @param @param userid
	 * @param @param islook
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: updateLocationState
	 * @Description: TODO
	 */
	public static boolean updateLocationState(String userid, int islook)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonString = http.httpGet(MyApplication.HOST
				+ MyApplication.UPDATE_LOCATION_STATE, "islook=" + islook
				+ "&userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			return true;
		}
		return false;
	}

	/**
	 * TODO 创建公会
	 * 
	 * @param upBean
	 * @return
	 * @throws Exception
	 */
	public static Object[] createSociaty(SociatyBean upBean) throws Exception {
		SociatyBean cinfo = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(upBean);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("guildJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		String result = http.httpPost(MyApplication.HOST
		// String result = http.httpPost("http://192.168.0.121:8080"
				+ MyApplication.CREATE_SOCIATY, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			JSONObject json = obj.getJSONObject("result");
			cinfo = gson.fromJson(json.toString(), SociatyBean.class);
		}
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess, cinfo };
		return objects;
	}

	/**
	 * 修改公会资料
	 * 
	 * @param @param objs
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 * @Title: updateSociaty
	 * @Description: TODO
	 */
	public static Object[] updateSociaty(Object objs) throws Exception {
		SociatyBean cinfo = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(objs);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("guildJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		String result = http.httpPost(MyApplication.HOST
				+ MyApplication.UPDATE_SOCIATY_INFO, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			JSONObject json = obj.getJSONObject("result");
			cinfo = gson.fromJson(json.toString(), SociatyBean.class);
		}
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess, cinfo };
		return objects;
	}

	/**
	 * @param @param objs
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 * @Title: updateSociaty
	 * @Description: TODO 22 修改圈子,目前无返回整个圈子信息
	 */
	public static Object[] updateLoop(Object objs) throws Exception {
		LoopInfoBean cinfo = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(objs);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("circleJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		String result = http.httpPost(MyApplication.HOST
				+ MyApplication.UPDATE_CIRCLE, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		// if (obj.getBoolean("isSucess")) {
		// JSONObject json = obj.getJSONObject("result");
		// cinfo = gson.fromJson(json.toString(), LoopInfoBean.class);
		// }
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess };
		return objects;
	}

	/**
	 * TODO 15-1模糊查询公会
	 * 
	 * @param minId
	 * @param searchStr
	 * @return
	 * @throws Exception
	 */
	public static List<SociatyBean> searchGuild(String minId, String searchStr)
			throws Exception {
		String jsonString = "minId=" + minId + "&guildname=" + searchStr;
		return getSociatyBeans(jsonString);
	}

	/**
	 * TODO 15.得到公会集合，可被重复调用
	 * 
	 * @param jsonString
	 * @return
	 * @throws JSONException
	 */
	private static List<SociatyBean> getSociatyBeans(String jsonString)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GUILD_LIST_BY_TEMP, jsonString);

		List<SociatyBean> list = new ArrayList<SociatyBean>();
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			SociatyBean bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				bean = gson.fromJson(json.toString(), SociatyBean.class);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * TODO 15-2 查询自己属于的公会（类型Long）
	 * 
	 * @param minId
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static List<SociatyBean> searchMyGuild(String minId, int userid)
			throws Exception {
		String jsonString = "minId=" + minId + "&userid=" + userid;
		return getSociatyBeans(jsonString);
	}

	/**
	 * TODO 15-3 推荐的公会（类型int）
	 * 
	 * @param minId
	 * @return
	 * @throws Exception
	 */
	public static List<SociatyBean> searchRecommendGuild(String minId)
			throws Exception {
		String jsonString = "minId=" + minId + "&recommend=" + 1;
		return getSociatyBeans(jsonString);
	}

	/**
	 * TODO 15-4 单个查询某个公会详情的信息(Long)
	 * 
	 * @param guildid
	 * @return
	 * @throws Exception
	 */
	public static SociatyBean searchThisGuild(String guildid) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GUILD, "guildid=" + guildid);

		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		SociatyBean bean = null;
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONObject json = obj.getJSONObject("result");
			bean = gson.fromJson(json.toString(), SociatyBean.class);
		}
		return bean;
	}

	/**
	 * TODO 15-5 (1-5)单个查询某个圈子的信息
	 * 
	 * @param circleid
	 * @return
	 * @throws Exception
	 */
	public static LoopInfoBean getCircle(String circleid) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_CIRCLE, "circleid=" + circleid);

		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		LoopInfoBean bean = null;
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONObject json = obj.getJSONObject("result");
			bean = gson.fromJson(json.toString(), LoopInfoBean.class);
		}
		return bean;
	}

	/**
	 * TODO 16.申请加入公会
	 * 
	 * @param userId
	 * @param guildid
	 * @param askremark
	 * @return
	 * @throws Exception
	 */
	public static String askForGuild(String userId, int guildid,
			String askremark) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.ASK_FOR_GUILD, "userid=" + userId + "&guildid="
				+ guildid + "&askremark=" +
                URLEncoder.encode(askremark, "UTF-8"));
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {

		}
		String msg = obj.getString("message");
		return msg;
	}
    /**
     * TODO 19，获取某个公会的所有公会成员
     *
     * @param guildid
     * @return
     * @throws Exception
     */
    public static List<FriendData> getUserByGuild(int guildid) throws Exception {
        List<FriendData> list = new ArrayList<FriendData>();
        SyncHttp http = new SyncHttp();
        String jsonStr = http.httpGet(MyApplication.HOST
                + MyApplication.GET_USER_BY_GUILD, "guildid=" + guildid);
		/*
		 * jsonStr=
		 * "{\"data\":{\"isSucess\":true,\"message\":\"执行成功\",\"result\":[{\"id\":11,\"role\":1,\"userId\":\"123456\",\"userName\":\"123456\",\"userPhoto\":\"http://192.168.0.121:8080//upload/userinfo/head/default.png\"},"
		 * +
		 * "{\"id\":12,\"role\":0,\"userId\":\"456\",\"userName\":\"456\",\"userPhoto\":\"http://192.168.0.121:8080//upload/userinfo/head/default.png\"}],\"status\":\"200\"}}"
		 * ;
		 */
        JSONObject jsonObject = new JSONObject(jsonStr);
        JSONObject obj = jsonObject.getJSONObject("data");
        if (obj.getBoolean("isSucess")) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            JSONArray jsonArray = obj.getJSONArray("result");
            FriendData bean;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                bean = gson.fromJson(json.toString(), FriendData.class);
                list.add(bean);
            }
        }
        return list;
    }

	/**
	 * TODO 19，获取某个公会的所有公会成员
	 * 
	 * @param guildid
	 * @return
	 * @throws Exception
	 */
	public static List<FriendData> getUserByGuildMem(int guildid,String tag) throws Exception {
		List<FriendData> list = new ArrayList<FriendData>();
		SyncHttp http = new SyncHttp();
        String str="";
        if (tag.equals("Loop")){
            str="/circleinfoService_getUserByCircle.action?circleid="+ guildid;
        }else if (tag.equals("Trade")){
            str="/guildinfoService_getUserByGuild.action?guildid="+ guildid;
        }
		String jsonStr = http.Get(MyApplication.HOST, str);
		/*
		 * jsonStr=
		 * "{\"data\":{\"isSucess\":true,\"message\":\"执行成功\",\"result\":[{\"id\":11,\"role\":1,\"userId\":\"123456\",\"userName\":\"123456\",\"userPhoto\":\"http://192.168.0.121:8080//upload/userinfo/head/default.png\"},"
		 * +
		 * "{\"id\":12,\"role\":0,\"userId\":\"456\",\"userName\":\"456\",\"userPhoto\":\"http://192.168.0.121:8080//upload/userinfo/head/default.png\"}],\"status\":\"200\"}}"
		 ;
		 */
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			FriendData bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				bean = gson.fromJson(json.toString(), FriendData.class);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * 退出或者解散公会
	 * 
	 * @param @param guildid 公会id
	 * @param @param userid 用户id
	 * @param @return
	 * @return boolean
	 * @throws Exception
	 * @Title: exitSociaty
	 * @Description: TODO
	 */
	public static Object[] exitSociaty(int guildid, int userid)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.EXIT_SOCIATY, "guildid=" + guildid + "&userid="
				+ userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		return new Object[] { msg, isSucess };
	}

	/**
	 * 创建公会活动
	 * 
	 * @param @param objs
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 * @Title: createSociaty
	 * @Description: TODO
	 */
	public static Object[] createSociatyAct(Object objs) throws Exception {
		SociatyActBean cinfo = null;
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(objs);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("actJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		String result = http.httpPost(MyApplication.HOST
				+ MyApplication.CREATE_SOCIATY_ACT, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			JSONObject json = obj.getJSONObject("result");
			cinfo = gson.fromJson(json.toString(), SociatyActBean.class);
		}
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess, cinfo };
		return objects;
	}

	/**
	 * 查询公会活动
	 * 
	 * @param @param guildid
	 * @param @param minid
	 * @param @return
	 * @param @throws Exception
	 * @return List<SociatyActBean>
	 * @Title: getSociatyActList
	 * @Description: TODO
	 */
	public static List<SociatyActBean> getSociatyActList(int guildid,
			String minid) throws Exception {
		List<SociatyActBean> list = new ArrayList<SociatyActBean>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.SELECT_SOCIATY_ACT, "guildid=" + guildid
				+ "&minId=" + minid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			SociatyActBean bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				bean = gson.fromJson(json.toString(), SociatyActBean.class);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * 获取公会申请列表
	 * 
	 * @param @param userid
	 * @param @return
	 * @param @throws Exception
	 * @return List<SociatyApplyInfo>
	 * @Title: getSociatyApplyInfoList
	 * @Description: TODO
	 */
	public static List<ApplyInfo> getSociatyApplyInfoList(String userid)
			throws Exception {
		List<ApplyInfo> list = new ArrayList<ApplyInfo>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.SELECT_SOCIATY_APPLYLIST, "userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			ApplyInfo bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONArray item = jsonArray.getJSONArray(i);
				for (int j = 0; j < item.length(); j++) {
					JSONObject json = item.getJSONObject(j);
					bean = gson.fromJson(json.toString(), ApplyInfo.class);
					bean.isF = 2;
					list.add(bean);
				}
			}
		}
		return list;
	}

	/**
	 * 用户查询自己被邀请公会的所有记录
	 * 
	 * @Title: getInvateList
	 * @Description: TODO
	 * @param @param userid
	 * @param @return
	 * @param @throws Exception
	 * @return List<ApplyInfo>
	 */
	public static List<ApplyInfo> getInvateList(String userid) throws Exception {
		List<ApplyInfo> list = new ArrayList<ApplyInfo>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_INVAITE_SOCIATY, "userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			ApplyInfo bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject item = jsonArray.getJSONObject(i);
				bean = gson.fromJson(item.toString(), ApplyInfo.class);
				bean.isF = 3;
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * 处理申请公会的消息
	 * 
	 * @param @param guildid 公会id
	 * @param @param userid 申请人id
	 * @param @param solveState 1为接受，2为拒绝
	 * @param @return
	 * @param @throws Exception
	 * @return boolean
	 * @Title: handleSociatyApplyInfo
	 * @Description: TODO
	 */
	public static Object[] handleSociatyApplyInfo(int guildid, String userid,
			int solveState) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.HANDLE_SOCIATY_APPLYINFO, "guildid=" + guildid
				+ "&userid=" + userid + "&solveState=" + solveState);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess };
		return objects;
	}

	/**
	 * 用户处理公会邀请，接受或者拒绝   TODO，和上面重复
	 * 
	 * @Title: handleInvateInfo
	 * @Description: TODO
	 * @param @param guildid
	 * @param @param userid
	 * @param @param solveState
	 * @param @return
	 * @param @throws Exception
	 * @return Object[]
	 */
	public static Object[] handleInvateInfo(int guildid, String userid,
			int solveState) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.HANDLE_INVATE_SOCIATY, "userid=" + userid
				+ "&guildid=" + guildid + "&solveState=" + solveState);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess };
		return objects;
	}

	/**
	 * TODO 退出圈子 ，暂缺
	 * 
	 * @param circleid
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public static Object exitLoop(int circleid, int userid) throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.EXIT_CIRCLE, "circleid=" + circleid
				+ "&userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		return new Object[] { msg, isSucess };
	}

	/**
	 * TODO 11 完善用户资料，修改用户资料
	 * 
	 * @param objs
	 * @return
	 * @throws Exception
	 */
	public static Object[] updateUser(Object objs) throws Exception {
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		String circleJson = gson.toJson(objs);
		List<Parameter> params = new ArrayList<Parameter>();
		Parameter par = new Parameter("userJson", circleJson);
		params.add(par);
		SyncHttp http = new SyncHttp();
		String result = http.httpPost(MyApplication.HOST
				+ MyApplication.UPDATE_UPDATE_USER, params);
		JSONObject jsonObject = new JSONObject(result);
		JSONObject obj = jsonObject.getJSONObject("data");
		UserInfo cinfo = null;
		if (obj.getBoolean("isSucess")) {
			JSONObject json = obj.getJSONObject("result");
			cinfo = gson.fromJson(json.toString(), UserInfo.class);
			if (null == cinfo.id || "".equals(cinfo.id))
				cinfo.id = ((PersonInfoActivity.UpdateUser) objs).id;
		}
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		Object[] objects = new Object[] { msg, isSucess, cinfo };
		return objects;
	}

	/**
	 * 获取用户群组信息
	 * 
	 * @param @param userid
	 * @param @return
	 * @param @throws Exception
	 * @return List<Groups>
	 * @Title: getGroups
	 * @Description: TODO
	 */
	public static List<Groups> getGroups(String userid) throws Exception {
		List<Groups> list = new ArrayList<Groups>();
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.GET_GROUPS, "userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		if (obj.getBoolean("isSucess")) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			JSONArray jsonArray = obj.getJSONArray("result");
			Groups bean;
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject json = jsonArray.getJSONObject(i);
				bean = gson.fromJson(json.toString(), Groups.class);
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * TODO 23 转让公会
	 * 
	 * @param guildid
	 *            公会ID
	 * @param userid
	 *            转让的人（新会长）的ID
	 * @return
	 * @throws Exception
	 */
	public static Object[] transferGuild(String guildid, String userid)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST
				+ MyApplication.TRANSFER_GUILD, "guildid=" + guildid
				+ "&userid=" + userid);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		return new Object[] { msg, isSucess };
	}
	
	/**
	 * 创建房间失败后需要进行回滚
	 * @Title: delteOpenfire 
	 * @Description: TODO
	 * @param @param path
	 * @param @param name
	 * @param @param id
	 * @param @return
	 * @param @throws Exception     
	 * @return Object[]
	 */
	public static Object[] delteOpenfire(String path,String name, String id)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(path, name+"=" + id);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		return new Object[] { msg, isSucess };
	}

    /**
     * TODO  10、搜索界面的，感兴趣游戏接口（相当于推荐的游戏）
     * @return
     * @throws Exception
     */
    public static List<GameInfo> getGameIntersting() throws Exception{
        List<GameInfo> list = new ArrayList<GameInfo>();
        SyncHttp http = new SyncHttp();
        String jsonStr = http.httpGet(
                MyApplication.HOST
                        + MyApplication.RECOMMEND_GAMELIST, "type=" + 2);
        JSONObject jsonObject = new JSONObject(jsonStr);
        JSONObject obj = jsonObject.getJSONObject("data");
        if (obj.getBoolean("isSucess")) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            JSONArray jsonArray = obj.getJSONArray("result");
            GameInfo info = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                info = gson.fromJson(json.toString(), GameInfo.class);
                info.imgUrl = MyApplication.HOST +info.imgUrlPosition+ info.imgUrl;
                list.add(info);
            }
        }
        return list;
    }
    
    /**
     * 修改密码
     * @Title: chagePass 
     * @Description: TODO
     * @param @param id
     * @param @param oldpass
     * @param @param newpass
     * @param @return
     * @param @throws Exception     
     * @return Object[]
     */
    public static Object[] chagePass(String id,String oldpass, String newpass)
			throws Exception {
		SyncHttp http = new SyncHttp();
		String jsonStr = http.httpGet(MyApplication.HOST+MyApplication.CHANGE_PASS, 
				"userid="+id+"&userpwd="+oldpass+"&userpwdnew="+newpass);
		JSONObject jsonObject = new JSONObject(jsonStr);
		JSONObject obj = jsonObject.getJSONObject("data");
		String msg = obj.getString("message");
		boolean isSucess = obj.getBoolean("isSucess");
		return new Object[] { msg, isSucess };
	}
    
}
