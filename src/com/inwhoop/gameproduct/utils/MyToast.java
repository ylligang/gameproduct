package com.inwhoop.gameproduct.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * @Describe: 自定义Toast，时间LENGTH_LONG
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/03/24 16:03
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MyToast {
    private static Toast toast;

    /**
     * 短时间显示Toast
     *
     * @param context
     * @param message
     */
    public static void showShort(Context context, CharSequence message) {
        if (null == toast) {
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
            // toast.setGravity(Gravity.CENTER, 0, 0);
        } else {
            toast.setText(message);
        }
        toast.show();
    }

    /**
     * 短时间显示Toast
     *
     * @param context
     * @param message
     */
    public static void showShort(Context context, int message) {
        showShort(context, context.getString(message));
    }

    /**
     * 长时间显示Toast
     *
     * @param context
     * @param message
     */
    public static void showLong(Context context, CharSequence message) {
        if (null == toast) {
            toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            // toast.setGravity(Gravity.CENTER, 0, 0);
        } else {
            toast.setText(message);
        }
        toast.show();
    }

    /**
     * 长时间显示Toast
     *
     * @param context
     * @param message
     */
    public static void showLong(Context context, int message) {
        showLong(context, context.getString(message));
    }

    /**
     * 自定义显示Toast时间
     *
     * @param context
     * @param message
     * @param duration 毫秒
     */
    public static void showForTime(Context context, CharSequence message, int duration) {
        if (null == toast) {
            toast = Toast.makeText(context, message, duration);
            // toast.setGravity(Gravity.CENTER, 0, 0);
        } else {
            toast.setText(message);
        }
        toast.show();
    }

    /**
     * 自定义显示Toast时间
     *
     * @param context
     * @param message
     * @param duration
     */
    public static void showForTime(Context context, int message, int duration) {
        showForTime(context, context.getString(message), duration);
    }

    /**
     * Hide the toast, if any.
     */
    public static void hideToast() {
        if (null != toast) toast.cancel();
    }

//    public static void showMyToast(Context context, String smsg) {
//        useMyToast(context, smsg);
//    }

    /**
     * @param context
     * @param resMsg
     */
//    public static void showMyToast(Context context, int resMsg) {
//        useMyToast(context, context.getString(resMsg));
//    }

    /**
     * 完全自定义Toast
     *
     * @param context
     * @param msg
     */
//    public static Toast useMyToast(Context context, CharSequence smsg) {
//
//
//        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
//        View layout = inflater.inflate(R.layout.toast_custom,
//                (ViewGroup) ((Activity) context).findViewById(R.id.llToast));
//        ImageView image = (ImageView) layout
//                .findViewById(R.id.tvImageToast);
//        image.setImageResource(R.drawable.ic_launcher);
//        TextView title = (TextView) layout.findViewById(R.id.tvTitleToast);
////        title.setText("Attention");
//        TextView text = (TextView) layout.findViewById(R.id.tvTextToast);
//        text.setText(smsg);
//        Toast toast = new Toast(context.getApplicationContext());
//        toast.setGravity(Gravity.RIGHT | Gravity.TOP, 12, 40);
//        toast.setDuration(Toast.LENGTH_LONG);
//        toast.setView(layout);
//        toast.show();
//        return toast;
//    }
}
