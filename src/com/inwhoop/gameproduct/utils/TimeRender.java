package com.inwhoop.gameproduct.utils;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Describe: TODO
 * * * *
 * ****** Created by ZK ********
 * @Date: 2014/04/28 18:34
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
@SuppressLint("SimpleDateFormat")
public class TimeRender {

    private static SimpleDateFormat formatBuilder;


    public static String getPointDate() {
        formatBuilder = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return formatBuilder.format(new Date());
    }

    /**
     *
     * @return 斜线格式
     */
    public static String getSlantTime() {
        formatBuilder = new SimpleDateFormat("yyyy/M/d H:mm:ss");
        return formatBuilder.format(new Date());
    }

    public static String getNoDayTime(){
        formatBuilder = new SimpleDateFormat("HH:mm:ss");
        return formatBuilder.format(new Date());
    }
    
    public static String getStandardDate() {
        formatBuilder = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatBuilder.format(new Date());
    }
    
    public static String getStandardDateBylongtime(long time) {
        formatBuilder = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return formatBuilder.format(time);
    }
    
}
