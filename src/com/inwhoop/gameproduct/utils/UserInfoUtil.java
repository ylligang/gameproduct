package com.inwhoop.gameproduct.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.entity.UserInfo;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project:
 * @Title:
 * @Package com.inwhoop.zhixin.util
 * @Description: TODO
 * @date 2014/4/11   17:50
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class UserInfoUtil {

    public static void rememberUserInfo(Context context,UserInfo userInfo) {
        SharedPreferences setting = context.getSharedPreferences(MyApplication.SETTING_INFO, 0);
        setting.edit().putString(MyApplication.USER_ID, userInfo.id)
                .putString(MyApplication.USERNAME,userInfo.userId)
                .putString(MyApplication.CHATID, userInfo.chatId)
                .putString(MyApplication.HEADIMGURL,userInfo.userphoto)
                .putString(MyApplication.USER_NICK,userInfo.userNick)
                .putString(MyApplication.USER_REMARK,userInfo.remark)
                .putString(MyApplication.USER_SEX,userInfo.sex)
                .putString(MyApplication.USER_BIRTHDAY,userInfo.birthday)
                .putString(MyApplication.USER_ADDRESS,userInfo.useraddress)
                .putString(MyApplication.PASSWORD, userInfo.openfirepassword)
                .commit();
    }

    public static UserInfo getUserInfo(Context context) {
        UserInfo userInfo = new UserInfo();
        SharedPreferences setting = context.getSharedPreferences(MyApplication.SETTING_INFO, 0);
        userInfo.id = setting.getString(MyApplication.USER_ID, "");
        userInfo.userId =setting.getString(MyApplication.USERNAME,"");
        userInfo.chatId = setting.getString(MyApplication.CHATID, "");
        userInfo.userphoto =setting.getString(MyApplication.HEADIMGURL,"");
        userInfo.userNick =setting.getString(MyApplication.USER_NICK,"");
        userInfo.remark =setting.getString(MyApplication.USER_REMARK,"");
        userInfo.sex =setting.getString(MyApplication.USER_SEX,"");
        userInfo.birthday =setting.getString(MyApplication.USER_BIRTHDAY,"");
        userInfo.useraddress =setting.getString(MyApplication.USER_ADDRESS,"");
        userInfo.openfirepassword =setting.getString(MyApplication.PASSWORD,"");

        return userInfo;
    }

    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(MyApplication.SETTING_INFO,
                Context.MODE_PRIVATE);
        sp.edit().clear().commit();
    }
}
