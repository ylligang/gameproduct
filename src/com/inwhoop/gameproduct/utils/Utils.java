package com.inwhoop.gameproduct.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.inwhoop.gameproduct.application.MyApplication;
import com.inwhoop.gameproduct.xmpp.ClienConServer;
import com.inwhoop.gameproduct.xmpp.ClientGroupServer;
import com.inwhoop.gameproduct.xmpp.MypacketListener;
import com.inwhoop.gameproduct.xmpp.XmppConnectionUtil;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.RenderPriority;

/**
 * 工具类
 * 
 * @author ylligang118@126.com
 * @version V1.0
 * @Project: GameProduct
 * @Title: Utils.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * @date 2014-4-8 下午3:16:21
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
@SuppressLint("SimpleDateFormat")
public class Utils {

	/**
	 * 保存数据到sp
	 * 
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * 
	 * @return void
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 */
	public static void savePreference(Context context, String key, String value) {
		PreferenceManager.getDefaultSharedPreferences(context).edit()
				.putString(key, value).commit();
	}

	/**
	 * 从sp获取数据
	 * 
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * 
	 * @return String 返回的值
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 */
	public static String getpreference(Context context, String key) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(key, "");
	}

	/**
	 * 保存int数据到sp
	 * 
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @param value 值
	 * 
	 * @return void
	 * 
	 * @Title: savePreference
	 * @Description: TODO
	 */
	public static void saveIntPreferenceData(Context context, String key,
			int value) {
		// PreferenceManager.getDefaultSharedPreferences(context).edit()
		// .putString(key, value).commit();
		SharedPreferences sp = context.getSharedPreferences("gamevalue",
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	/**
	 * 从sp获取数据
	 * 
	 * @param @param context 上下文对象
	 * @param @param key 键
	 * @param @return
	 * 
	 * @return String 返回的值
	 * 
	 * @Title: getpreference
	 * @Description: TODO
	 */
	public static int getIntpreferenceData(Context context, String key,
			int defaultValue) {
		SharedPreferences sp = context.getSharedPreferences("gamevalue",
				Context.MODE_PRIVATE);
		return sp.getInt(key, defaultValue);
	}

	/**
	 * 将中文转换成拼音
	 * 
	 * @param zhongwen
	 *            拼音 -汉字
	 * 
	 * @return
	 */
	public static String getPinYin(String zhongwen)
			throws BadHanyuPinyinOutputFormatCombination {

		String zhongWenPinYin = "";
		char[] chars = zhongwen.toCharArray();

		for (int i = 0; i < chars.length; i++) {
			String[] pinYin = PinyinHelper.toHanyuPinyinStringArray(chars[i],
					getDefaultOutputFormat());
			// 当转换不是中文字符时,返回null
			if (pinYin != null) {
				zhongWenPinYin += pinYin[0];
			} else {
				zhongWenPinYin += chars[i];
			}
		}

		return zhongWenPinYin + "-" + zhongwen;
	}

	/**
	 * 输出格式
	 * 
	 * @return
	 */
	public static HanyuPinyinOutputFormat getDefaultOutputFormat() {
		HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
		format.setCaseType(HanyuPinyinCaseType.LOWERCASE);// 小写
		format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);// 没有音调数字
		format.setVCharType(HanyuPinyinVCharType.WITH_U_AND_COLON);// u显示

		return format;
	}

	/**
	 * 判断email格式是否正确
	 * 
	 * @param @param email
	 * @param @return
	 * 
	 * @return boolean
	 * 
	 * @Title: isEmail
	 * @Description: TODO
	 */
	public static boolean isEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);

		return m.matches();
	}

	/**
	 * 验证手机号
	 * 
	 * @param @param mobiles
	 * @param @return
	 * 
	 * @return boolean
	 * 
	 * @throws
	 * @Title: isMobileNO
	 * @Description: TODO
	 */
	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	/**
	 * 用来判断服务是否运行.
	 * 
	 * @param mContext
	 * @param className
	 *            判断的服务名字
	 * 
	 * @return true 在运行 false 不在运行
	 */
	public static boolean isServiceRunning(Context mContext, String className) {
		boolean isRunning = false;
		ActivityManager activityManager = (ActivityManager) mContext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> serviceList = activityManager
				.getRunningServices(30);
		if (!(serviceList.size() > 0)) {
			return false;
		}
		for (int i = 0; i < serviceList.size(); i++) {
			if (serviceList.get(i).service.getClassName().equals(className) == true) {
				isRunning = true;
				break;
			}
		}
		return isRunning;
	}

	public static Bitmap compressImage(String picpath) {
		BitmapFactory.Options newOpts = new BitmapFactory.Options();
		// 开始读入图片，此时把options.inJustDecodeBounds 设回true了
		newOpts.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(picpath, newOpts);// 此时返回bm为空

		newOpts.inJustDecodeBounds = false;
		int w = newOpts.outWidth;
		int h = newOpts.outHeight;
		// 现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
		float hh = 600f;// 这里设置高度为800f
		float ww = 360f;// 这里设置宽度为480f
		// 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
		int be = 1;// be=1表示不缩放
		if (w > h && w > ww) {// 如果宽度大的话根据宽度固定大小缩放
			be = (int) (newOpts.outWidth / ww);
		} else if (w < h && h > hh) {// 如果高度高的话根据宽度固定大小缩放
			be = (int) (newOpts.outHeight / hh);
		}
		if (be <= 0)
			be = 1;
		newOpts.inSampleSize = be;// 设置缩放比例
		// 重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
		bitmap = BitmapFactory.decodeFile(picpath, newOpts);
		return compressImage(bitmap);
	}

	public static Bitmap compressImage(Bitmap image) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 60, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int options = 60;
		while (baos.toByteArray().length / 1024 > 30) { // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
			baos.reset();// 重置baos即清空baos
			image.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
			options -= 10;// 每次都减少10
		}
		ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
		Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
		return bitmap;
	}

	/**
	 * bitmap转base64位
	 * 
	 * @param bitmap
	 * @return
	 */
	public static String imgToBase64(Bitmap bitmap) {
		ByteArrayOutputStream out = null;
		try {
			out = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.flush();
			out.close();
			byte[] imgBytes = out.toByteArray();
			return Base64.encodeToString(imgBytes, Base64.DEFAULT);
		} catch (Exception e) {
			return null;
		} finally {
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 设置WebView
	 * 
	 * @param @param context
	 * @param @param webView
	 * 
	 * @return void
	 * 
	 * @Title: setWebView
	 * @Description: TODO
	 */
	@SuppressLint("SetJavaScriptEnabled")
	public static void setWebView(final Context context, final WebView webView) {
		webView.setVisibility(View.VISIBLE);
		WebSettings webSettings = webView.getSettings();
		webSettings.setBlockNetworkImage(true);
		webSettings.setRenderPriority(RenderPriority.HIGH);
		webSettings.setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {

				view.getSettings().setBlockNetworkImage(false);

				super.onPageFinished(view, url);

			}
		});
	}

	/**
	 * 加载WebView
	 * 
	 * @param @param webView 加载内容的WebView
	 * @param @param textView 提示消息的textView
	 * @param @param url 需要加载的url
	 * 
	 * @return void
	 * 
	 * @Title: LoadWeb
	 * @Description: TODO
	 */
	public static void LoadcontentWeb(Context context, WebView webView,
			String content) {
		// setWebView(context, webView);
		try {
			webView.loadDataWithBaseURL(
					"fake://not/needed",
					"<html><head><meta http-equiv='content-type' content='text/html;charset=utf-8'><style type=\"text/css\">img{ width:95%}</style><STYLE TYPE=\"text/css\"> BODY { margin:0; padding: 5px 3px 5px 5px; background-color:#ffffff;} </STYLE><BODY TOPMARGIN=5 rightMargin=0 MARGINWIDTH=0 MARGINHEIGHT=0></head><body>"
							+ new String(content.getBytes("utf-8"))
							+ "</body></html>", "text/html", "utf-8", "");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置listview的高度最大 : TODO 设置ListView的Adapter后调用 ,
	 * 但是要注意的是，子ListView的每个Item必须是LinearLayout，不能是其他的，
	 * 因为其他的Layout(如RelativeLayout)没有重写onMeasure()，所以会在onMeasure()时抛出异常。
	 * 
	 * @param listView
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	/**
	 * TODO 真实路径转uri。。大概是本地
	 * 
	 * @param picPath
	 * 
	 * @return
	 */
	public static Uri pathToUri(Context context, String picPath) {
		Uri mUri = Uri.parse("content://media/external/images/media");
		Uri mImageUri = null;
		Cursor cursor = ((Activity) context).managedQuery(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null,
				MediaStore.Images.Media.DEFAULT_SORT_ORDER);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			String data = cursor.getString(cursor
					.getColumnIndex(MediaStore.MediaColumns.DATA));
			if (picPath.equals(data)) {
				int ringtoneID = cursor.getInt(cursor
						.getColumnIndex(MediaStore.MediaColumns._ID));
				mImageUri = Uri.withAppendedPath(mUri, "" + ringtoneID);
				return mImageUri;
			}
			cursor.moveToNext();
		}
		return mImageUri;
	}

	/**
	 * 将文件转成base64 字符串
	 * 
	 * @param path 文件路径
	 * @return *
	 * @throws Exception
	 */

	public static String encodeBase64File(String path) throws Exception {
		File file = new File(path);
		FileInputStream inputFile = new FileInputStream(file);
		byte[] buffer = new byte[(int) file.length()];
		inputFile.read(buffer);
		inputFile.close();
		return Base64.encodeToString(buffer, Base64.DEFAULT);
	}

	/**
	 * 将base64字符解码保存文件
	 * 
	 * @param base64Code
	 * @param targetPath
	 * @throws Exception
	 */

	public static void decoderBase64File(String base64Code, String targetPath)
			throws Exception {
		File file = new File(targetPath).getParentFile();
		if (!file.exists()) {
			file.mkdirs();
		}
		byte[] buffer = Base64.decode(base64Code, Base64.DEFAULT);
		FileOutputStream out = new FileOutputStream(targetPath);
		out.write(buffer);
		out.close();
	}

	/**
	 * TODO uri转真实路径
	 * 
	 * @param context
	 * @param uri
	 * @return
	 */
	public static String uriToPath(Context context, Uri uri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor actualimagecursor = ((Activity) context).managedQuery(uri, proj,
				null, null, null);
		int actual_image_column_index = actualimagecursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		actualimagecursor.moveToFirst();
		return actualimagecursor.getString(actual_image_column_index);
	}

	/**
	 * 创建一条图片地址uri,用于保存拍照后的照片
	 * 
	 * @param context
	 * @return 图片的uri
	 */
	public static Uri createImagePathUri(Context context) {
		Uri imageFilePath = null;
		String status = Environment.getExternalStorageState();
		SimpleDateFormat timeFormatter = new SimpleDateFormat(
				"yyyyMMdd_HHmmss", Locale.CHINA);
		long time = System.currentTimeMillis();
		String imageName = timeFormatter.format(new Date(time));
		// ContentValues是我们希望这条记录被创建时包含的数据信息
		ContentValues values = new ContentValues(3);
		values.put(MediaStore.Images.Media.DISPLAY_NAME, imageName);
		values.put(MediaStore.Images.Media.DATE_TAKEN, time);
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
		if (status.equals(Environment.MEDIA_MOUNTED)) {// 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
			imageFilePath = context.getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		} else {
			imageFilePath = context.getContentResolver().insert(
					MediaStore.Images.Media.INTERNAL_CONTENT_URI, values);
		}
		Log.i("", "生成的照片输出路径：" + imageFilePath.toString());
		return imageFilePath;
	}

	/**
	 * 聊天的时候比较两个时间
	 * 
	 * @Title: compareTwoDate
	 * @Description: TODO
	 * @param @return
	 * @return String
	 */
	public static boolean compareTwoDate(String predate, String nextdate) {
		if ((getLongTime(nextdate) - getLongTime(predate)) < (5 * 60 * 1000)) {
			return false;
		}
		return true;
	}

	@SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public static String compareNowdate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd HH:mm");
		try {
			Date nowdate = new Date(System.currentTimeMillis());
			if (getTwodateDay(date, sdf.format(nowdate)) == 0) {
				return getNoDayTime(date);
			} else if (getTwodateDay(date, sdf.format(nowdate)) == 1) {
				return "昨天\t" + getNoDayTime(date);
			} else {
				return sdf1.format(sdf.parse(date));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@SuppressLint("SimpleDateFormat")
	public static String getNoDayTime(String date) {
		SimpleDateFormat formatBuilder = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return formatBuilder.format(sdf.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 
	 * @Title: getTwodateDay
	 * @Description: TODO
	 * @param @param date1
	 * @param @param date2
	 * @param @return
	 * @return long
	 */
	public static long getTwodateDay(String date1, String date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return (sdf.parse(date2).getTime() - sdf.parse(date1).getTime())
					/ (24 * 60 * 60 * 1000);
		} catch (ParseException e) {
			return 0;
		}
	}

	/**
	 * 把标准时间转换成时间戳
	 * 
	 * @Title: getTime
	 * @Description: TODO
	 * @param @param user_time
	 * @param @return
	 * @return String
	 */
	@SuppressLint("SimpleDateFormat")
	public static long getLongTime(String user_time) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Date d;
		try {
			d = sdf.parse(user_time);
			long l = d.getTime();
			return l;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

    /**
     * 重新连接openfire
     * @param context
     */
	public static void againLogin(Context context){
		try{
			ClienConServer.client = null;
			ClientGroupServer.client = null;
			MypacketListener.getInstance().loginout();
			XmppConnectionUtil.getInstance().closeConnection();
			Intent intent = new Intent();
			intent.setAction(MyApplication.AGAIN_CONNECT);
			context.sendBroadcast(intent);
		}catch(Exception e){
            LogUtil.e("againLogin,exception:"+e.toString());
		}
	}

	/**
	 * 强制停止应用程序
	 * 
	 * @param pkgName
	 */
//	public static void forceStopPackage(String pkgName, Context context)
//			throws Exception {
//		ActivityManager am = (ActivityManager) context
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		Method method = Class.forName("android.app.ActivityManager").getMethod(
//				"forceStopPackage", String.class);
//		method.invoke(am, pkgName);
//	}
}
