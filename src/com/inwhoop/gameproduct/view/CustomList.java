package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 自定义ListView-解决ListView和ScrollView冲突的问题
 */
public class CustomList extends ListView {
    public CustomList(Context context) {
        super(context);
    }

    public CustomList(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomList(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        int expandSpec = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
