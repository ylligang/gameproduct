package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/** 
 * 
 * @Project: MainActivity
 * @Title: MyViewpager.java
 * @Package com.inwhoop.gameproduct.view
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2014-4-3 下午3:27:54
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MyViewpager extends ViewPager{

	public MyViewpager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyViewpager(Context context) {
		super(context);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
	
}
