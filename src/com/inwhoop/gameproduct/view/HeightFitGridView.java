package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 *    xml布局里的属性
 android:gravity="center"
 android:stretchMode="columnWidth"
 android:verticalSpacing="10dp"
 android:horizontalSpacing="10dp"
 android:numColumns="auto_fit"
 android:columnWidth="90dp"
 *
 * TODO: 高度自适应item个数与屏幕的gridview
 * Created withZK
 * User: Adm
 * Date: 14-1-13
 * Time: 下午5:32
 * To change this template use File | Settings | File Templates.
 */
public class HeightFitGridView extends GridView{
    public HeightFitGridView(Context context) {
        super(context);
    }

    public HeightFitGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeightFitGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //高度自动适应
        int expandSpec = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
