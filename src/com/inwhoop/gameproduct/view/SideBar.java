package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.inwhoop.gameproduct.R;
import com.inwhoop.gameproduct.application.MyApplication;

public class SideBar extends View {
    private char[] letter;
    private SectionIndexer sectionIndexter = null;
    private ListView list;
    private TextView mDialogText;
    private final int m_nItemHeight = MyApplication.heightPixels/43;
    private int choose = -1;// 选中
    private OnTouchingLetterChangedListener onTouchingLetterChangedListener;
    Paint paint = new Paint();

    public SideBar(Context context) {
        super(context);
        init();
    }

    public SideBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        letter = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'W', 'X', 'Y', 'Z','#'};
    }

    public SideBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setListView(ListView _list) {
        list = _list;
        sectionIndexter =(SectionIndexer) _list.getAdapter();
    }

    public void setTextView(TextView mDialogText) {
        this.mDialogText = mDialogText;
    }

    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        int i = (int) event.getY();
        int idx = i / m_nItemHeight;
        if (idx >= letter.length) {
            idx = letter.length - 1;
        } else if (idx < 0) {
            idx = 0;
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            mDialogText.setVisibility(View.VISIBLE);
            mDialogText.setText("" + letter[idx]);
            if (sectionIndexter == null) {
                sectionIndexter = (SectionIndexer) list.getAdapter();
            }
            int position = sectionIndexter.getPositionForSection(letter[idx]);
            if (position == -1) {
                return true;
            }
            list.setSelection(position);
        } else {
            mDialogText.setVisibility(View.INVISIBLE);
        }
        return true;
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float y = event.getY();// 点击y坐标
        final int oldChoose = choose;
        final OnTouchingLetterChangedListener listener = onTouchingLetterChangedListener;
        final int c = (int) (y / getHeight() * letter.length);// 点击y坐标所占总高度的比例*b数组的长度就等于点击b中的个数.

        switch (action) {
            case MotionEvent.ACTION_UP:
                setBackgroundDrawable(new ColorDrawable(0x00000000));
                choose = -1;//
                invalidate();
                if (mDialogText != null) {
                    mDialogText.setVisibility(View.INVISIBLE);
                }
                break;

            default:
                setBackgroundColor(getResources().getColor(R.color.homepage_bg));
                if (oldChoose != c) {
                    if (c >= 0 && c < letter.length) {
                        if (listener != null) {
                            listener.onTouchingLetterChanged(""+letter[c]);
                        }
                        if (mDialogText != null) {
                            mDialogText.setText(""+letter[c]);
                            mDialogText.setTextColor(getResources().getColor(R.color.green));
                            mDialogText.setVisibility(View.VISIBLE);
                        }

                        choose = c;
                        invalidate();
                    }
                }

                break;
        }
        return true;
    }
    /**
     * 向外公开的方法
     *
     * @param onTouchingLetterChangedListener
     */
    public void setOnTouchingLetterChangedListener(
            OnTouchingLetterChangedListener onTouchingLetterChangedListener) {
        this.onTouchingLetterChangedListener = onTouchingLetterChangedListener;
    }

    /**
     * 接口
     *
     * @author coder
     *
     */
    public interface OnTouchingLetterChangedListener {
        public void onTouchingLetterChanged(String s);
    }


    protected void onDraw(Canvas canvas) {

        float widthCenter = getMeasuredWidth() / 2;
        for (int i = 0; i < letter.length; i++) {
            paint.setColor(getResources().getColor(R.color.gray));
            paint.setTextSize(18);
            paint.setAntiAlias(true);
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            paint.setTextAlign(Paint.Align.CENTER);
            paint.setDither(true);
            if (i == choose) {
                paint.setColor(Color.parseColor("#3ecaf8"));
               // paint.setFakeBoldText(true);
               /* canvas.drawRect(widthCenter-5,(m_nItemHeight + 10) + (i * m_nItemHeight)-5,widthCenter+5,
                        (m_nItemHeight + 10) + (i * m_nItemHeight)+5,paint);*/
            }
            canvas.drawText(String.valueOf(letter[i]), widthCenter, (m_nItemHeight + 10) + (i * m_nItemHeight), paint);
        }
        super.onDraw(canvas);
    }
}
