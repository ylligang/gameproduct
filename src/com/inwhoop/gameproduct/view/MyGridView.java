package com.inwhoop.gameproduct.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * @Project: GuoTeng
 * @Title: MyGridView.java
 * @Package com.inwhoop.guoteng.view
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-14 上午11:22:15
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class MyGridView extends GridView {

	public MyGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyGridView(Context context) {
		super(context);
	}

	public MyGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}

}
